README.txt - 20180912 - 20170103 - 20140713 - 20140710 - 20140626

# Osm2xg Project

polyview2d:
==========

Having recently built [polyview2d](https://sites.google.com/site/polyview2d/), I wanted to be 
able to convert OpenStreetMap (OSM) xml extracts to the simple xgraph file used by polyview2d.
Polyview2d uses Qt to make it a cross platform app.

This necessitated getting to understand more of the osm xml format, like planet.osm... and lesser 
'extracts' downloaded by say [JOSM](http://josm.openstreetmap.de/wiki/Download).

Hence commenced this project, a suite of 'osm' apps... They are all 64-bit apps, since they 
are intended to handle really large files. See build notes below.

osmFile:
========

A library to open and read OSM files transparently, be it a BZip2 or gzip compressed file, 
or a plan text file. Thus this library has dependency on BZip2 and ZLIB libraries.

TODO:
Continue on the development a BZ2_seek64() function, like the gzseek64 functioned offered
by ZLIB.

osmstats:
=========

This application uses the above osmFile library to do the file open and reading...

This will 'stat' a file of any size, but takes time. For example, on the giant 37 GB 
planet-latest.osm.bz2 (26/06/2014), the first node is not encountered until about 7.7GB 
of uncompressed data has been processed, and the 'relation' not reached until 
about 502GB processed.

The results:

```
osmstats: Done uncompressed 541,414,654,737 bytes (504.24 GB), in 2:12:28.261 hrs:min:secs.
osmstats: xml -1577904996, chgsets 22994510, tags 1113851978, nodes -1895269335,
          relat 2638445, memb 30562052, ways 239643942, nds -1447353982, opn 0
```
          
DONE: The stat counters are now 64-bit counters due to the limit of int is exceeded as shown by 
the above negative numbers... 

DONE: This 'stat' app now can also output an xgraph .xg file after reading and collecting 
information from an OSM file (.osm, .osm.gz, .osm.bz2), and is fast superceeding all the 
others in the suite.

osm2xgr:
========

It is suffixed 'r' because for a simple, fast xml parser, it uses [rapidxml](http://rapidxml.sourceforge.net/manual.html) which comes as a single heder file only.

But to use rapidxml it is neccessary to load the whole file into memory which is passed to the
XML parser... and that memory MUST be maintained until processing finished since rapidxml does 
not copy any strings, but stores only pointer to the memory...

And it ONLY handles ASCII (well UTF-8) OSM files.

From experiments with JOSM I managed to download just the OSM for the area around YGIL
`C:\Users\Public\Documents\JOSM\ygil.osm` - just 541,859 bytes.

NEED ANOTHER STRATEGY TO DEAL WITH THE GIANT planet.osm, which weighs in at 300-500 GB, or more, 
since most systems can NOT allocate that much memory to load the whole file. Need to read and 
parse the OSM XML buffer by buffer.

osm2xgs:
========

It is suffixed 's' since it treats the input file as a simple byte stream, doing its own simple 
xml parsing.

It has a dependency on libbzip2, and works ONLY for *.osm.bz2 files.

Now it will handle any size *.osm.bz2, but takes a LOT of time.

And later found in trying to handle the GIANT 37GB planet-latest.osm.bz2 it still runs 
out of vector allocation memory while still doing the MILLIONS of nodes.

So cast around for other ideas...

osmconvert:
===========

20170103: Updated from 0.7, to 2016-02-12 20:30 Version 0.8.5 source from http://m.m.i24.cc/osmconvert.c, see [wiki](https://wiki.openstreetmap.org/wiki/Osmconvert), which may be the same source as in the [repo](https://gitlab.com/osm-c-tools/osmctools/tree/master/src), but not checked. The original source is C, but since it uses `bool`, used a `.cxx` extent, and merged several previous fixes for WIN32, built using msvc140 2015, in 64-bits, and bumped the version to `0.8.6`.

This has a dependency on gzip library, thus will handle BOTH ASCII *.osm, and *.osm.gz files, due to the fact library gzip falls back to a pass-through if the file in question is not a gz file.

This is an old source but did not require too much effort to compile it in Windows, using 
MSVC 10/14. I only revived it to 'review' how they handled large files... And the important 
thing noted was that it uses gzseek64()...

That means it can keep a hashed list of nodes that match the bound box parameter, and can 
get back the node co-ordinates when the ways (and prehaps relations) are being processed.

Another thing noted was that writes to temporary files, rather than trying, like I was, to 
store the information in allocated memory. Maybe this is the solution to the vector memory 
problem.

osm2xgf:
========

After seeing that osmconvert uses very fast xml parsing. It is certainly NOT very robust,
since it only uses the first letter, and sometime with the second letter of the xml tag to 
do the parsing.

So 'n' is 'node' or 'nd' depending on the second; 't' is tag, 'w' is way, etc...

Second, instead of storing the 'nodes' in a vector list, and then when looking a an `<nd ref="123456" />`
by going through the vector node list, repeatedly, is uses `<map>` to keep each node only as -
`std::map<unsigned long, APT>`, where APT is a simple struct with lat,lon. This sped up the nd 
lookup stage by over 1,000% without any apparent additional time penalty when storing the 
node.

I will now always try to apply this 'map' idea to looking up nodes...
 
So this is really a repeat of osm2xgr without the aid of an external xml parser library,
but still like osm2xgr allocates memory to hold the whole *.osm file content.

osmtags:
========

Just to get to 'understand' better what information can and is stored in 'tags', I wrote 
this app to give me a list from any *.osm file.

It essentially skips every xml tag except the `<tag k="key" v="value" />`. It stored and 
sort this list of 'keys' and the 'values' used for each, and outputs a list.

It pesently only works on uncompressed osm files, and like others in this suite reads the 
entire file contents into allocated memory to do the xml parsing itself, so really suffers 
as the file size grows. In fact in Windows I set an arbitrary limit of about 3GB...

TODO: 

 (a) Handle file as a stream, removing file size limit
 (b) Handle bz2 or gz file transparently

pbf2osm:
========

Another block of old code, that reads the osm pbf file format, and outputs it as osm xml. This 
functionality is now covered by osmconvert, but I thought it may yield some more clues to me 
about this osm pbf format.

But it was not any easier to read that osmconvert, so nothing more was done with this source 
aside from being able to compile it in Windows 7/10 using MSVC 10/14.

## Build Notes:

Osm2xg Project uses a CMake CMakeLists.txt to generate the appropriate build files. It is highly
recommended that the 'build' be done in an out-of-source directory, typically a 'build' directory.

The CMakeLists.txt contains a few user options -

#### option( BUILD_SHARED_LIB "Set ON to build and use shared library" OFF )

When on, will try to used the shared libraries for BZip2 and Zlib dependencies.
When off, the default, the MSVC build will use the 'static' libraries, if available.

#### option( USE_ZLIB2_FIND   "Set ON to use local FindZLIB2,cmake"     ON )

When off, the cmake distributed FindZLIB.cmake will be used, and it only searches for the 
single zlib shared libraries.

Alternatively, when on, the default, a custom FindZLIB2.cmake will be used, which searches 
for the debug and release static libraries for MSVC use. This custom 'find' also has a 
ZLIB_DEBUG switch, default off, to output extra information about the search progress.

In UNIX this should be -

```
$ cd <source>
$ cd build
$ cmake ..
$ make
$ make install (if desired)
```

In Windows this should be -

```
$ cd <source>
$ cd build
$ cmake ..
$ cmake --build . --config Release
$ cmake --build . --config Release --target INSTALL (if desired)
```

OR, of course the cmake GUI can be used.

SPECIAL WINDOWS NOTE:
=====================

Be warned that Windows places some limits on file sizes and memory allocations.
In most Windows OS (32 or 64 bit), a 32-bit application has the maximum is 2 
to 4GB! Remember a UINT_MAX is 4,294,967,295! This is also the maximum files size 
of a HDD formatted to WIN32 FAT.

In my case I build in Window 7/10 64-bits, using MSVC 10/14 64-bits, so build all 64-bit
apps, but to build the FULL suite of apps, the cmake configuration process must find 
64-bit versions of BZip2 and Zlib libraries to link with. And my HDD uses NTFS
formatting, Here an unsigned long long has a maximum value of 16 ExaBytes 
(18,446,744,073,709,551,615 bytes) - more than enough (I hope) ;=)).

It may be possible to build some or all in 32-bits, but this has NOT been tried, and 
may require some cmake and/or code modifications.

This is a **WORK IN PROGRESS**

It was a 'seach' in OSM for Bearbong Road that started this 'osm2xg' madness ;=))
See : http://geoffair.org/travel/bearbong.htm

Enjoy,  
Geoff.  
20180912

; eof
