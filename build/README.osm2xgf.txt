README.osm2xgf.txt - 20140705

This is an attempt to make it FAST ;=))

So,

1: load the whole file into memory,
2: Use minimal parsing stared in minimal vectors

This approach worked fine for small files, but as the osm file size increased 
the time also massively increased.

With the file 'F:\Projects\osm2xg\build\ygil1.osm', of 165 MB, (167,445,104 bytes), the 
load into memory was quick - in 6.817 secs.

The pasrsing of this memory was also quite FAST -
osm2xgf: xml items 2166672, changesets 0, tags 251101, nodes 838061, relat 1487,
 memb 11211, way 42415, nd 904573, in 48.158 secs
osm2xgf: Have collects 42415 ways, with 904573 nd's, to apply to 838061 nodes

But now processing this collected data into features, really took some time ;=((

I suspect the real time is take in looking up the nearly 1 million points (nd),
in the nearly a million (836,061) nodes is the killing action.

For each point, up to 836K nodes must be searched to find a match.
TOOK HOURS. I STOPPED the processing after over 2 HOURS, and only
about 75% of the ways had been done.

MAYBE, if I stored these nodes in a vector 'map', like 

typedef std::map<unsigned long,APT> mNODES;

static mNODES mNodes;

	APT apt;
	apt.lat = np->lat;
	apt.lon = np->lon;
	mNodes[np->ulid] = apt;

This would probably ADD some time to the data processing, but hopefully 
lead to a speed up while finding an 'nd'...

        for (i3 = 0; i3 < max3; i3++) {
            pndm = &pw->vNDm[i3];
            apt = mNodes[pnd->ulref];
or something like that...

Time for a test - 

Ok, the above extraction[ref] ALWAYS works, BUT if the 'ref' does NOT exist 
you get a blank/nul value

Seems the 'best' method is to use .find(ref), to get an iterator, and this 
will be the end() if not found, so...

typedef mNODES::iterator mNODESi;

	mNODESi iend = mNodes.end();
	mNODESi iter;

        for (i3 = 0; i3 < max3; i3++) {
            pndm = &pw->vNDm[i3];
	    iter = mNodes.find(pndm->ref);
	    if (iter == iend) {
		SPRTF("pn ref=%ld NOT found!\n", pndm->ref);
	    } else {
		apt = (*iter).second;

	    }
	}

Going to code this under a USE_STD_MAP switch. (after walking the dog ;=))

#ifdef USE_STD_MAP
#else // !USE_STD_MAP
#endif // USE_STD_MAP y/n

#ifdef USE_STD_MAP
#endif // USE_STD_MAP

#ifndef USE_STD_MAP
#endif // !USE_STD_MAP

Ok, it seems the data extraction processing was not lengthened -
osm2xgf: Loading default example 'F:\Projects\osm2xg\build\ygil1.osm'!
osm2xgf: Loaded file 'F:\Projects\osm2xg\build\ygil1.osm', 167445104 bytes, in 4
.508 secs
osm2xgf: xml items 2166672, changesets 0, tags 251101, nodes 838061, relat 1487,
 memb 11211, way 42415, nd 904573, in 44.038 secs
osm2xgf: Have collects 42415 ways, with 904573 nd's, to apply to 838061 nodes

Amd YIKES, the collection into 'features' and outputting thme fell down to 
just a few minutes, instead of HOURS ;=))
osm2xgf: Done data output... total elapsed 2:29.678 min:secs

A CLEAR CASE WHERE USING 'map' IS VERY, VERY EFFECTIVE.

But an interesting note, at exit the clean up of these largish vectors, and map,
can also take MINUTES, sometime LONGER than the processing :-
osm2xgf: Done data output... total elapsed 1:28.847 min:secs
osm2xgf: Clean up took 1:17.199 min:secs
osm2xgf: End processing in 2:46.048 min:secs


//////////////////////////////////////////////////////////////////////////////////

// eof

