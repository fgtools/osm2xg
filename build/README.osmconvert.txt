README.osmconvert.txt - 20140704

Note while building osmconvert.cxx

See build.diff for the few changes required to compile it in MSVC10

The most interesting option (for me) is the bounding box
-b=<x1>,<y1>,<x2>,<y2>    apply a border box

The 'box' orientation
        To do this, enter the southwestern and the
        northeastern corners of that area. 
	For example -b=-0.5,51,0.5,52

And to ensure 'way' are complete
--complete-ways           do not clip ways at the borders
--complex-ways            do not clip multipolygons at the borders

Thankfully, parameters cna be put in a file
--parameter-file=<file>   param. in file, separated by empty lines
        Lines starting with "// " will be treated as comments.

So to apply my YGIL value
@set MINX=147
@set MAXX=150
@set MINY=-32
@set MAXY=-31

// NOTE: MUST ADD extra line between params
// YGIL extract
-v=2
-b=147,-32,150,-31
--complete-ways
--complex-ways
-o=ygil.osm
D:\SAVES\OSM\australia.osm


