@setlocal
@if "%~1x" == "x" (
@echo Give word to find...
@exit /b 1
)
@set TMPFND=%1
@shift
@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT
:GOTCMD
@cd ..
fa4 "%TMPFND%" * -r -b- -x:build %TMPCMD%

