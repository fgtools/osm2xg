@setlocal
@set TMPEXE=Release\osmconvert.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@set TMP3RD=F:\Projects\software.x64\bin

@set TMPCMD=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPCMD=%TMPCMD% %1
@shift
@goto RPT

:GOTCMD

@REM Switch to using the static zlib (zlibstatic.lib) so this should not be required
@REM set PATH=%TMP3RD%;%PATH%
@if "%TMPCMD%x" == "x" goto TRYEX


%TMPEXE% %TMPCMD%

@goto END

:TRYEX
@set TMPFIL=D:\SAVES\OSM\australia.osm.gz
@if EXIST %TMPFIL% goto TRYFIL
@echo File %TMPFIL% NOT FOUND! *** FIX ME *** to point to an *.osm.gz example file...
@goto END

:TRYFIL
%TMPEXE% -v --statistics "-o=tempstat.txt" %TMPFIL%
@goto END

:NOEXE
@echo Can NOT find exe %TMPEXE%! Has it been built? *** FIX ME **
@goto END

:END
