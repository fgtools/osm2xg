@setlocal
@set TMPEXE=Release\osm2xgr.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@set TMPFIL1=..\testdata\ygil.osm
@if NOT EXIST %TMPFIL1% goto NOFIL1
@set TMPOUT1=..\testData\ygil.xg
@if EXIST %TMPOUT1% goto NOWAY

%TMPEXE% -o %TMPOUT1% %TMPFIL1%

@goto END

:NOEXE
@echo Can NOT locate %TMPEXE%! Has it been built?
@goto END

:NOFIL1
@echo Can NOT locate %TMPFIL1%! *** FIX ME *** to suit your environment...
@goto END

:NOWAY
@echo %TMPOUT1% already exists! rename, delete, move to do this test again
@goto END

:END
