@setlocal
@set TMPEXE=Release\osm2xgs.exe
@if NOT EXIST %TMPEXE% goto NOEXE
@if "%~1x" == "x" goto USEDEF
@set TMPFIL1=%1
@goto TESTFIL
:USEDEF
@set TMPFIL1=C:\Users\Public\Documents\JOSM\ygil.osm.bz2
:TESTFIL
@if NOT EXIST %TMPFIL1% goto NOFIL1

%TMPEXE% %TMPFIL1%

@goto END

:NOEXE
@echo Can NOT locate %TMPEXE%! Has it been built?
@goto END

:NOFIL1
@echo Can NOT locate %TMPFIL1%! *** FIX ME *** to suit your environment...
@goto END

:END
