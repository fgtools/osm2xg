@setlocal

@set TMPFIL=osm2xgf.exe
@call :DOONE %TMPFIL%
@set TMPFIL=osm2xgr.exe
@call :DOONE %TMPFIL%
@set TMPFIL=osm2xgs.exe
@call :DOONE %TMPFIL%
@set TMPFIL=osmconvert.exe
@call :DOONE %TMPFIL%
@set TMPFIL=osmstats.exe
@call :DOONE %TMPFIL%
@set TMPFIL=osmtags.exe
@call :DOONE %TMPFIL%
@set TMPFIL=shp2xg.exe
@call :DOONE %TMPFIL%
@set TMPFIL=xgdiff.exe
@call :DOONE %TMPFIL%

@REM Update polyView2D
@set TMPFIL=polyView2D.exe
@set TMPSRC=polyView2D\Release\%TMPFIL%
@set TMPDST=C:\MDOS\%TMPFIL%
@call :DOTWO %TMPFIL%

@goto END

:DOONE
@set TMPSRC=Release\%1
@set TMPDST=C:\MDOS\%1

:DOTWO
@if NOT EXIST %TMPSRC% goto NOFIL1
@if NOT EXIST %TMPDST% goto DOCOPY1
@fc4 -q -b -v0 %TMPSRC% %TMPDST%
@if ERRORLEVEL 2 goto DOCOPY1
@if ERRORLEVEL 1 goto DOCOPY1
@echo Files %TMPFIL% apppear the SAME - nothing done...
@goto DNONE1
:DOCOPY1
@echo.
@echo Different, see -
@call dirmin %TMPSRC%
@call dirmin %TMPDST%
@echo.
@ask *** CONTINUE with install? *** Only y continues
@if ERRORLEVEL 2 goto NOASK
@if ERRORLEVEL 1 goto DOINST
@echo Skipping copy at this time...
@echo.
@goto END
:NOASK
@echo ask not found in path...
@echo *** CONTINUE with install? *** Only Ctrl+c aborts
@pause
:DOINST
@copy %TMPSRC% %TMPDST%
@echo.
@goto DNONE1

:NOFILE1
@echo Can NOT locate %TMPFIL%
:DNONE1
@goto :EOF

:END
