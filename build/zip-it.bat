@setlocal
@set TMPVER=02

@set TMPZIP=F:\Projects\osm2xg\build\osm2xg-%TMPVER%.zip
@set TMPA=-a

@if EXIST %TMPZIP% (
@echo.
@echo ******** This is an UPDATE! ************
@echo.
@set TMPA=-u
)

@echo Will zip to %TMPZIP%
@echo *** CONTINUE? ***
@pause

cd ..

call zip8 %TMPA% -r -P -o -xbuild\* %TMPZIP%

call zip8 -u -P %TMPZIP% build\build-me.bat

@REM eof
