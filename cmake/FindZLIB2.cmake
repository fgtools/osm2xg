# - Find zlib2 - 20140710
# This modified version attempts to find the 'static' libraries
# Find the native ZLIB includes and library.
# Once done this will define
#
#  ZLIB2_INCLUDE_DIRS   - where to find zlib.h, etc.
#  ZLIB2_LIBRARIES      - List of libraries when using zlib - need -DZLIB_DLL defined
#  ZLIB2_STATICLIBS     - List of static libraries when using zlib
#  ZLIB2_FOUND          - True if zlib found.
#
#  ZLIB2_VERSION_STRING - The version of zlib found (x.y.z)
#  ZLIB2_VERSION_MAJOR  - The major version of zlib
#  ZLIB2_VERSION_MINOR  - The minor version of zlib
#  ZLIB2_VERSION_PATCH  - The patch version of zlib
#  ZLIB2_VERSION_TWEAK  - The tweak version of zlib
#
# The following variable are provided for backward compatibility
#
#  ZLIB2_MAJOR_VERSION  - The major version of zlib
#  ZLIB2_MINOR_VERSION  - The minor version of zlib
#  ZLIB2_PATCH_VERSION  - The patch version of zlib
#
# An includer may set ZLIB_ROOT to a zlib installation root to tell
# this module where to look.
# Adding ZLIB2_DEBUG will output LOTS of information
#
#=============================================================================
# Copyright 2001-2011 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

set(_ZLIB2_SEARCHES)

# Search ZLIB_ROOT first if it is set.
if(ZLIB_ROOT)
  set(_ZLIB2_SEARCH_ROOT PATHS ${ZLIB_ROOT} NO_DEFAULT_PATH)
  list(APPEND _ZLIB2_SEARCHES _ZLIB2_SEARCH_ROOT)
endif()

# Normal search.
set(_ZLIB2_SEARCH_NORMAL
  PATHS "[HKEY_LOCAL_MACHINE\\SOFTWARE\\GnuWin32\\Zlib;InstallPath]"
        "$ENV{PROGRAMFILES}/zlib"
  )
list(APPEND _ZLIB2_SEARCHES _ZLIB2_SEARCH_NORMAL)

set(ZLIB2_NAMES z zlib zdll zlib1 zlibd zlibd1)
if (ZLIB2_DEBUG)
    message(STATUS "*** Will search ${_ZLIB2_SEARCHES} for ${ZLIB2_NAMES}, and zlibstatic")
endif ()
# Try each search configuration.
foreach(search ${_ZLIB2_SEARCHES})
  find_path(ZLIB2_INCLUDE_DIR NAMES zlib.h             ${${search}} PATH_SUFFIXES include)
  find_library(ZLIB2_LIBRARY  NAMES ${ZLIB2_NAMES}     ${${search}} PATH_SUFFIXES lib)
if (MSVC)
  find_library(ZLIB2_STATIC_LIB_REL  NAMES zlibstatic  ${${search}} PATH_SUFFIXES lib)
  find_library(ZLIB2_STATIC_LIB_DBG  NAMES zlibstaticd ${${search}} PATH_SUFFIXES lib)
else ()
  find_library(ZLIB2_STATICLIB  NAMES zlibstatic       ${${search}} PATH_SUFFIXES lib)
endif ()
endforeach()
if (MSVC)
    if (ZLIB2_STATIC_LIB_REL AND ZLIB2_STATIC_LIB_DBG)
        set( ZLIB2_STATICLIB optimized ${ZLIB2_STATIC_LIB_REL} debug ${ZLIB2_STATIC_LIB_DBG} )
    elseif (ZLIB2_STATIC_LIB_REL)
        set( ZLIB2_STATICLIB ${ZLIB2_STATIC_LIB_REL} )
    endif ()
endif ()
mark_as_advanced(ZLIB2_LIBRARY ZLIB2_INCLUDE_DIR)

if(ZLIB2_INCLUDE_DIR AND EXISTS "${ZLIB2_INCLUDE_DIR}/zlib.h")
    file(STRINGS "${ZLIB2_INCLUDE_DIR}/zlib.h" ZLIB_H REGEX "^#define ZLIB_VERSION \"[^\"]*\"$")

    string(REGEX REPLACE "^.*ZLIB_VERSION \"([0-9]+).*$" "\\1" ZLIB_VERSION_MAJOR "${ZLIB_H}")
    string(REGEX REPLACE "^.*ZLIB_VERSION \"[0-9]+\\.([0-9]+).*$" "\\1" ZLIB_VERSION_MINOR  "${ZLIB_H}")
    string(REGEX REPLACE "^.*ZLIB_VERSION \"[0-9]+\\.[0-9]+\\.([0-9]+).*$" "\\1" ZLIB_VERSION_PATCH "${ZLIB_H}")
    set(ZLIB2_VERSION_STRING "${ZLIB_VERSION_MAJOR}.${ZLIB_VERSION_MINOR}.${ZLIB_VERSION_PATCH}")

    # only append a TWEAK version if it exists:
    set(ZLIB_VERSION_TWEAK "")
    if( "${ZLIB_H}" MATCHES "^.*ZLIB_VERSION \"[0-9]+\\.[0-9]+\\.[0-9]+\\.([0-9]+).*$")
        set(ZLIB_VERSION_TWEAK "${CMAKE_MATCH_1}")
        set(ZLIB2_VERSION_STRING "${ZLIB_VERSION_STRING}.${ZLIB_VERSION_TWEAK}")
    endif()

    set(ZLIB2_MAJOR_VERSION "${ZLIB_VERSION_MAJOR}")
    set(ZLIB2_MINOR_VERSION "${ZLIB_VERSION_MINOR}")
    set(ZLIB2_PATCH_VERSION "${ZLIB_VERSION_PATCH}")
endif()

# handle the QUIETLY and REQUIRED arguments and set ZLIB_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZLIB2 REQUIRED_VARS ZLIB2_LIBRARY ZLIB2_INCLUDE_DIR
                                       VERSION_VAR ZLIB2_VERSION_STRING)

if(ZLIB2_FOUND)
    if (ZLIB2_DEBUG)
        message(STATUS "*** ZLIB2_FOUND inc ${ZLIB2_INCLUDE_DIR} lib ${ZLIB2_LIBRARY} static ${ZLIB2_STATICLIB}")
    endif ()
    set(ZLIB2_INCLUDE_DIRS ${ZLIB2_INCLUDE_DIR})
    set(ZLIB2_LIBRARIES    ${ZLIB2_LIBRARY})
    set(ZLIB2_STATICLIBS   ${ZLIB2_STATICLIB})
elseif (ZLIB2_DEBUG)    
    message(STATUS "*** ZLIB2-NOTFOUND inc ${ZLIB2_INCLUDE_DIR} lib ${ZLIB2_LIBRARY} static ${ZLIB2_STATICLIB}")
endif()

# eof
