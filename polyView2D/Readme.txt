Update 20141005 - 20140713:

This is a clone/fork of the original polyview2d. It may or may not
work as the online documentation states, but should be very close.

site: https://sites.google.com/site/polyview2d/

Also this clone/fork is built by cmake. No attempt has been made 
to keep the qmake *.pro file up-to-date, and thus may not build.

To compile PolyView from source, you need CMake installed. See -
 http://www.cmake.org/install/

Change directory to the 'build' folder, and run -
build-me.bat - Windows
build-me.sh  - Linux

Note the Windows install location is C:\MDOS, which is in my PATH
and the linux install location is $HOME/bin, also in my PATH.
Manually adjust these locations as desired.

And in windows the Qt 64-bit, and MSVC vc batch for AMD64 must be 
available. See the build-me.bat

But even without these scripts the simple build instruction are -

Windows: In the build folder run
> cmake .. [-DCMAKE_INSTALL_PREFIX=path/for/install]
> cmake --build . --config Release
And if install desired -
> cmake --build . --config Release --target INSTALL

Linux: In the build folder run
$ cmake .. [-DCMAKE_INSTALL_PREFIX=path/for/install]
$ make
ANd if install desired
$ make install

Running the polyView2D binary with --help will show the command line
help.

The testData folder contains lots of example xg files to view. The 
source indicates it also supports files of (type == "pol" || type == "cnt")
but this has NOT been tested in this source.

The xg file format is a sub-set of xgraph files. polyView2D supports line like
# this is a comment line, and skipped
anno <lon> <lat> Annotation text
color red # trailing comment
lon1 lat1 [; 1:1]
lon2 lat2
lon3 lat3
next

A very simply and quick method to 'draw' and 'view' a polygon or poly line.

Have fun...

Geoff.


Original Text 20111109: - DEPRECIATED
-----------------------

As stated above no attempt has been made to keep the .pro file
correct, so this may fail...

PolyView is a free polygon viewer. The PolyView documentation is at

https://sites.google.com/site/polyview2d/

To compile PolyView from source, the Qt 4 development libraries need to
be installed. They can can be obtained in Ubuntu by typing

apt-get install qmake

The next steps are:

cd gui
qmake polyview.pro 
make

The 'qmake' command will generate a makefile which whill be used by
'make' to build the executable. The 'gui' directory has a sample
makefile as generated by 'qmake'.

# eof
