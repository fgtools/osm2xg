/*\
 * osm2xg.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _OSM2XG_HXX_
#define _OSM2XG_HXX_
#include <vector>
#include <string>

#define BAD_LL 400.0

// <tag k='leisure' v='pitch' />
typedef struct tagTAG {
    std::string k;
    std::string v;
}TAG, *PTAG;
typedef std::vector<TAG> vTAG;

// <nd ref='1803404969' />
typedef struct tagND {
    unsigned long ulref;
    // vTAG vTags; // no, tags belong to the 'way'
}ND, *PND;
typedef std::vector<ND> vND;

// <way id='169174092' timestamp='2012-06-26T23:32:07Z' uid='168136' user='goldfishxyz' visible='true' version='1' changeset='12031826'>
/* ----------------------------------------------------
 <way id="242413390" visible="true" version="1" changeset="18394571" timestamp="2013-10-16T22:02:59Z" user="josmougn" uid="522742">
  <nd ref="2498917261"/>
  <nd ref="2498917245"/>
  <nd ref="2498917243"/>
  <nd ref="2498917241"/>
  <nd ref="2498917235"/>
  <nd ref="2498917238"/>
  <nd ref="2498917239"/>
  <nd ref="2498917259"/>
  <nd ref="2498917261"/>
  <tag k="area" v="yes"/>
  <tag k="highway" v="pedestrian"/>
  <tag k="name" v="Place Ren� Cassin"/>
  <tag k="source" v="bing"/>
 </way>
 <way id="166829076" visible="true" version="2" changeset="18394571" timestamp="2013-10-16T22:02:59Z" user="josmougn" uid="522742">
  <nd ref="1782630994"/>
  <nd ref="2498917253"/>
  <nd ref="1782631035"/>
  <nd ref="2498917257"/>
  <nd ref="1782631031"/>
  <tag k="highway" v="footway"/>
  <tag k="source" v="GPS"/>
 </way>
   --------------------------------------------------- */
typedef struct tagWAY {
    unsigned long ulid; //='169174092' 
    time_t timestamp;   //='2012-06-26T23:32:07Z' 
    unsigned long uluid;    //='168136' 
    std::string user;   //='goldfishxyz' 
    bool visible;   //='true' 
    int version;    //='1' 
    unsigned long ulchangeset;  // ='12031826'>
    int valid;
    vND vRefs;
    vTAG vTags;
} WAY, *PWAY;

// ELE_NODE node id="133840431" timestamp="2010-10-11T19:55:07Z" uid="79144" user="John H" visible="true" version="5" changeset="6015441" lat="-31.7156299" lon="148.6700669" 
typedef struct tagNODE {
    unsigned long ulid; //="133840431" 
    time_t timestamp; //="2010-10-11T19:55:07Z" 
    unsigned long uluid;    // ="79144"
    std::string user;   //="John H" 
    bool visible;   //="true" 
    int version;    //="5" 
    unsigned long ulchangeset;  //="6015441" 
    double lat; // ="-31.7156299" 
    double lon; //="148.6700669" 
    int valid;
    vTAG vTags;
    int used;
}NODE, *PNODE;

typedef std::vector<WAY> vWAY;
typedef std::vector<NODE> vNODE;
#define typ_none   0
// <member type='way' ref='127295009' role='forward' />
#define typ_way    1
// <member type="node" ref="273347893" role="via"/>
#define typ_node   2
// <member type="relation" ref="408651" role=""/>
#define typ_relat  3


typedef struct tatMEMB {
    int type;
    unsigned long ulref;
    std::string role;
} MEMB, *PMEMB;

typedef std::vector<MEMB> vMEMBS;

typedef struct tagRELAT {
    unsigned long ulid; //='169174092' 
    time_t timestamp;   //='2012-06-26T23:32:07Z' 
    unsigned long uluid;    //='168136' 
    std::string user;   //='goldfishxyz' 
    bool visible;   //='true' 
    int version;    //='1' 
    unsigned long ulchangeset;  // ='12031826'>
    int valid;
    vMEMBS vMems;
    vTAG vTags;
} RELAT, *PRELAT;

typedef std::vector<RELAT> vRELATS;

typedef struct tagAPT {
    double lat,lon;
    unsigned long ulid;
}APT, *PAPT;

#define USE_NEW_FEAT

typedef struct tagANNO {
    std::string anno;   // annotation string
    APT pt; // location of annotation
}ANNO, *PANNO;
typedef std::vector<ANNO> vANNO;

typedef std::vector<APT> vAPT;
typedef std::vector<std::string> vSTG;
typedef struct tagFEAT {
    const char *color;  // color for point
    vAPT vPoints;   // points of feature
    vANNO vAnnos;   // any annotations
    vSTG comments;      // add comments to output
}FEAT, *PFEAT;

typedef struct tagFEATO {
    vAPT vPoints;
    std::string anno;   // annotation
    APT pt;             // point for annotation
    vSTG comments;      // add comments to output
}FEATO, *PFEATO;

typedef std::vector<FEAT> vFEATS;

#define v_id   0x00000001
#define v_ts   0x00000002
#define v_uid  0x00000004
#define v_user 0x00000008
#define v_vis  0x00000010
#define v_vers 0x00000020
#define v_chg  0x00000040
#define v_lat  0x00000080
#define v_lon  0x00000100
#define v_k    0x00000200
#define v_v    0x00000400
// maybe "visible", v_vis, is NOT always present. 
// Was NOT in some OSM nodes written by osmconvert app!
#define v_way (v_id|v_ts|v_uid|v_user|v_vers|v_chg)
#define v_all (v_way|v_lat|v_lon)

// ======================================
// MINIMAL SET
typedef struct tagNODEM {
    unsigned long ulid;
    double lat,lon;
}NODEM, *PNODEM;
typedef std::vector<NODEM> vNODEM;

typedef struct tagNDM {
    unsigned long ulref;
}NDM, *PNDM;
typedef std::vector<NDM> vNDM;

typedef struct tagWAYM {
    unsigned long ulid;
    vNDM vNDm;
    vTAG vTags;
}WAYM, *PWAYM;
typedef std::vector<WAYM> vWAYM;

// ======================================


#endif // #ifndef _OSM2XG_HXX_
// eof - osm2xg.hxx
