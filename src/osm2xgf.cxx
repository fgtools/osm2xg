/*\
 * osm2xgf.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/
#define USE_STD_MAP

#include <sstream>
#include <stdio.h>
#ifdef _MSC_VER
#include <conio.h>
#else
#include <stdlib.h> // for atoi(), ...
#include <string.h> // for memset(), ...
#endif
#ifdef USE_STD_MAP
#include <map>
#endif // USE_STD_MAP y/n
#include <math.h>   // for atof(), ...
#include "sprtf.hxx"
#include "utils.hxx"
#include "osm2xg.hxx"
#include "osm2xgf.hxx"

static const char *module = "osm2xgf";

// map a string to a string, simple
typedef std::map<std::string,std::string> vMAPSS; //  tags;
typedef vMAPSS::iterator iMAPSS;

// #define USE_HIGHWAY_FILTER
#undef USE_HIGHWAY_FILTER   // somethings failing???

// forward reference
int process_buffer(char *text, size_t len);
int use_data();
void clean_up();

static const char *example = "D:\\SAVES\\OSM\\LEIG.osm";
static const char *example1 = "F:\\Projects\\osm2xg\\build\\ygil.osm";
static const char *example4 = "F:\\Projects\\osm2xg\\build\\ygil1.osm";
static const char *example2 = "D:\\SAVES\\OSM\\australia.osm";
static const char *example3 = "C:\\Users\\Public\\Documents\\JOSM\\ygil.osm";

static vSTG vIn_Files;
static bool verbose_xg = false;
static bool add_each_file = false;
static std::string base = "temp";
//static std::string out_file = "tempallf.xg";
static std::string out_file = "tempall1.xg";
static int verbosity = 0;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static int check_me( const char *msg )
{
    int i = SPRTF("CHECK ME: %s\n",msg);
#ifdef _MSC_VER
    if (!bIsRedON()) {
        //_kbhit();
        SPRTF("Any key to continue!\n");
        _getch();
    }
#endif
    return i;
}

// stats
typedef struct tagXSTATS {
    int xml_items, chset_count, tag_count, node_count, relat_cnt, memb_cnt, way_cnt, nd_cnt;
}XSTATS, *PXSTATS;

static XSTATS xStats;

void give_help( char *name )
{
    SPRTF("\n");
    SPRTF("%s version %s\n", name, VERSION);
    SPRTF("Usage: osm2xg [options] in_file1.osm [in_file2.osm ... in_fileN.osm]\n");
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = Give this help and exit(2)\n");
    SPRTF(" --base name   (-b) = Set 'base' name for each file. (def=%s)\n", base.c_str());
    SPRTF(" Setting a base name implies write each feature to a numbered file.\n");
    SPRTF(" --each        (-e) = Write each feature to a numbered file list. (def=%s)\n",
        (add_each_file ? "On" : "Off"));
    SPRTF(" --log file    (-l) = Set log file. (def=%s)\n", get_log_file());
    SPRTF(" --out file    (-o) = Set output file. (def=%s)\n", out_file.c_str());
    SPRTF(" --verb[n]     (-v) = Bump/Set verbosity. (def=%d)\n", verbosity );
    SPRTF(" --xgverb      (-x) = Add extra comments to xgraph file(s). (def=%s)\n",
        (verbose_xg ? "On" : "Off"));
    SPRTF(" Includes index, way id, and distance from previous (simple Pythagorean calc)\n");

}

int check_log_file(int argc, char **argv)
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    set_log_file((char *)"tempexf.txt",false);
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'b':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    base = sarg;
                    if (VERB1) SPRTF("%s: Set base of numbered file to '%s'\n", module, base.c_str());
                } else {
                    printf("%s: Expected base file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                // NOTE fall through - setting base implies each feature to new numbered file
            case 'e':
                add_each_file = true;
                if (VERB1) SPRTF("%s: Set to write each feature to '%snn.xg", module, base.c_str());
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                } else {
                    printf("%s: Expected file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            }
        }
    }
    return iret;
}


int parse_command( int argc, char **argv )
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    iret = check_log_file(argc,argv);
    if (iret)
        return iret;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
            case 'l':
                i++;    // already dealt with
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_file = sarg;
                    if (VERB1) SPRTF("%s: Set out file to '%s'.\n", module, out_file.c_str());
                } else {
                    SPRTF("%s: Expected out_file to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'v':
                verbosity++;
                while (*sarg) {
                    if (*sarg == 'v') {
                        verbosity++;
                    } else if (ISDIGIT(*sarg)) {
                        verbosity = atoi(sarg);
                    }
                }
                if (VERB1) SPRTF("%s: Set verbosity to %d\n", module, verbosity);
                break;
            case 'x':
                verbose_xg = true;
                if (VERB1) SPRTF("%s: Set verbose_xg\n", module);
                break;
            default:
                SPRTF("%s: Unknown argument '%s'! Aborting...\n", module, arg);
                return 1;

            }
        } else {
#ifdef _MSC_VER
            if (is_file_or_directory64(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#else
            if (is_file_or_directory32(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#endif
            vIn_Files.push_back(arg);
            if (VERB1) SPRTF("%s: Added file '%s' for processing.\n", module, arg);
        }

    }
    i = (int)vIn_Files.size();
#ifndef NDEBUG
#ifdef _MSC_VER
    if ((i == 0) && (is_file_or_directory64((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    
    // Include way tag as information anno strings
    verbose_xg = true;  // *** ALWAYS **ON** FOR DEBUG ***

#else
    if ((i == 0) && (is_file_or_directory32((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    
#endif
#endif
    if (i == 0) {
        SPRTF("%s: No input files found in command! Aborting...\n", module );
        iret = 1;
    }
    return iret;

}

int process_in_files()
{
    int iret = 0;
    size_t ii,max = vIn_Files.size();
    std::stringstream s;
    std::string indent = " ";
    char *text;
    size_t len, res;
    PXSTATS pxs = &xStats;
    double bgn = get_seconds();

    memset(pxs,0,sizeof(xStats)); // clear the stats

    for (ii = 0; ii < max; ii++) {
        double bgnf = get_seconds();
        std::string f = vIn_Files[ii];
        char *file = (char *)f.c_str();
#ifdef _MSC_VER
        if (is_file_or_directory64(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        __int64 i64 = get_last_file_size64();
        __int64 size;
        size_t bytes = sizeof(size_t);
        if (bytes == sizeof(unsigned int)) {
            size = UINT_MAX;
            SPRTF("%s: Error: size_t is ONLY an 'unsigned int'\n", module, bytes);
            exit(1);
        } else if (bytes == sizeof(unsigned long))
            size = ULONG_MAX;
        else if (bytes == sizeof(unsigned long long))
            size = ULLONG_MAX;
        // memory allocation limit for a 32-bit app on a 64-bit system
        // You should link your application with /LARGEADDRESSAWARE to make more 
        // than 2GB available to the application. Then you can use up to 4GB 
        // on a 64-bit OS in a 32-bit application.
        // australia.osm is 2798125270
        if (i64 > 3000000000) {
            SPRTF("%s: Files size %s TOO big to allocate memory\n", module, get_I64_Stg( i64 ));
            exit(1);
        }
        len = (size_t)i64;
#else
        if (is_file_or_directory32(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        len = get_last_file_size32();
#endif
        if (len > 500000000) {
            SPRTF("%s: Large file... loading can take time... patients...\n");
        }
        text = new char[len+2];
        if (!text) {
            SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)len);
            return 1;
        }
        FILE *fp = fopen(file,"rb"); // text file
        if (!fp) {
            SPRTF("%s: Unable to open file %s!\n", module, file );
            delete text;
            return 1;
        }
        //res = fread(text,len,1,fp);
        //if (res != 1) {
        res = fread(text,1,len,fp);
        if (res != len) { // this fails if not "rb"
            SPRTF("%s: Read FAILED! re %d, got %d\n", module, (int)len, (int)res );
            fclose(fp);
            delete text;
            return 1;
        }
        fclose(fp);

        text[len] = 0;


        SPRTF("%s: Loaded file '%s', %u bytes, in %s\n", module, file, (int)len,
            get_seconds_stg( get_seconds() - bgnf) );

        iret = process_buffer(text,len);

        delete text;

        if (iret)
            break;
    }

    SPRTF( "%s: xml items %d, changesets %d, tags %d, nodes %d, relat %d, memb %d, way %d, nd %d, in %s\n", module,
        pxs->xml_items, pxs->chset_count, pxs->tag_count, pxs->node_count, 
        pxs->relat_cnt, pxs->memb_cnt, pxs->way_cnt, pxs->nd_cnt,
        get_seconds_stg( get_seconds() - bgn ) );

    ///////////////////////////////////////////
    iret |= use_data(); // whole purpose of the load
    //////////////////////////////////////////
    SPRTF("%s: Done data output... total elapsed %s\n", module, get_seconds_stg( get_seconds() - bgn ) );
    return iret;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    double bgns = get_seconds();
    // test_check_me();
    iret = parse_command(argc,argv);
    if (iret)
        return iret;
    iret = process_in_files();
    clean_up();

    SPRTF("%s: End processing in %s\n", module, get_seconds_stg( get_seconds() - bgns ));
    return iret;
}

////////////////////////////////////////////////////////////////////////////

static bool isxmlend = false;
typedef struct tagXMLTAGS {
    char *key;
    char *val;
}XMLTAGS, *PXMLTAGS;
#define MY_MAX_TAGS 64
static XMLTAGS xmlTags[MY_MAX_TAGS];
static int next_tag;

int get_xml_tags( char *xbgn, char *xend )
{
    int iret = 0;
    next_tag = 0;
    int c, d;
    xbgn++; // skip '<'
    while ((c = *xbgn) && (c > ' ') && (xbgn < xend)) xbgn++;   // skip type, tag in this case
    // gather attributes
    while (xbgn < xend) {
        while ((*xbgn <= ' ')&&(xbgn < xend)) xbgn++;
        c = *xbgn;
        if ((c == '/')||(c == '>'))
            break;
        xmlTags[next_tag].key = xbgn;
        while ((c = *xbgn) && (c > ' ') && (c != '=') && (c != '/') && (c != '>') && (xbgn < xend)) xbgn++;
        if (c == '=') {
            xbgn++; // skip '='
            d = *xbgn++; // skip quote '"' or "'"
            xmlTags[next_tag++].val = xbgn; // set begin
            while ((c = *xbgn) && (c != d) && (xbgn < xend)) xbgn++;
            xbgn++; // skip trailong quote
        } else {
            xmlTags[next_tag++].val = 0;
        }
        if (next_tag >= MY_MAX_TAGS) {
            SPRTF("Recompile with larger MY_MAX_TAGS\n");
            check_me("Need more tag buffer");
            exit(1);
        }
    }
    return iret;
}

#define xml_none 0
#define xml_node 1
#define xml_way  2
static int last_xml_item = xml_none;

#ifdef USE_STD_MAP
typedef std::map<unsigned long,APT> mNODES;
typedef mNODES::iterator mNODESi;
static mNODES mNodes;
#else // !USE_STD_MAP
static vNODEM vNodes;
static PNODEM last_node = 0;
#endif // USE_STD_MAP y/n
#define v_nodem (v_id|v_lat|v_lon)
static vWAYM vWays;
static PWAYM last_way = 0;
static vFEATS vFeats;
static FEAT feat;

#ifdef USE_HIGHWAY_FILTER
// highway filter - add or not...
bool highway_filter( vMAPSS &tags )
{
    bool add_feat = true;
    std::string stg;
    iMAPSS hwy = tags.find("highway");
    if (hwy != tags.end()) {
        stg = hwy->second;
        if (stg == "track")
            add_feat = false;
        else if (stg == "tertiary")
            add_feat = false;
        else if (stg == "residential")
            add_feat = false;
        else if (stg == "footway")
            add_feat = false;
        else if (stg == "unclassified")
            add_feat = false;
        else if (stg == "pedestrian")
            add_feat = false;
        else if (stg == "service")
            add_feat = false;
        else if (stg == "steps")
            add_feat = false;
        else if (stg == "living_street")
            add_feat = false;
        else if (stg == "trunk")
            add_feat = true;
        else if (stg == "trunk_link")
            add_feat = true;
        else if (stg == "motorway_link")
            add_feat = true;
        else if (stg == "tertiary_link")
            add_feat = false;
        else if (stg == "motorway")
            add_feat = true;
        else if (stg == "path")
            add_feat = false;
        else if (stg == "cycleway")
            add_feat = false;
        else
            add_feat = true;
    }
    return add_feat;
}

#endif // highway filter 

////////////////////////////////////////////////////////////
// output of information from osm file loaded
// copy what is needed into a features structure
//
///////////////////////////////////////////////////////////
int use_data()
{
    int iret = 0;
    size_t ii, max = vWays.size();
#ifdef USE_STD_MAP
    mNODESi iter, iend = mNodes.end();
#else // !USE_STD_MAP
    size_t i2, max2 = vNodes.size();
    PNODEM pn;
#endif // USE_STD_MAP y/n
    size_t i3, max3;
    size_t i4, max4;
    PWAYM pw;
    PNDM pndm;
    APT apt;
    PFEAT pf = &feat;
    char *ptmp;
    bool add_feat;
    double lat, lon;
    std::string name, ref;
    ANNO a;
    max3 = 0;
    for (ii = 0; ii < max; ii++) {
        pw = &vWays[ii];    // use_data(): convert OSM file input to a FEAT struct - color wpt anno comment
        max3 += pw->vNDm.size();
    }
    SPRTF("%s: Have collects %d ways, with %d nd's, to apply to %d nodes\n", module,
        (int)max, (int)max3, (int)xStats.node_count);
    for (ii = 0; ii < max; ii++) {
        pw = &vWays[ii];    // use_data(): convert OSM file input to a FEAT struct - color wpt anno comment
        max3 = pw->vNDm.size();
        max4 = pw->vTags.size();
        clean_feat(pf);
        add_feat = true;
        for (i3 = 0; i3 < max3; i3++) {
            pndm = &pw->vNDm[i3];
#ifdef USE_STD_MAP
            iter = mNodes.find(pndm->ulref); // use_data(): lookup Way ref to get node
            if (iter == iend) {
                SPRTF("pn ref=%ld NOT found!\n", pndm->ulref);
            } else {
                apt = (*iter).second;
                pf->vPoints.push_back(apt);
            }
#else // !USE_STD_MAP
            for (i2 = 0; i2 < max2; i2++) {
                pn = &vNodes[i2];
                if (pndm->ulref == pn->ulid) {
                    break;
                }
            }
            if (i2 < max2) {
                // found our NODE
                apt.lat = pn->lat;
                apt.lon = pn->lon;
                apt.ulid = pndm->ulref;  // KEEP the ID of this point
                pf->vPoints.push_back(apt);
            } else {
                SPRTF("pn ref=%ld NOT found!\n", pndm->ulref);
            }
#endif // USE_STD_MAP y/n
        }

        // this is already done in getting <way ... <tag ...
        name = "";
        ref = "";
        if (verbose_xg && max4) {
            std::string stg;
            ptmp = GetNxtBuf();
            // add_feat = false;
            vMAPSS tags; // std::map<std::string,std::string> tags;
            for (i4 = 0; i4 < max4; i4++) {
                PTAG pt = &pw->vTags[i4];
                sprintf(ptmp,"tag k='%s' v='%s'", pt->k.c_str(), pt->v.c_str());
                pf->comments.push_back(ptmp);
                tags[pt->k] = pt->v;
            }
            iMAPSS names = tags.find("name");
            if (names != tags.end()) {
                name = names->second;
            }
            iMAPSS refs = tags.find("ref");
            if (refs != tags.end()) {
                ref = refs->second;
            }
            iMAPSS cables = tags.find("cables");
            if (cables != tags.end()) {
                add_feat = false;
            }

#ifdef USE_HIGHWAY_FILTER
            iMAPSS hwy = tags.find("highway");
            if (hwy != tags.end()) {
                add_feat |= highway_filter( tags );
            }
// #if 0 // 0000000000000000000000000000000000000000000000
#else
            iMAPSS hwy = tags.find("highway");
            if (hwy != tags.end()) {
                stg = hwy->second;
                if (stg == "track")
                    add_feat = false;
                else if (stg == "tertiary")
                    add_feat = false;
                else if (stg == "residential")
                    add_feat = false;
                else if (stg == "footway")
                    add_feat = false;
                else if (stg == "unclassified")
                    add_feat = false;
                else if (stg == "pedestrian")
                    add_feat = false;
                else if (stg == "service")
                    add_feat = false;
                else if (stg == "steps")
                    add_feat = false;
                else if (stg == "living_street")
                    add_feat = false;
                else if (stg == "trunk")
                    add_feat = true;
                else if (stg == "trunk_link")
                    add_feat = true;
                else if (stg == "motorway_link")
                    add_feat = true;
                else if (stg == "tertiary_link")
                    add_feat = false;
                else if (stg == "motorway")
                    add_feat = true;
                else if (stg == "path")
                    add_feat = false;
                else if (stg == "cycleway")
                    add_feat = false;
                else if (stg == "raceway")
                    add_feat = false;
                else
                    add_feat = true;
            }
#endif // USE_HIGHWAY_FILTER y/n

            iMAPSS admin = tags.find("boundary");
            if (admin != tags.end()) {
                add_feat = false;
            }
            iMAPSS land = tags.find("landuse");
            if (land != tags.end()) {
                add_feat = false;
            }
            iMAPSS water = tags.find("waterway");
            if (water != tags.end()) {
                add_feat = false;
            }
            iMAPSS amen = tags.find("amenity");
            if (amen != tags.end()) {
                add_feat = false;
            }
            iMAPSS leis = tags.find("leisure");
            if (leis != tags.end()) {
                add_feat = false;
            }
            iMAPSS rail = tags.find("railway");
            if (rail != tags.end()) {
                add_feat = false;
            }
            iMAPSS pow = tags.find("power");
            if (pow != tags.end()) {
                add_feat = false;
            }
            // # tag k='location' v='underground'
            // # tag k='man_made' v='pipeline'
            // # tag k='type' v='gas'
            iMAPSS loc = tags.find("location");
            if (loc != tags.end()) {
                add_feat = false;
            }

            // building - some are significant
            iMAPSS bld = tags.find("building");
            if (bld != tags.end()) {
                add_feat = false;
            }

        }
        //////////////////////////////////////////
        // feature filter - should this be added to output?
        if (add_feat) {
            // ADD a single FEAT - set of wpts, with names...
            double mnlon = 400;
            double mnlat = 400;
            double mxlon = -400;
            double mxlat = -400;
            double bgnlat = 400, bgnlon = 400;
            max3 = pf->vPoints.size();  // pf->vPoints.push_back(apt);
            for (i3 = 0; i3 < max3; i3++) {
                apt = pf->vPoints[i3];
                lat = apt.lat;
                lon = apt.lon;
                if (lat < mnlat)
                    mnlat = lat;
                if (lon < mnlon)
                    mnlon = lon;
                if (lat > mxlat)
                    mxlat = lat;
                if (lon > mxlon)
                    mxlon = lon;
                if (i3 == 0) {
                    bgnlat = lat;
                    bgnlon = lon;
                }
            }
            if (name.size() > 0) {
                a.pt.lat = bgnlat;
                a.pt.lon = bgnlon;
                a.anno = name;
                if (ref.size() > 0) {
                    size_t pos = name.find(ref);
                    if (pos == std::string::npos) {
                        a.anno += " ";
                        a.anno += ref;
                    }
                }
                pf->vAnnos.push_back(a);
            }
            vFeats.push_back(*pf);
        }
    }
    //////////////////////////////////////////////////////////
    // Chosen what to add to xg, like some anno
    iret |= output_feats( vFeats, out_file, verbose_xg );
    //////////////////////////////////////////////////////////
    return iret;
}


int add_node(char *xbgn,char *xend)
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    NODEM n;
#ifndef USE_STD_MAP
    size_t sz = vNodes.size();
#endif // !USE_STD_MAP
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'i') {
                n.ulid = strtoul(cp,0,10);
                valid |= v_id;
            } else if (c == 'l') {
                if (kp[1] == 'a') {
                    n.lat = atof(cp);
                    valid |= v_lat;
                } else if (kp[1] == 'o') {
                    n.lon = atof(cp);
                    valid |= v_lon;
                }
            }
        }
        if (valid == v_nodem)
            break;
    }
    if (valid == v_nodem) {
#ifdef USE_STD_MAP
        APT apt;
        apt.lat = n.lat;
        apt.lon = n.lon;
        mNodes[n.ulid] = apt;
#else // !USE_STD_MAP
        vNodes.push_back(n);
        last_node = &vNodes[sz];
#endif // USE_STD_MAP y/n
    }
    return iret;
}

int add_way( char *xbgn, char *xend )
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    WAYM w;
    size_t sz = vWays.size();
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'i') {
                w.ulid = strtoul(cp,0,10);
                vWays.push_back(w);
                last_way = &vWays[sz];
                break;
            }
        }
    }
    return iret;

}

int add_nd( char *xbgn, char *xend )
{
    if (last_xml_item != xml_way)
        return 1;
    if (!last_way)
        return 1;
    int i, c, iret;
    PXMLTAGS pxt = xmlTags;
    NDM ndm;
    char *kp, *vp;
    iret = get_xml_tags(xbgn,xend);
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        vp = pxt[i].val;
        if (kp && vp) {
            c = *kp;
            if (c == 'r') {
                ndm.ulref = strtoul(vp,0,10);
                last_way->vNDm.push_back(ndm);
                break;
            }
        }
    }
    return iret;
}

/* ==============================================================

 <way id="166829076" visible="true" version="2" changeset="18394571" timestamp="2013-10-16T22:02:59Z" user="josmougn" uid="522742">
  <nd ref="1782630994"/>
  <nd ref="2498917253"/>
  <nd ref="1782631035"/>
  <nd ref="2498917257"/>
  <nd ref="1782631031"/>
  <tag k="highway" v="footway"/>
  <tag k="source" v="GPS"/>
 </way>
   ============================================================== */
#define v_tag (v_k|v_v)
int add_tag( char *xbgn, char *xend )
{
    int iret = 0;
    if (last_xml_item != xml_way)
        return 1;
    if (!last_way)
        return 1;
    int i, c;
    PXMLTAGS pxt = xmlTags;
    char *kp, *vp;
    int validtag = 0;
    iret = get_xml_tags(xbgn,xend);
    TAG tag;
    char *ptmp = GetNxtBuf();
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        vp = pxt[i].val;
        if (kp && vp) {
            c = *kp;
            if (c == 'k') {
                strcpy2qt(ptmp,vp,xend);
                tag.k = ptmp;
                validtag |= v_k;
            } else if (c == 'v') {
                strcpy2qt(ptmp,vp,xend);
                tag.v = ptmp;
                validtag |= v_v;
            }
        }
        if (validtag == v_tag) {
            break;
        }
    }

    if (validtag == v_tag) {
        // add <tag k="key" v="val" /> to last (open) way
        last_way->vTags.push_back(tag);
    }


    return iret;
}

// <bounds minlat="41.5596000" minlon="1.6171000" maxlat="41.6075000" maxlon="1.6897000"/>
static double g_minlat = 400;
static double g_minlon = 400;
static double g_maxlat = -400;
static double g_maxlon = -400;
static std::string xg_bbox;

int add_bounds( char *xbgn, char *xend )
{
    int i, iret = 0;
    PXMLTAGS pxt = xmlTags;
    char *kp, *vp;
    char *pminlat = 0;
    char *pminlon = 0;
    char *pmaxlat = 0;
    char *pmaxlon = 0;
    double d;
    iret = get_xml_tags(xbgn,xend);
    double minlat = 400;
    double minlon = 400;
    double maxlat = -400;
    double maxlon = -400;
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        vp = pxt[i].val;
        d = atof(vp);
        if (strncmp(kp,"minlon=", 7) == 0) {
            if (d < minlon) {
                minlon = d;
                pminlon = vp;
            }
        } else if (strncmp(kp,"minlat=", 7) == 0) {
            if (d < minlat) {
                minlat = d;
                pminlat = vp;
            }
        } else if (strncmp(kp,"maxlat=", 7) == 0) {
            if (d > maxlat) {
                maxlat = d;
                pmaxlat = vp;
            }
        } else if (strncmp(kp,"maxlon=", 7) == 0) {
            if (d > maxlon) {
                maxlon = d;
                pmaxlon = vp;
            }
        }

    }
    if (in_world_range( minlat, minlon ) &&
        in_world_range( maxlat, maxlon )) {
        if (minlon < g_minlon)
            g_minlon = minlon;
        if (minlat < g_minlat)
            g_minlat = minlat;
        if (maxlon > g_maxlon)
            g_maxlon = maxlon;
        if (maxlat > g_maxlat)
            g_maxlat = maxlat;
        xg_bbox = get_xg_bbox( g_minlon, g_minlat, g_maxlon, g_maxlat );
    }

    return iret;
}

//////////////////////////////////////////////////////////////
// Have an xml "<...>"
/////////////////////////////////////////////////////////////
int process_xml( char *xbgn, char *xend )
{
    int iret = 0;
    PXSTATS pxs = &xStats;
    char *tmp;
    char c1 = xbgn[1];
    pxs->xml_items++;
    if (c1 == '?') {
        // header
        last_xml_item = xml_none;
        return iret;
    } else if (c1 == '/') {
        // close
        last_xml_item = xml_none;
        return iret;
    } else if (c1 == 'o') {
        // osm
        last_xml_item = xml_none;
        return iret;
    }
    char c2 = xbgn[2];
    char c3 = xbgn[3];
    if (c1 == 'n') {
        if (c2 == 'o') {
            // node
            pxs->node_count++;
            iret = add_node(xbgn,xend);
            if (!isxmlend)
                last_xml_item = xml_node;
        } else if (c2 == 'd') {
            // nd
            iret = add_nd(xbgn,xend);
            pxs->nd_cnt++;
        } else {
            tmp = GetNxtBuf();
            sprintf(tmp,"What is this '%c'", (char)c1);
            check_me(tmp);
        }
    } else if (c1 == 't') {
        // tag
        pxs->tag_count++;
        if (verbose_xg) {
            //iret = add_tag(xbgn,xend);
            add_tag(xbgn,xend);
        }
    } else if (c1 == 'w') {
        // way
        pxs->way_cnt++;
        iret = add_way(xbgn,xend);
        if (!isxmlend)
            last_xml_item = xml_way;
    } else if (c1 == 'r') {
        // relation
        pxs->relat_cnt++;
    } else if (c1 == 'm') {
        // member
        pxs->memb_cnt++;
    } else if (c1 == 'b') {
        // <bounds minlat="41.5596000" minlon="1.6171000" maxlat="41.6075000" maxlon="1.6897000"/>
        // bounds
        iret = add_bounds(xbgn,xend);
    } else if (c1 == 'c') {
        // changeset
        pxs->chset_count++;
    } else {
        tmp = GetNxtBuf();
        sprintf(tmp,"What is this '%c'", (char)c1);
        check_me(tmp);
    }

    return iret;
}

void clean_up()
{
    double bgns = get_seconds();
#ifdef USE_STD_MAP
    mNodes.clear();
#else // !USE_STD_MAP
    vNodes.clear();
#endif // USE_STD_MAP y/n
    size_t ii,max = vWays.size();
    PWAYM pwm;
    PFEAT pf;
    for (ii = 0; ii < max; ii++) {
        pwm = &vWays[ii];
        pwm->vNDm.clear();
    }
    vWays.clear();
    max = vFeats.size();
    for (ii = 0; ii < max; ii++) {
        pf = &vFeats[ii];
        pf->vPoints.clear();
#ifdef USE_NEW_FEAT
        pf->vAnnos.clear();
#else
        pf->anno.clear();
        pf->comments.clear();
#endif
    }
    vFeats.clear();
    SPRTF("%s: Clean up took %s\n", module, get_seconds_stg( get_seconds() - bgns ));
}



int process_buffer(char *text, size_t len)
{
    int iret = 0;
    int c, pc;
    size_t i;
    int inxml = 0;
    char *xbgn;
    char *xend;
    for (i = 0; i < len; i++) {
        c = text[i];
        if (inxml) {
            if (c == '>') {
                xend = &text[i];
                inxml = 0;
                isxmlend = (pc == '/') ? true : false;
                iret = process_xml( xbgn, xend );
            }
            pc = c;
        } else if (c == '<') {
            xbgn = &text[i];
            inxml = 1;
        }
    }

    return iret;
}

// eof = osm2xgf.cxx
