/*\
 * osm2xgr.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
    // OK, simple problem - used atoi(val) to get the 'id', 'ref' and 'changeset'
    // Changed ALL these to like 'unsigned long ulid', etc, and used strtoul(val,0,10)
    // AND EVERY THING WORKED PERFECTLY.
    // **************************************************************************
    // This uses a simpler parser, http://rapidxml.sourceforge.net/ - just a header,
    // but you have to load whole file into memory, and keep that memory until the end,
    // but since I used the same structures with like int id = atoi(val), I came
    // up with the SAME problem. It was in that code that I discovered this
    // string-to-value conversion error. Now both apps work fine...
    // **************************************************************************
    // BUT ANYWAY NEED ANOTHER STRATEGY TO DEAL WITH THE GIANT planet.osm
    // It weighs in at 300-500 GB, or more, just in most systems can NOT
    // allocate that much memory to load the whole file. Need to read and 
    // parse the OSM XML buffer by buffer.
    // *************************************************************************
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <iostream>
#ifdef _MSC_VER
#include <conio.h>
#else
#include <string.h> // for strcpy(), ...
#endif
#include <time.h>
#include <sstream>
#include <math.h>
#include <rapidxml/rapidxml.hpp>	// other includes
#include <rapidxml/rapidxml_print.hpp>
#include <limits.h>
#include "sprtf.hxx"
#include "utils.hxx"
#include "osm2xg.hxx"

#ifndef SPRTF
#define SPRTF printf
#endif

#define M2NM 0.0005399568   // meters to nm

using namespace rapidxml;
//using namespace std;
#define MEOL std::endl

static const char *module = "osm2xg";

// options
static bool verbose_xg = false;
static bool add_each_file = false;
static std::string base = "temp";
static std::string out_file = "tempxgr.xg";
static int verbosity = 1;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static int check_me( const char *msg )
{
    int i = SPRTF("CHECK ME: %s\n",msg);
#ifdef _MSC_VER
    if (!bIsRedON()) {
        //_kbhit();
        SPRTF("Any key to continue!\n");
        _getch();
    }
#endif
    return i;
}

static vNODE vNodes;
static vWAY vWays;
static vMEMBS vMembs;
static vRELATS vRelats;
static vFEATS vFeats;

static TAG tag;
static ND nd;
static NODE node;
static WAY way;
static RELAT relat;
static MEMB memb;

void clean_node( PNODE pn )
{
    pn->ulid = -1; //="133840431" 
    pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
    pn->uluid = 0;    // ="79144"
    pn->user = "";   //="John H" 
    pn->visible = false;   //="true" 
    pn->version = 0;    //="5" 
    pn->ulchangeset = 0;  //="6015441" 
    pn->lat = BAD_LL; // ="-31.7156299" 
    pn->lon = BAD_LL; //="148.6700669" 
    pn->valid = 0;
    pn->vTags.clear();
    pn->used = 0;
}

void clean_way( PWAY pn )
{
    pn->ulid = -1; //="133840431" 
    pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
    pn->uluid = 0;    // ="79144"
    pn->user = "";   //="John H" 
    pn->visible = false;   //="true" 
    pn->version = 0;    //="5" 
    pn->ulchangeset = 0;  //="6015441" 
    pn->valid = 0;
    pn->vTags.clear();
    pn->vRefs.clear();
}
void clean_tag( PTAG pt )
{
    pt->k.clear();
    pt->v.clear();
}
void clean_nd( PND pnd )
{
    pnd->ulref = -1;
    // pnd->vTags.clear(); // no tags belong to the 'way'
}
void clean_memb( PMEMB pm )
{
    pm->ulref = -1;
    pm->type = typ_none;
    pm->role = "";
}

void clean_relat( PRELAT pn )
{
    pn->ulid = -1; //="133840431" 
    pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
    pn->uluid = 0;    // ="79144"
    pn->user = "";   //="John H" 
    pn->visible = false;   //="true" 
    pn->version = 0;    //="5" 
    pn->ulchangeset = 0;  //="6015441" 
    pn->valid = 0;
    pn->vMems.clear();
    pn->vTags.clear();
}

// ==============================================================
// services
// 01234567890123456789 
// 2010-10-11T19:55:07Z
// 12345678901234
// 20101011195507
static const char *bgn_epoch = "1970-01-01_00:00:00";
static const char *form = "%Y-%m-%dT%H:%M:%SZ";
bool get_timestamp( char *in, time_t *pts )
{
    char *cp = GetNxtBuf();
    strcpy(cp,in);
    size_t len = strlen(cp);
    const char *msg = "";
    struct tm t;
    memset(&t,0,sizeof(t));
    time_t val;
    char *tp;
    if ((len >= 20) &&
        (cp[4] == '-') && (cp[7] == '-') && (cp[10] == 'T') && (cp[13] == ':') && (cp[16] == ':') && (cp[19] == 'Z')) 
    {
        tp = cp;
        cp[4] = 0;
        t.tm_year = atoi(tp);
        if (t.tm_year >= 1900) {
            t.tm_year -= 1900;
        } else {
            msg = "YR OOR";
            goto FAILED;
        }

        tp = &cp[5];
        cp[7] = 0;
        t.tm_mon = atoi(tp);
        if (t.tm_mon > 0) {
            t.tm_mon -= 1;
        } else {
            msg = "MTH OOR";
            goto FAILED;
        }

        tp = &cp[8];
        cp[10] = 0;
        t.tm_mday = atoi(tp);

        tp = &cp[11];
        cp[13] = 0;
        t.tm_hour = atoi(tp);

        tp = &cp[14];
        cp[16] = 0;
        t.tm_min = atoi(tp);

        tp = &cp[17];
        cp[19] = 0;
        t.tm_sec = atoi(tp);

        val = mktime(&t);
        if (val == -1) {
            msg = "mktime FAILED";
            goto FAILED;
        }
        *pts = val;
#ifdef ADD_STRFTIME_TEST
        *cp = 0;
        c = strftime(cp,1024,form,&t);
        if (strcmp(cp,in) == 0) {
            return true;
        } else {
            SPRTF("\nORG = '%s'\nNEW = '%s'\n", in, cp);
            msg = "strftime FAILED";
        }
#else
        return true;
#endif

    } else {
        msg = "BAD FORM";
    }

FAILED:
    sprintf(cp,"Date '%s' FAILED! %s *** FIX ME ***\n", in, msg);
    check_me(cp);
    return false;
}

////////////////////////////////////////////////////////////////////
// add_node(node), all atrributes first, then any children
// <node id='113727586' timestamp='2007-11-14T12:09:42Z' uid='10359' user='swampwallaby' visible='true' version='1' changeset='510260' lat='-31.8316667' lon='149.0011111'>
//    <tag k='created_by' v='JOSM' />
//////////////////////////////////////////////////////////////////
PNODE add_node(xml_node<> *n)
{
    char *name, *val;
    size_t sz;
    PNODE pn = &node;
    clean_node(pn);
    std::stringstream s;
    if (VERB5) {
        s << " <node";
    }
    for (xml_attribute<> *attr = n->first_attribute(); attr; attr = attr->next_attribute()) {
        name = attr->name();
        val  = attr->value();
        if (VERB5) {
            s << " ";
            s << name;
            s << "=\"";
            s << val;
            s << "\"";
        }
        if (strcmp(name,"id") == 0) {
            //pn->id = -1; //="133840431" 
            pn->ulid = strtoul(val,0,10);
            pn->valid |= v_id;
        } else
        if (strcmp(name,"timestamp") == 0) {
            //pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
            //pn->timestamp = strftime(tmp,len,form,&tm);
            if (get_timestamp( val, &pn->timestamp )) {
                pn->valid |= v_ts;
            }
        } else
        if (strcmp(name,"uid") == 0) {
            //pn->uid = 0;    // ="79144"
            pn->uluid = strtoul(val,0,10);
            pn->valid |= v_uid;
        } else
        if (strcmp(name,"user") == 0) {
            //pn->user = "";   //="John H" 
            pn->user = val;
            pn->valid |= v_user;
        } else
        if (strcmp(name,"visible") == 0) {
            //pn->visible = false;   //="true" 
            if (strcmp(val,"true") == 0) {
                pn->visible = true;
                pn->valid |= v_vis;
            } else if (strcmp(val,"false") == 0) {
                pn->visible = false;
                pn->valid |= v_vis;
            }
        } else
        if (strcmp(name,"version") == 0) {
            //pn->version = 0;    //="5" 
            pn->version = atoi(val);
            pn->valid |= v_vers;
        } else
        if (strcmp(name,"changeset") == 0) {
            //pn->changeset = 0;  //="6015441" 
            pn->ulchangeset = strtoul(val,0,10);
            pn->valid |= v_chg;
        } else
        if (strcmp(name,"lat") == 0) {
            //pn->lat = BAD_LL; // ="-31.7156299" 
            pn->lat = atof(val);
            if (valid_lat(pn->lat))
                pn->valid |= v_lat;
        } else
        if (strcmp(name,"lon") == 0) {
            //pn->lon = BAD_LL; //="148.6700669" 
            pn->lon = atof(val);
            if (valid_lon(pn->lon))
                pn->valid |= v_lon;
        } else {
            char *tmp = GetNxtBuf();
            sprintf(tmp,"WARNING: Unused %s=\"%s\"\n", name,val);
            check_me(tmp);
        }
    }
    // check for children
    xml_node<> *child = n->first_node();
    if (child) {
        if (VERB5) {
            s << ">";
            SPRTF("%s\n", s.str().c_str());
            s.str("");
        }
        char *cn;
        for ( ; child; child = child->next_sibling() ) {
            cn = child->name();
            if (strcmp(cn,"tag") == 0) {
                PTAG pt = &tag;
                clean_tag(pt);
                if (VERB5) {
                    s << "  <" << cn;
                }
                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                    name = attr->name();
                    val  = attr->value();
                    if (VERB5) {
                        s << " ";
                        s << name;
                        s << "=\"";
                        s << val;
                        s << "\"";
                    }
                    if (strcmp(name,"k") == 0) {
                        pt->k = val;
                    } else
                    if (strcmp(name,"v") == 0) {
                        pt->v = val;
                    } else {
                        char *tmp = GetNxtBuf();
                        sprintf(tmp,"4:Uncoded attr '%s' of a 'tag'! *** FIX ME ***", name);
                        check_me(tmp);
                    }
                }
                if (VERB5) {
                    s << " />" << MEOL;
                }
                // done 'tag' attributes of 'node'
                if (child->first_node()) {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Warning: 'tag' with children! *** CHECK ME ***");
                    check_me(tmp);
                }
                if (pt->k.size() && pt->v.size()) {
                    pn->vTags.push_back(*pt);
                } else {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Failed 'tag' attr! k='%s' v='%s' *** CHECK ME ***", pt->k.c_str(), pt->v.c_str());
                    check_me(tmp);
                }
            } else {
                char *tmp = GetNxtBuf();
                sprintf(tmp,"5:Uncoded child '%s' of a 'node'! *** FIX ME ***", cn);
                check_me(tmp);
            }
        }
        if (VERB5) {
            s << " </node>";
        }
    } else {
        if (VERB5) {
            s << " />";
        }
    }
    if ((pn->valid & v_all) == v_all) {
        sz = vNodes.size();
        vNodes.push_back(*pn);
        pn = &vNodes[sz];
    } else {
        int valid = (pn->valid & v_all);
        std::string s1 = getFlagNames( valid );
        std::string s2 = getMissingBits( valid, v_all );
        SPRTF("FAILED: Got %s, missed %s\n", s1.c_str(), s2.c_str());
        check_me("FAILED NODE");
        pn = 0;
    }
    if (VERB5) {
        SPRTF("%s\n", s.str().c_str());
    } else {
        sz = vNodes.size();
        if ((sz % 10000) == 0) {
            SPRTF("%d nodes\n", (int) sz);
        }
    }

    return pn;
}

/////////////////////////////////////////////////////////////
// add_way(node), attributes and children
////////////////////////////////////////////////////////////
PWAY add_way(xml_node<> *n)
{
    char *name, *val;
    size_t sz;
    PWAY pw = &way;
    clean_way(pw);
    std::stringstream s;
    if (VERB5) {
        s << " <way";
    }
    int way_nd_count = 0;
    int way_tag_count = 0;
    for (xml_attribute<> *attr = n->first_attribute(); attr; attr = attr->next_attribute()) {
        name = attr->name();
        val  = attr->value();
        if (VERB5) {
            s << " ";
            s << name;
            s << "=\"";
            s << val;
            s << "\"";
        }
        if (strcmp(name,"id") == 0) {
            //pn->id = -1; //="133840431" 
            pw->ulid = strtoul(val,0,10);
            pw->valid |= v_id;
        } else
        if (strcmp(name,"timestamp") == 0) {
            //pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
            //pn->timestamp = strftime(tmp,len,form,&tm);
            if (get_timestamp( val, &pw->timestamp )) {
                pw->valid |= v_ts;
            }
        } else
        if (strcmp(name,"uid") == 0) {
            //pn->uid = 0;    // ="79144"
            pw->uluid = strtoul(val,0,10);
            pw->valid |= v_uid;
        } else
        if (strcmp(name,"user") == 0) {
            //pn->user = "";   //="John H" 
            pw->user = val;
            pw->valid |= v_user;
        } else
        if (strcmp(name,"visible") == 0) {
            //pn->visible = false;   //="true" 
            if (strcmp(val,"true") == 0) {
                pw->visible = true;
                pw->valid |= v_vis;
            } else if (strcmp(val,"false") == 0) {
                pw->visible = false;
                pw->valid |= v_vis;
            }
        } else
        if (strcmp(name,"version") == 0) {
            //pn->version = 0;    //="5" 
            pw->version = atoi(val);
            pw->valid |= v_vers;
        } else
        if (strcmp(name,"changeset") == 0) {
            //pn->changeset = 0;  //="6015441" 
            pw->ulchangeset = strtoul(val,0,10);
            pw->valid |= v_chg;
        } else {
            char *tmp = GetNxtBuf();
            sprintf(tmp,"WARNING: Unused %s=\"%s\"\n", name,val);
            check_me(tmp);
        }
    }

    // ============================================
    // check for children of 'way' - 'nd' and 'tag'
    // ============================================
    xml_node<> *child = n->first_node();
    if (child) {
        unsigned long nd_ref;
        if (VERB5) {
            s << ">";
            SPRTF("%s\n", s.str().c_str());
            s.str("");
        }
        char *cn;
        for ( ; child; child = child->next_sibling() ) {
            cn = child->name();
            // 'way' 'tag' child
            if (strcmp(cn,"tag") == 0) {
                PTAG pt = &tag;
                clean_tag(pt);
                way_tag_count++;
                s << "  <" << cn;
                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                    name = attr->name();
                    val  = attr->value();
                    if (VERB5) {
                        s << " ";
                        s << name;
                        s << "=\"";
                        s << val;
                        s << "\"";
                    }
                    if (strcmp(name,"k") == 0) {
                        pt->k = val;
                    } else
                    if (strcmp(name,"v") == 0) {
                        pt->v = val;
                    } else {
                        char *tmp = GetNxtBuf();
                        sprintf(tmp,"6:Uncoded attr '%s' of a 'tag'! *** FIX ME ***", name);
                        check_me(tmp);
                    }
                }
                if (VERB5) {
                    s << " />" << MEOL;
                }
                // done 'tag' attributes of 'way'
                if (child->first_node()) {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Warning: 'tag' with children! *** CHECK ME ***");
                    check_me(tmp);
                }
                if (pt->k.size() && pt->v.size()) {
                    pw->vTags.push_back(*pt);
                } else {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Failed 'tag' attr! k='%s' v='%s' *** CHECK ME ***", pt->k.c_str(), pt->v.c_str());
                    check_me(tmp);
                }
            } else
            if (strcmp(cn,"nd") == 0) {
                // 'way' 'nd' child
                PND pnd = &nd;
                clean_nd(pnd);
                way_nd_count++;
                if (VERB5) {
                    s << "  <" << cn;
                }
                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                    name = attr->name();
                    val  = attr->value();
                    if (VERB5) {
                        s << " ";
                        s << name;
                        s << "=\"";
                        s << val;
                        s << "\"";
                    }
                    if (strcmp(name,"ref") == 0) {
                        nd_ref = strtoul(val,0,10);
                        if (nd_ref == 2147483647) {
                            check_me("Got nd ref = 2147483647!!!");
                        }
                        pnd->ulref = nd_ref;
                    } else {
                        char *tmp = GetNxtBuf();
                        sprintf(tmp,"7:Uncoded attr '%s' of a 'nd'! *** FIX ME ***", name);
                        check_me(tmp);
                    }
                }
                // done 'nd' of a 'way'
                if (VERB5) {
                    s << " />" << MEOL;
                }
                if (pnd->ulref == -1) {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Failed 'nd' of a 'way'! *** CHECK ME ***");
                    check_me(tmp);
                } else {
                    pw->vRefs.push_back(*pnd);
                }
            } else {
                char *tmp = GetNxtBuf();
                sprintf(tmp,"8:Uncoded child '%s' of a 'nd'! *** FIX ME ***", cn);
                check_me(tmp);
            }
        }
        if (VERB5) {
            s << " </way>";
        }
    } else {
        if (VERB5) {
            s << " />";
        }
    }
    if ((pw->valid & v_way) == v_way) {
        sz = vWays.size();
        vWays.push_back(*pw);
        pw = &vWays[sz];
    } else {
        SPRTF("FAILED: ");
        check_me("FAILED WAY");
        pw = 0;
    }
    if (VERB5) {
        // can be too long for SPRTFSPRTF("%s\n", s.str().c_str());
        s << MEOL;
        direct_out_it((char *)s.str().c_str());
    } else {
        sz = vWays.size();
        if ((sz % 10000) == 0) {
            SPRTF("%d tags\n", (int)sz);
        }
    }
    return pw;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// add_relation(node) - attributes and children
//  <relation id="207473" timestamp="2014-03-12T03:13:59Z" uid="46482" user="Leon K" visible="true" version="103" changeset="21056894">
//   <member type="way" ref="192435813" role="link" />
//   <member type="way" ref="127469919" role="link" />
//   <member type="way" ref="173385464" role="" />
//   <member type="way" ref="4725178" role="" />
//   <member type="way" ref="127295004" role="forward" />
//   <tag k="addr:country" v="AU" />
//   <tag k="addr:state" v="NSW" />
//  </relation>

PRELAT add_relation(xml_node<> *n)
{
    char *name, *val;
    size_t sz;
    PRELAT pr = &relat;
    clean_relat(pr);
    std::stringstream s;
    if (VERB5) {
        s << " <relation";
    }
    for (xml_attribute<> *attr = n->first_attribute(); attr; attr = attr->next_attribute()) {
        name = attr->name();
        val  = attr->value();
        if (VERB5) {
            s << " ";
            s << name;
            s << "=\"";
            s << val;
            s << "\"";
        }
        if (strcmp(name,"id") == 0) {
            //pn->id = -1; //="133840431" 
            pr->ulid = strtoul(val,0,10);
            pr->valid |= v_id;
        } else
        if (strcmp(name,"timestamp") == 0) {
            //pn->timestamp = 0; //="2010-10-11T19:55:07Z" 
            //pn->timestamp = strftime(tmp,len,form,&tm);
            if (get_timestamp( val, &pr->timestamp )) {
                pr->valid |= v_ts;
            }
        } else
        if (strcmp(name,"uid") == 0) {
            //pn->uid = 0;    // ="79144"
            pr->uluid = strtoul(val,0,10);
            pr->valid |= v_uid;
        } else
        if (strcmp(name,"user") == 0) {
            //pn->user = "";   //="John H" 
            pr->user = val;
            pr->valid |= v_user;
        } else
        if (strcmp(name,"visible") == 0) {
            //pn->visible = false;   //="true" 
            if (strcmp(val,"true") == 0) {
                pr->visible = true;
                pr->valid |= v_vis;
            } else if (strcmp(val,"false") == 0) {
                pr->visible = false;
                pr->valid |= v_vis;
            }
        } else
        if (strcmp(name,"version") == 0) {
            //pn->version = 0;    //="5" 
            pr->version = atoi(val);
            pr->valid |= v_vers;
        } else
        if (strcmp(name,"changeset") == 0) {
            //pn->changeset = 0;  //="6015441" 
            pr->ulchangeset = strtoul(val,0,10);
            pr->valid |= v_chg;
        } else {
            char *tmp = GetNxtBuf();
            sprintf(tmp,"WARNING: Unused %s=\"%s\"\n", name,val);
            check_me(tmp);
        }
    }

    // deal with children
    xml_node<> *child = n->first_node();
    if (child) {
        if (VERB5) {
            s << ">";
            SPRTF("%s\n", s.str().c_str());
            s.str("");
        }
        char *cn;
        for ( ; child; child = child->next_sibling() ) {
            cn = child->name();
            if (strcmp(cn,"tag") == 0) {
                PTAG pt = &tag;
                clean_tag(pt);
                if (VERB5) {
                    s << "  <" << cn;
                }
                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                    name = attr->name();
                    val  = attr->value();
                    if (VERB5) {
                        s << " ";
                        s << name;
                        s << "=\"";
                        s << val;
                        s << "\"";
                    }
                    if (strcmp(name,"k") == 0) {
                        pt->k = val;
                    } else
                    if (strcmp(name,"v") == 0) {
                        pt->v = val;
                    } else {
                        char *tmp = GetNxtBuf();
                        sprintf(tmp,"9:Uncoded attr '%s' of a 'tag'! *** FIX ME ***", name);
                        check_me(tmp);
                    }
                }
                if (VERB5) {
                    s << " />" << MEOL;
                }
                // done 'tag' attributes of 'way'
                if (child->first_node()) {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Warning: 'tag' with children! *** CHECK ME ***");
                    check_me(tmp);
                }
                if (pt->k.size() && pt->v.size()) {
                    pr->vTags.push_back(*pt);
                } else {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Failed 'tag' attr! k='%s' v='%s' *** CHECK ME ***", pt->k.c_str(), pt->v.c_str());
                    check_me(tmp);
                }
            } else
            if (strcmp(cn,"member") == 0) {
                PMEMB pm = &memb;
                clean_memb(pm);
                if (VERB5) {
                    s << "  <" << cn;
                }
                for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                    name = attr->name();
                    val  = attr->value();
                    if (VERB5) {
                        s << " ";
                        s << name;
                        s << "=\"";
                        s << val;
                        s << "\"";
                    }
                    if (strcmp(name,"ref") == 0) {
                        pm->ulref = strtoul(val,0,10);
                    } else 
                    if (strcmp(name,"type") == 0) {
                        if (strcmp(val,"way") == 0) {
                            pm->type = typ_way;
                        } else
                        if (strcmp(val,"node") == 0) {
                            pm->type = typ_node;
                        } else
                        if (strcmp(val,"relation") == 0) {
                            pm->type = typ_relat;
                        } else {
                            char *tmp = GetNxtBuf();
                            sprintf(tmp,"1:Uncoded type '%s' of a 'relation'! *** FIX ME ***", val);
                            check_me(tmp);
                        }

                    } else
                    if (strcmp(name,"role") == 0) {
                        pm->role = val;
                    } else {
                        char *tmp = GetNxtBuf();
                        sprintf(tmp,"2:Uncoded attr '%s' of a 'relation'! *** FIX ME ***", name);
                        check_me(tmp);
                    }
                }
                // done 'member' of a 'relation'
                if (VERB5) {
                    s << " />" << MEOL;
                }
                if ((pm->ulref == -1)||(pm->type == typ_none)) {
                    char *tmp = GetNxtBuf();
                    sprintf(tmp,"Failed 'member' of a 'relation'! *** CHECK ME ***");
                    check_me(tmp);
                } else {
                    pr->vMems.push_back(*pm);
                }
            } else {
                char *tmp = GetNxtBuf();
                sprintf(tmp,"3:Uncoded child '%s' of a 'relation'! *** FIX ME ***", cn);
                check_me(tmp);
            }
        }
        if (VERB5) {
            s << " </relation>";
        }
    } else {
        if (VERB5) {
            s << " />";
        }
    }
    if ((pr->valid & v_way) == v_way) {
        sz = vRelats.size();
        vRelats.push_back(*pr);
        pr = &vRelats[sz];
    } else {
        SPRTF("FAILED: ");
        check_me("FAILED RELATION");
        pr = 0;
    }
    if (VERB5) {
        // can be too long for SPRTFSPRTF("%s\n", s.str().c_str());
        s << MEOL;
        direct_out_it((char *)s.str().c_str());
    } else {
        sz = vRelats.size();
        if ((sz % 10000) == 0) {
            SPRTF("%d relations\n", (int)sz);
        }
    }
    return pr;
}

////////////////////////////////////////////////////////////////////////////
// get_children(node,...)
// Given a primary node, iterate all the children
// Deal with names 'node', 'way', 'relation'
////////////////////////////////////////////////////////////////////////////
void get_children(xml_node<> *node, std::string ind)
{
    char *name;
    PNODE pn;
    PWAY  pw;
    PRELAT pr;
    std::stringstream s;
    for (xml_node<> *child = node->first_node(); child; child = child->next_sibling()) {
        name = child->name();
        if (strlen(child->value())) {
            char *tmp = GetNxtBuf();
            sprintf(tmp,"child with a value '%s'! CHECK ME", child->value());
            check_me(tmp);
        }
        if (strcmp(name,"node") == 0) {
            pn = add_node(child);   // do 'node', and any 'tag' of node
        } else if (strcmp(name,"way") == 0) {
            pw = add_way(child);    // do 'way', and any 'tag' or 'nd' of way
        } else if (strcmp(name,"relation") == 0) {
            pr = add_relation(child); // do 'relations', and any 'tag' or 'members' of relation
        } else {
            // === just for debug ===
            // ==========================================================================================
            s.str("");
            s << ind;
            s << "<" << name;
            for (xml_attribute<> *attr = child->first_attribute(); attr; attr = attr->next_attribute()) {
                s << " " << attr->name() << "=\"";
                s << attr->value() << "\"";
            }
            if (child->first_node()) {
                s << ">";
                SPRTF("%s\n", s.str().c_str());
                std::string indent = ind;
                indent += " ";
                get_children(child,indent);
                s.str("");
                s << ind.c_str();
                s << "</" << child->name() << ">" << MEOL;
            } else {
                s << " />";
            }
            SPRTF("DBG: %s\n", s.str().c_str());
        }
        // ==========================================================================================
    }
    if ( !pn && !pw && !pr ) {
        SPRTF("DBG: No nodes, ways, or relations!\n");
    }
}

//////////////////////////////////////////////////////////////////////////////
// use_data()
// After collecting ALL the 'data' into structures in vectors
// resolve the features to ouput xgraph format file or files.
/////////////////////////////////////////////////////////////////////////////
int use_data()
{
    int iret = 0;
    size_t ii, max = vNodes.size();
    size_t ii2, max2 = vWays.size();
    size_t ii3, max3;
    size_t ii4, max4;
    size_t pcnt;
    static FEAT feat;
    PFEAT pf = &feat;
    PNODE pn;
    PTAG  pt;
    PND pnd, pnd_last;
    char *ptmp;
    SPRTF("\nDATA: Have %d 'ways', and %d 'nodes' loaded...\n", (int)max2, (int)max);
    // foreach 'way'...
    for (ii2 = 0; ii2 < max2; ii2++) {
        PWAY pw = &vWays[ii2];
        max3 = pw->vRefs.size();
        max4 = pw->vTags.size();
        SPRTF("%d: way id %lu, has %d 'nd' refs, %d 'tag'\n", (int)(ii2 + 1), pw->ulid, (int)max3, (int)max4);
        APT apt, first, sum;
        clean_feat(pf);
        first.lat = BAD_LL;
        first.lon = BAD_LL;
        sum.lat = 0;
        sum.lon = 0;
        for (ii3 = 0; ii3 < max3; ii3++) {
            pnd = &pw->vRefs[ii3];
            if (ii3) {
                if (pnd->ulref == pnd_last->ulref) {
                    SPRTF("WARNING: 'nd' ref %d SAME as last! (%lu) *** CHECK ME *** way id=%lu\n", pnd->ulref, (ii3 + 1),
                        pw->ulid);
                    continue;
                }
            }
            // TODO: change this to a 'map' - too slow to interate through like this
            for (ii = 0; ii < max; ii++) {
                pn = &vNodes[ii];
                if (pnd->ulref == pn->ulid) {
                    pn->used++;
                    break;    
                }
            }
            if (ii < max) {
                apt.lat = pn->lat;
                apt.lon = pn->lon;
                apt.ulid = pnd->ulref;  // KEEP the ID of this point
                // TODO: Need to stick annotation ot a better location than the first point
                if (first.lat == BAD_LL) {
                    first.lat = pn->lat;
                    first.lon = pn->lon;
                }
                sum.lat += pn->lat;
                sum.lon += pn->lon;
                pf->vPoints.push_back(apt);
            } else {
                SPRTF("WARNING: id %lu NOT FOUND\n", pnd->ulref);
            }
            pnd_last = pnd;
        }
        pcnt = pf->vPoints.size();
        if (pcnt > 1) {
            if (pcnt == 2) {
                first.lat = (sum.lat / 2.0);
                first.lon = (sum.lon / 2.0);
            }
            for(ii4 = 0; ii4 < max4; ii4++) {
                pt = &pw->vTags[ii4];
                if ((pt->k == "name") && (pt->v.size()) && (first.lat != BAD_LL)) {
#ifdef USE_NEW_FEAT
                    ANNO a;
                    a.anno = pt->v;
                    a.pt.lat = first.lat;
                    a.pt.lon = first.lon;
                    pf->vAnnos.push_back(a);
#else
                    pf->anno = pt->v;
                    pf->pt   = first;
#endif       
                } else if (pt->k.size() && pt->v.size()) {
                    // <way ... 'tag' added as 'comments'
                    ptmp = GetNxtBuf();
                    sprintf(ptmp,"tag k='%s' v='%s'", pt->k.c_str(), pt->v.c_str());
                    pf->comments.push_back(ptmp);
                }
            }
            vFeats.push_back(*pf);
        } else {
            SPRTF("WARNING: Skipping feature - only %d points! *** CHECK ME *** way id=%lu\n", (int)pcnt,
                pw->ulid);
        }
    }

    ///////////////////////////////////////////////////////////
    //// output the xgraph files
    ///////////////////////////////////////////////////////////
    max = vFeats.size();
    if (max == 0) {
        SPRTF("%s: No features found!\n", module);
        return 1;
    }
    SPRTF("\nCollected %d Features\n", (int)max);
    SPRTF("# Collected %d Features\n", (int)max);
    std::stringstream s;
    std::stringstream all;
    std::string file;
    int file_count = 0;
    PAPT papt, plast;
    FILE *fp;
    for (ii = 0; ii < max; ii++) {
        pf = &vFeats[ii];
        max2 = pf->vPoints.size();
        if (!max2)
            continue;
        s.str("");
        ptmp = GetNxtBuf();
        max4 = pf->comments.size();
        for (ii4 = 0; ii4 < max4; ii4++) {
            s << "# " << pf->comments[ii4] << MEOL;
        }
        sprintf(ptmp,"color %d", (int)ii);
        s << ptmp << MEOL;
#ifdef USE_NEW_FEAT
        size_t i5, m5 = pf->vAnnos.size();
        for (i5 = 0; i5 < m5; i5++) {
            PANNO pa = &pf->vAnnos[i5];
            sprintf(ptmp,"anno %lf %lf %s",
                pa->pt.lon,
                pa->pt.lat,
                pa->anno.c_str());
            s << ptmp << MEOL;
        }
#else
        if (pf->anno.size() && pf->pt.lat != BAD_LL) {
            sprintf(ptmp,"anno %lf %lf %s",
                pf->pt.lon,
                pf->pt.lat,
                pf->anno.c_str());
            s << ptmp << MEOL;
        }
#endif
        for (ii2 = 0; ii2 < max2; ii2++) {
            papt = &pf->vPoints[ii2];
            if (verbose_xg) {
                if (ii2) {
                    double dlon = papt->lon - plast->lon;
                    double dlat = papt->lat - plast->lat;
                    //double dist = M2NM * sqrt( (dlon * dlon) + (dlat * dlat) );
                    double dist = sqrt( (dlon * dlon) + (dlat * dlat) );
                    sprintf(ptmp,"%lf %lf # %d %lu %lf m", papt->lon, papt->lat, (int)ii2, papt->ulid, dist);
                } else {
                    sprintf(ptmp,"%lf %lf # %d %lu", papt->lon, papt->lat, (int)ii2, papt->ulid);
                }
            } else {
                sprintf(ptmp,"%lf %lf", papt->lon, papt->lat);
            }
            s << ptmp << MEOL;
            plast = papt;
        }
        s << "NEXT" << MEOL;
        if (add_each_file) {
            file_count++;
            sprintf(ptmp,"%s%d.xg", base.c_str(), file_count);
            fp = fopen(ptmp,"w");
            if (fp) {
                fwrite(s.str().c_str(),s.str().size(),1,fp);
                fclose(fp);
                s << "# output to " << ptmp << MEOL;
            } else {
                s << "# output to " << ptmp << " FAILED" << MEOL;
            }
        }
        // maybe too large for buffer SPRTF("%s",s.str().c_str());
        direct_out_it((char *)s.str().c_str());
        all << s.str() << MEOL;
    }
    fp = fopen(out_file.c_str(),"w");
    max = all.str().size();
    if (fp) {
        max2 = fwrite(all.str().c_str(), 1, max, fp);
        if (max2 == max) {
            SPRTF("%s: Results written to '%s', %d bytes.\n", module, out_file.c_str(), (int)max);
        } else {
            SPRTF("%s: Write to '%s', %d bytes, FAILED!\n", module, out_file.c_str(), (int)max);
            iret = 1;
        }
        fclose(fp);

    } else {
        SPRTF("%s: Failed to 'open' file '%s'!\n", module, out_file.c_str());
        iret = 1;
    }
    return iret;
}


static const char *example = "C:\\Users\\Public\\Documents\\JOSM\\ygil.osm";
static vSTG vIn_Files;
//static bool verbose_xg = false;
//static bool add_each_file = false;
//static std::string base = "temp";
//static std::string out_file = "tempall.xg";

void give_help( char *name )
{
    SPRTF("\n");
    SPRTF("%s version %s\n", name, VERSION);
    SPRTF("Usage: osm2xg [options] in_file1.osm [in_file2.osm ... in_fileN.osm]\n");
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = Give this help and exit(2)\n");
    SPRTF(" --base name   (-b) = Set 'base' name for each file. (def=%s)\n", base.c_str());
    SPRTF(" Setting a base name implies write each feature to a numbered file.\n");
    SPRTF(" --each        (-e) = Write each feature to a numbered file list. (def=%s)\n",
        (add_each_file ? "On" : "Off"));
    SPRTF(" --log file    (-l) = Set log file. (def=%s)\n", get_log_file());
    SPRTF(" --out file    (-o) = Set output file. (def=%s)\n", out_file.c_str());
    SPRTF(" --verb[n]     (-v) = Bump/Set verbosity. (def=%d)\n", verbosity );
    SPRTF(" --xgverb      (-x) = Add extra comments to xgraph file(s). (def=%s)\n",
        (verbose_xg ? "On" : "Off"));
    SPRTF(" Includes index, way id, and distance from previous (simple Pythagorean calc)\n");

}

int check_log_file(int argc, char **argv)
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'b':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    base = sarg;
                    if (VERB1) SPRTF("%s: Set base of numbered file to '%s'\n", module, base.c_str());
                } else {
                    printf("%s: Expected base file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                // NOTE fall through - setting base implies each feature to new numbered file
            case 'e':
                add_each_file = true;
                if (VERB1) SPRTF("%s: Set to write each feature to '%snn.xg", module, base.c_str());
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                } else {
                    printf("%s: Expected file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            }
        }
    }
    return iret;
}


int parse_command( int argc, char **argv )
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    iret = check_log_file(argc,argv);
    if (iret)
        return iret;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
            case 'l':
                i++;    // already dealt with
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_file = sarg;
                    if (VERB1) SPRTF("%s: Set out file to '%s'.\n", module, out_file.c_str());
                } else {
                    SPRTF("%s: Expected out_file to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'v':
                verbosity++;
                while (*sarg) {
                    if (*sarg == 'v') {
                        verbosity++;
                    } else if (ISDIGIT(*sarg)) {
                        verbosity = atoi(sarg);
                    }
                }
                if (VERB1) SPRTF("%s: Set verbosity to %d\n", module, verbosity);
                break;
            case 'x':
                verbose_xg = true;
                if (VERB1) SPRTF("%s: Set verbose XG output\n", module);
                break;

            default:
                SPRTF("%s: Unknown argument '%s'! Aborting...\n", module, arg);
                return 1;

            }
        } else {
#ifdef _MSC_VER
            if (is_file_or_directory64(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#else
            if (is_file_or_directory32(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#endif
            vIn_Files.push_back(arg);
            if (VERB1) SPRTF("%s: Added file '%s' for processing.\n", module, arg);
        }

    }
    i = (int)vIn_Files.size();
#ifndef NDEBUG
#ifdef _MSC_VER
    if ((i == 0) && (is_file_or_directory64((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    

#else
    if ((i == 0) && (is_file_or_directory32((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    
#endif
#endif
    if (i == 0) {
        SPRTF("%s: No input files found in command! Aborting...\n", module );
        iret = 1;
    }
    return iret;

}

typedef std::vector<char *> vCHARPTR;

static vCHARPTR vMemBlocks;

int process_in_files()
{
    int iret = 0;
    size_t ii,max = vIn_Files.size();
    std::stringstream s;
    std::string indent = " ";
    char *text;
    size_t len, res;
    for (ii = 0; ii < max; ii++) {
        std::string f = vIn_Files[ii];
        char *file = (char *)f.c_str();
#ifdef _MSC_VER
        if (is_file_or_directory64(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        __int64 i64 = get_last_file_size64();
        __int64 size;
        size_t bytes = sizeof(size_t);
        if (bytes == sizeof(unsigned int)) {
            size = UINT_MAX;
            SPRTF("%s: Error: size_t is ONLY an 'unsigned int'\n", module, bytes);
            exit(1);
        } else if (bytes == sizeof(unsigned long))
            size = ULONG_MAX;
        else if (bytes == sizeof(unsigned long long))
            size = ULLONG_MAX;
        // memory allocation limit for a 32-bit app on a 64-bit system
        // You should link your application with /LARGEADDRESSAWARE to make more 
        // than 2GB available to the application. Then you can use up to 4GB 
        // on a 64-bit OS in a 32-bit application.

        if (i64 > 2000000000) {
            SPRTF("%s: Files size %s TOO big to allocate memory\n", module, get_I64_Stg( i64 ));
            exit(1);
        }
        len = (size_t)i64;
#else
        if (is_file_or_directory32(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        len = get_last_file_size32();
#endif
        text = new char[len+2];
        if (!text) {
            SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)len);
            return 1;
        }
        FILE *fp = fopen(file,"rb"); // text file
        if (!fp) {
            SPRTF("%s: Unable to open file %s!\n", module, file );
            delete text;
            return 1;
        }
        //res = fread(text,len,1,fp);
        //if (res != 1) {
        res = fread(text,1,len,fp);
        if (res != len) { // this fails if not "rb"
            SPRTF("%s: Read FAILED! re %d, got %d\n", module, (int)len, (int)res );
            fclose(fp);
            delete text;
            return 1;
        }
        fclose(fp);

        text[len] = 0;

        vMemBlocks.push_back(text);

        SPRTF("%s: Loaded file '%s', %u bytes\n", module, file, (int)len );

        xml_document<> doc;    // character type defaults to char
        doc.parse<0>(text);    // 0 means default parse flags
        // Print to stream using operator <<
        //std::cout << doc;  
        xml_node<> *node = doc.first_node();
        if (!node) {
            SPRTF("%s: Failed to find first node! Skipping...", module);
            iret = 1;
            continue;
            //goto EXIT;
        }
        s << "<" << node->name();
        //if (strlen(node->value()))
        //    cout << " " << node->value();
        for (xml_attribute<> *attr = node->first_attribute();
            attr; attr = attr->next_attribute())
        {
            s << " " << attr->name() << "=\"";
            s << attr->value() << "\"";
        }
        s << ">";
        SPRTF("%s\n", s.str().c_str());
        get_children(node, indent);
        SPRTF("</%s>\n", node->name());

        if (iret)
            break;
    }

    ///////////////////////////////////////////
    iret = use_data(); // whole purpose of the load
    //////////////////////////////////////////

    // release memory
    max = vMemBlocks.size();
    for (ii = 0; ii < max; ii++) {
        text = vMemBlocks[ii];
        delete text;
    }

    return iret;
}

void test_check_me()
{
    check_me("Test check_me!");
    exit(1);
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)"tempxgr.txt",false);
    // test_check_me();
    iret = parse_command(argc,argv);
    if (iret)
        return iret;
    iret = process_in_files();
    return iret;
}


// eof = osm2xgr.cxx
