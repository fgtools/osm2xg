/*\
 * osm2xgs.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
    // **************************************************************************
    // NEED STRATEGY TO DEAL WITH THE GIANT planet.osm
    // It weighs in at 300-500 GB, or more, just in most systems can NOT
    // allocate that much memory to load the whole file. Need to read and 
    // parse the OSM XML buffer by buffer, reading DIRECTLY from the compressed bz2
    // file.
    // Here using libbzip2 to directly stream read from the bz2 file.
    // *************************************************************************
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <iostream>
#ifdef _MSC_VER
#include <conio.h>
#endif
#include <time.h>
#include <sstream>
#include <math.h>
#include <bzlib.h>
#include "sprtf.hxx"
#include "osm2xg.hxx"
#include "utils.hxx"

#define BZ_UNIX      1
/*--
  Win32, as seen by Jacob Navia's excellent
  port of (Chris Fraser & David Hanson)'s excellent
  lcc compiler.  Or with MS Visual C.
  This is selected automatically if compiled by a compiler which
  defines _WIN32, not including the Cygwin GCC.
--*/
#define BZ_LCCWIN32  0
#if defined(_WIN32) && !defined(__CYGWIN__)
#undef  BZ_LCCWIN32
#define BZ_LCCWIN32 1
#undef  BZ_UNIX
#define BZ_UNIX 0
#endif

/*---------------------------------------------*/
/*--
  Some stuff for all platforms.
--*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <math.h>
#include <errno.h>
#include <ctype.h>
#include "bzlib.h"

//#define ERROR_IF_EOF(i)       { if ((i) == EOF)  ioError(); }
//#define ERROR_IF_NOT_ZERO(i)  { if ((i) != 0)    ioError(); }
#define ERROR_IF_MINUS_ONE(i) { if ((i) == (-1)) ioError(); }

/*---------------------------------------------*/
/*--
   Platform-specific stuff.
--*/

#if BZ_UNIX
#   include <fcntl.h>
#   include <sys/types.h>
#   include <utime.h>
#   include <unistd.h>
#   include <sys/stat.h>
#   include <sys/times.h>

#   define PATH_SEP    '/'
#   define MY_LSTAT    lstat
#   define MY_STAT     stat
#   define MY_S_ISREG  S_ISREG
#   define MY_S_ISDIR  S_ISDIR

#   define APPEND_FILESPEC(root, name) \
      root=snocString((root), (name))

#   define APPEND_FLAG(root, name) \
      root=snocString((root), (name))

#   define SET_BINARY_MODE(fd) /**/

#   ifdef __GNUC__
#      define NORETURN __attribute__ ((noreturn))
#   else
#      define NORETURN /**/
#   endif

#   ifdef __DJGPP__
#     include <io.h>
#     include <fcntl.h>
#     undef MY_LSTAT
#     undef MY_STAT
#     define MY_LSTAT stat
#     define MY_STAT stat
#     undef SET_BINARY_MODE
#     define SET_BINARY_MODE(fd)                        \
        do {                                            \
           int retVal = setmode ( fileno ( fd ),        \
                                  O_BINARY );           \
           ERROR_IF_MINUS_ONE ( retVal );               \
        } while ( 0 )
#   endif

#   ifdef __CYGWIN__
#     include <io.h>
#     include <fcntl.h>
#     undef SET_BINARY_MODE
#     define SET_BINARY_MODE(fd)                        \
        do {                                            \
           int retVal = setmode ( fileno ( fd ),        \
                                  O_BINARY );           \
           ERROR_IF_MINUS_ONE ( retVal );               \
        } while ( 0 )
#   endif
#endif /* BZ_UNIX */



#if BZ_LCCWIN32
#   include <io.h>
#   include <fcntl.h>
#   include <sys\stat.h>
#   include <sys\utime.h>

#   define NORETURN       /**/
#   define PATH_SEP       '\\'
#   define MY_LSTAT       _stati64
#   define MY_STAT        _stati64
#   define MY_S_ISREG(x)  ((x) & _S_IFREG)
#   define MY_S_ISDIR(x)  ((x) & _S_IFDIR)

#   define APPEND_FLAG(root, name) \
      root=snocString((root), (name))

#   define APPEND_FILESPEC(root, name)                \
      root = snocString ((root), (name))

#   define SET_BINARY_MODE(fd)                        \
      do {                                            \
         int retVal = setmode ( fileno ( fd ),        \
                                O_BINARY );           \
         ERROR_IF_MINUS_ONE ( retVal );               \
      } while ( 0 )

#endif /* BZ_LCCWIN32 */

/*---------------------------------------------*/
/*--
  Some more stuff for all platforms :-)
--*/

typedef char            Char;
typedef unsigned char   Bool;
typedef unsigned char   UChar;
typedef int             Int32;
typedef unsigned int    UInt32;
typedef short           Int16;
typedef unsigned short  UInt16;
// add a 64-bit entity
typedef long long          Int64;
typedef unsigned long long UInt64;
                                       
#define True  ((Bool)1)
#define False ((Bool)0)

/*--
  IntNative is your platform's `native' int size.
  Only here to avoid probs with 64-bit platforms.
--*/
typedef int IntNative;

static const char *module = "osm2xgs";

/*---------------------------------------------------*/
/*--- Misc (file handling) data decls             ---*/
/*---------------------------------------------------*/

Int32   verbosity = 0;
Bool    keepInputFiles, smallMode, deleteOutputOnInterrupt;
Bool    forceOverwrite, testFailsExist, unzFailsExist, noisy;
Int32   numFileNames, numFilesProcessed, blockSize100k;
Int32   exitValue;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

/*-- source modes; F==file, I==stdin, O==stdout --*/
#define SM_I2O           1
#define SM_F2O           2
#define SM_F2F           3

/*-- operation modes --*/
#define OM_Z             1
#define OM_UNZ           2
#define OM_TEST          3

Int32   opMode;
Int32   srcMode;

#define FILE_NAME_LEN 1034

Int32   longestFileName;
Char    inName [FILE_NAME_LEN];
Char    outName[FILE_NAME_LEN];
Char    tmpName[FILE_NAME_LEN];
Char    *progName;
Char    progNameReally[FILE_NAME_LEN];
FILE    *outputHandleJustInCase;
Int32   workFactor;

// potential forward references
static void    panic                 ( const Char* ) NORETURN;
static void    ioError               ( void )        NORETURN;
static void    outOfMemory           ( void )        NORETURN;
static void    configError           ( void )        NORETURN;
static void    crcError              ( void )        NORETURN;
static void    cleanUpAndFail        ( Int32 )       NORETURN;

static void    copyFileName ( Char*, Char* );
// static void*   myMalloc     ( Int32 );

/*---------------------------------------------*/
static 
Bool myfeof ( FILE* f )
{
   Int32 c = fgetc ( f );
   if (c == EOF) return True;
   ungetc ( c, f );
   return False;
}

/*---------------------------------------------*/
static
void setExit ( Int32 v )
{
   if (v > exitValue) exitValue = v;
}

//////////////////////////////////////////////////////////////////////////
typedef struct tagBZ2STG {
    int err;
    const char *stg;
}BZ2STG, *PBZ2STG;

static BZ2STG BZ2Stg[] = {
    { BZ_OK,          "BZ_OK" },        // 0
    { BZ_RUN_OK,      "BZ_RUN_OK" },    // 1
    { BZ_FLUSH_OK,    "BZ_FLUSH_OK" },  // 2
    { BZ_FINISH_OK,   "BZ_FINISH_OK" }, // 3
    { BZ_STREAM_END, "BZ_STREAM_END" }, // 4
    { BZ_SEQUENCE_ERROR, "BZ_SEQUENCE_ERROR" }, // (-1)
    { BZ_PARAM_ERROR,    "BZ_PARAM_ERROR" },    // (-2)
    { BZ_MEM_ERROR,      "BZ_MEM_ERROR" },      // (-3)
    { BZ_DATA_ERROR,     "BZ_DATA_ERROR" },     // (-4)
    { BZ_DATA_ERROR_MAGIC, "BZ_DATA_ERROR_MAGIC" }, // (-5)
    { BZ_IO_ERROR,         "BZ_IO_ERROR" },         // (-6)
    { BZ_UNEXPECTED_EOF,   "BZ_UNEXPECTED_EOF" },   // (-7)
    { BZ_OUTBUFF_FULL,     "BZ_OUTBUFF_FULL" },     // (-8)
    { BZ_CONFIG_ERROR,     "BZ_CONFIG_ERROR" },     // (-9)
    // end of table
    { -1,                  0 }
};

const char *get_bzerr_stg( int bzerr )
{
    PBZ2STG pb = &BZ2Stg[0];
    while (pb->stg) {
        if (pb->err == bzerr)
            return pb->stg;
        pb++;
    }
    char *tmp = GetNxtBuf();
    sprintf(tmp,"Unlist %d!", bzerr);
    return tmp;
}

static FILE *_s_inStr = 0;
static UChar _s_obuf[5000];
static Int32 last_read = 0;
static Int32 next_read = 0;
static BZFILE *_s_bzf = 0;
static UChar _s_unused[BZ_MAX_UNUSED];
static Int32 _s_nUnused = 0;
static Int32 _s_streamNo = 0;
static const char *last_err;
static Int32 _s_read_count = 0;
/*---------------------------------------------*/
static Bool get_byte( UChar *puc )
{
    char *tmp;
    if ((last_read > 0) && (next_read < last_read)) {
        *puc = _s_obuf[next_read++];
        return True;
    }
    Int32   bzerr;
    if (_s_inStr == 0)
        return False;
    last_err = 0;
    if (_s_bzf == 0) {
        _s_streamNo++;
        _s_bzf = BZ2_bzReadOpen ( &bzerr, _s_inStr, 0,      // verbosity - need no output from libbzip2, 
               (int)smallMode, _s_unused, _s_nUnused );
        if (_s_bzf == 0 || bzerr != BZ_OK) {
            tmp = GetNxtBuf();
            if (_s_bzf == 0)
                sprintf(tmp,"%s: Failed in BZ2_bzReadOpen! nUnused = %d, bzerr=%d (%s)\n", module, _s_nUnused, bzerr, get_bzerr_stg(bzerr));
            else
                sprintf(tmp,"%s: Error from BZ2_bzReadOpen! nUnused = %d, bzerr=%d (%s)\n", module, _s_nUnused, bzerr, get_bzerr_stg(bzerr));
            last_err = tmp;
            goto errhandler;
        }
    }
    _s_read_count++;
    last_read = BZ2_bzRead ( &bzerr, _s_bzf, _s_obuf, 5000 );
    if (bzerr == BZ_DATA_ERROR_MAGIC) {
        tmp = GetNxtBuf();
        sprintf(tmp, "%s: Got error BZ_DATA_ERROR_MAGIC %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
        last_err = tmp;
        goto errhandler;
    }
    if ( !((bzerr == BZ_OK) || (bzerr == BZ_STREAM_END)) ) {
        tmp = GetNxtBuf();
        sprintf(tmp, "%s: Not BZ_OK or BZ_STREAM_END %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
        last_err = tmp;
        goto errhandler;
    }
    if (VERB9) {
        SPRTF("%s: bzRead %d bytes. st=%d, rd=%d. %s\n", module, last_read, _s_streamNo, _s_read_count,
            ((bzerr == BZ_STREAM_END) ? "STREAM_END" : ""));
    }
    if (bzerr == BZ_STREAM_END) {
       void*   unusedTmpV;
        UChar*  unusedTmp;
        BZ2_bzReadGetUnused ( &bzerr, _s_bzf, &unusedTmpV, &_s_nUnused );
        if (bzerr != BZ_OK) {
            tmp = GetNxtBuf();
            sprintf(tmp, "%s: BZ2_bzReadGetUnused NOT BZ_OK! %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
            last_err = tmp;
            panic ( tmp );
        }
        unusedTmp = (UChar*)unusedTmpV;
        for (Int32 i = 0; i < _s_nUnused; i++) {
            _s_unused[i] = unusedTmp[i];
        }
        BZ2_bzReadClose ( &bzerr, _s_bzf );
        if (bzerr != BZ_OK) {
            tmp = GetNxtBuf();
            sprintf(tmp, "%s: BZ2_bzReadClose NOT BZ_OK! %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
            panic ( tmp );
        }
        _s_bzf = 0;
        if (_s_nUnused == 0 && myfeof(_s_inStr)) {
            fclose(_s_inStr);
            _s_inStr = 0;
            if (VERB5) {
                SPRTF("%s: Reached EOF. Buffered %d\n", module, last_read );
            }
        }
    }
    next_read = 0;
    if ((last_read > 0) && (next_read < last_read)) {
        *puc = _s_obuf[next_read++];
        return True;
    } else if ((last_read == 0) && (_s_bzf == 0) && _s_inStr ) {
        // OPEN AGAIN, AND TRY AGAIN
        // seems a 0 read can occur if a block ends at exactly the last char
        _s_streamNo++;
        _s_bzf = BZ2_bzReadOpen ( &bzerr, _s_inStr, 0,      // verbosity - need no output from libbzip2, 
               (int)smallMode, _s_unused, _s_nUnused );
        if (_s_bzf == 0 || bzerr != BZ_OK) {
            tmp = GetNxtBuf();
            if (_s_bzf == 0)
                sprintf(tmp,"%s: 2: Failed in BZ2_bzReadOpen! nUnused = %d, bzerr=%d (%s)\n", module, _s_nUnused, bzerr, get_bzerr_stg(bzerr));
            else
                sprintf(tmp,"%s: 2: Error from BZ2_bzReadOpen! nUnused = %d, bzerr=%d (%s)\n", module, _s_nUnused, bzerr, get_bzerr_stg(bzerr));
            last_err = tmp;
           goto errhandler;
        }
        _s_read_count++;
        last_read = BZ2_bzRead ( &bzerr, _s_bzf, _s_obuf, 5000 );
        if (bzerr == BZ_DATA_ERROR_MAGIC) {
            tmp = GetNxtBuf();
            sprintf(tmp, "%s: 2: Got error BZ_DATA_ERROR_MAGIC %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
            last_err = tmp;
            goto errhandler;
        }
        if ( !((bzerr == BZ_OK) || (bzerr == BZ_STREAM_END)) ) {
            tmp = GetNxtBuf();
            sprintf(tmp, "%s: 2: Not BZ_OK or BZ_STREAM_END %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
            last_err = tmp;
            goto errhandler;
        }
        if (VERB9) {
            SPRTF("%s: 2: bzRead %d bytes. st=%d, rd=%d. %s\n", module, last_read, _s_streamNo, _s_read_count,
                ((bzerr == BZ_STREAM_END) ? "STREAM_END" : ""));
        }
        if (bzerr == BZ_STREAM_END) {
           void*   unusedTmpV;
            UChar*  unusedTmp;
            BZ2_bzReadGetUnused ( &bzerr, _s_bzf, &unusedTmpV, &_s_nUnused );
            if (bzerr != BZ_OK) {
                tmp = GetNxtBuf();
                sprintf(tmp, "%s: BZ2_bzReadGetUnused NOT BZ_OK! %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
                last_err = tmp;
                panic ( tmp );
            }
            unusedTmp = (UChar*)unusedTmpV;
            for (Int32 i = 0; i < _s_nUnused; i++) {
                _s_unused[i] = unusedTmp[i];
            }
            BZ2_bzReadClose ( &bzerr, _s_bzf );
            if (bzerr != BZ_OK) {
                tmp = GetNxtBuf();
                sprintf(tmp, "%s: BZ2_bzReadClose NOT BZ_OK! %d (%s)\n", module, bzerr, get_bzerr_stg(bzerr) );
                panic ( tmp );
            }
            //if (_s_nUnused == 0 && myfeof(_s_inStr)) break;
            _s_bzf = 0;
        }
        if ((last_read > 0) && (next_read < last_read)) {
            *puc = _s_obuf[next_read++];
            return True;
        }
    }
    if (verbosity >= 1)  {
        SPRTF("%s: Error %s: last_read = %d", module, inName, last_read );
        if (_s_nUnused == 0 && myfeof(_s_inStr)) {
            SPRTF("%s: Appears EOF!\n", module );
        }
    }
    return False;

errhandler:

    Int32   bzerr_dummy;
    BZ2_bzReadClose ( &bzerr_dummy, _s_bzf );
    if (_s_inStr)
        fclose ( _s_inStr );
    _s_inStr = 0;
    if (last_err) 
       SPRTF("%s\n", last_err );
    switch (bzerr) 
    {
    case BZ_CONFIG_ERROR:
        configError(); 
        break;
    case BZ_IO_ERROR:
        SPRTF( "%s: I/O or other error, bailing out.  "
             "Possible reason follows.\n", module );
        break;
    case BZ_DATA_ERROR:
         SPRTF("%s: data integrity (CRC) error in data\n", module );
         break;
    case BZ_MEM_ERROR:
         outOfMemory();
         break;
    case BZ_UNEXPECTED_EOF:
         SPRTF("%s: file ends unexpectedly\n", module );
         break;
    case BZ_DATA_ERROR_MAGIC:
         if (_s_streamNo == 1) {
             SPRTF("%s: bad magic number (file not created by bzip2)\n", module );
         } else {
            if (noisy) {
                SPRTF( "%s: trailing garbage after EOF ignored\n", module );
            }
         }
         break;
      default:
         panic ( "unexpected error" );
    }

    panic ( "failed read file" );
    return False; /*notreached*/
}


/*---------------------------------------------*/
#define MX_XML_ITEM 4096
static UChar xml_buf[MX_XML_ITEM+2];
static Int32 xml_in = 0;
// skip showing          123456789012
const char *osm       = "<osm ";
const char *osm_end   = "</osm>";
const char *bound =     "<bound ";
const char *changeset = "<changeset ";
const char *chgend =    "</changeset>";
const char *tag =       "<tag ";
const char *node =      "<node ";
const char *nodeend =   "</node>";
const char *relation =  "<relation ";
const char *relat_end = "</relation>";
const char *member =    "<member ";
const char *way =       "<way ";
const char *way_end =   "</way>";
const char *nd =        "<nd ";
static Int32 chset_count = 0;
static Int32 chgend_count = 0;
static Int32 tag_count = 0;
static Int32 node_count = 0;
static Int32 node_end = 0;
static Int32 xml_items = 0;
static Int32 relat_cnt = 0;
static Int32 memb_cnt = 0;
static Int32 end_of_file = 0;
static Int32 rel_ends = 0;
static Int32 way_cnt = 0;
static Int32 way_ends = 0;
static Int32 nd_cnt = 0;
static Int32 in_osm = 0;
static Int32 bnds_cnt = 0;
static Int64 file_bytes = 0;
static bool is_closed = false;
#define xml_none  0
#define xml_node  1
#define xml_way   2
static Int32 last_xml = xml_none;

#if 0 // 00000000 MOVED TO HEADER 0000000

typedef struct tagNODEM {
    unsigned long ulid;
    double lat,lon;
}NODEM, *PNODEM;
typedef std::vector<NODEM> vNODEM;

typedef struct tagNDM {
    unsigned long ulref;
}NDM, *PNDM;
typedef std::vector<NDM> vNDM;

typedef struct tagWAYM {
    unsigned long ulid;
    vNDM vNDm;
}WAYM, *PWAYM;
typedef std::vector<WAYM> vWAYM;

#endif // 0000000 MOVED TO HEADER 0000

static vNODEM vNodem;
static vWAYM vWaym;

static NODEM nodem;
static WAYM waym;

PNODEM add_node( UChar *ucp )
{
    PNODEM pn = &nodem;
    size_t sz = vNodem.size();
    int c;
    char *cp = strstr((char *)ucp,"id=");
    if (cp) {
        cp = &cp[3];
        c = *cp;
        if ((c == '\'')||(c == '"'))
            cp++;
        pn->ulid = strtoul(cp,0,10);
        cp = strstr((char *)ucp,"lat=");
        if (cp) {
            cp = &cp[4];
            c = *cp;
            if ((c == '\'')||(c == '"'))
                cp++;
            pn->lat = atof(cp);
            if (valid_lat(pn->lat)) {
                cp = strstr((char *)ucp,"lon=");
                if (cp) {
                    cp = &cp[4];
                    c = *cp;
                    if ((c == '\'')||(c == '"'))
                        cp++;
                    pn->lon = atof(cp);
                    if (valid_lon(pn->lon)) {
                        if (VERB9 || ((sz % 1000000) == 0)) {
                            SPRTF("%ld: <node id='%lu' lat='%lf' lon='%lf'%s\n", (sz / 1000000), pn->ulid, pn->lat, pn->lon,
                                (is_closed ? " />" : ">") );
                        }
                        vNodem.push_back(*pn);
                        if (is_closed) 
                            return 0;
                        pn = &vNodem[sz];
                        return pn;
                    }
                }
            }
        }
    }
    SPRTF("<node %s FAILED\n",ucp);
    return 0;
}

//  <way id='18751149' timestamp='2014-01-27T06:20:31Z' uid='333561' user='cleary' visible='true' version='24' changeset='20223220'>
// 
PWAYM add_way( UChar *ucp )
{
    PWAYM pw = 0;
    int c;
    char *cp = strstr((char *)ucp,"id=");
    if (cp) {
        cp = &cp[3];
        c = *cp;
        if ((c == '\'')||(c == '"'))
            cp++;
        pw = &waym;
        pw->ulid = strtoul(cp,0,10);
        pw->vNDm.clear();
        size_t sz = vWaym.size();
        vWaym.push_back(*pw);
        pw = &vWaym[sz];
    } else {
        SPRTF("<way %s FAILED\n",ucp);
    }
    return pw;
}
static PNODEM _s_pnm = 0;
static PWAYM  _s_pwm = 0;

bool add_nd( UChar *ucp )
{
    int c;
    NDM ndm;
    char *cp;
    if ( (last_xml == xml_way) && _s_pwm ) {
        cp = strstr((char *)ucp,"ref=");
        if (cp) {
            cp = &cp[4];
            c = *cp;
            if ((c == '\'')||(c == '"'))
                cp++;
            ndm.ulref = strtoul(cp,0,10);
            _s_pwm->vNDm.push_back(ndm);
            return true;
        }
    }
    SPRTF("<nd %s FAILED\n",ucp);
    return false;
}

static vFEATS vFeats;
#ifndef MEOL
#define MEOL std::endl
#endif
static bool verbose_xgs = true;
static bool add_each_file = false;
static std::string bases = "temps";
static std::string out_files = "tempalls.xg";

int write_features()
{
    int iret = 0;
    PWAYM pw;
    PNDM pnd;
    PNODEM pn;
    size_t ii, max = vWaym.size();
    size_t ii2, max2 = vNodem.size();
    size_t ii3, max3;
    static FEAT feat;
    PFEAT pf = &feat;
    SPRTF("%s: Got %d ways. and %d nodes\n", module, (int)max, (int)max2 );
    for (ii = 0; ii < max; ii++) {
        pw = &vWaym[ii];
        max3 = pw->vNDm.size();
        APT apt;
        clean_feat(pf);
        for (ii3 = 0; ii3 < max3; ii3++) {
            pnd = &pw->vNDm[ii3];
            for (ii2 = 0; ii2 < max2; ii2++) {
                pn = &vNodem[ii2];
                if (pn->ulid == pnd->ulref) {
                    break;
                }
            }
            if (ii2 < max2) {
                apt.lat = pn->lat;
                apt.lon = pn->lon;
                apt.ulid = pnd->ulref;  // KEEP the ID of this point
                pf->vPoints.push_back(apt);
            } else {
                SPRTF("Failed to find a node with id/ref %lu\n", pnd->ulref);
            }
        }
        vFeats.push_back(*pf);
    }

    iret |= output_feats( vFeats, out_files, verbose_xgs );
    return iret;
}

///////////////////////////////////////////////////////////
//// output the xgraph files
///////////////////////////////////////////////////////////
#if 0 // 000000000000000000000000000000000000000000000000000
int output_feats2_NOT_USED( vFEATS &vFeats ) 
{
    int iret = 0;
    size_t ii, max = vFeats.size();
    if (max == 0) {
        SPRTF("%s: No features found!\n", module);
        return 1;
    }
    SPRTF("\nCollected %d Features\n", (int)max);
    SPRTF("# Collected %d Features\n", (int)max);
    std::stringstream s;
    std::stringstream all;
    std::string file;
    int file_count = 0;
    PAPT papt, plast;
    FILE *fp;
    char *ptmp;
    size_t ii4, max4;
    PFEAT pf;
    size_t ii2, max2;
    for (ii = 0; ii < max; ii++) {
        pf = &vFeats[ii];
        max2 = pf->vPoints.size();
        if (!max2)
            continue;
        s.str("");
        ptmp = GetNxtBuf();
        max4 = pf->comments.size();
        for (ii4 = 0; ii4 < max4; ii4++) {
            s << "# " << pf->comments[ii4] << MEOL;
        }
        sprintf(ptmp,"color %d", (int)ii);
        s << ptmp << MEOL;
        if (pf->anno.size() && pf->pt.lat != BAD_LL) {
            sprintf(ptmp,"anno %lf %lf %s",
                pf->pt.lon,
                pf->pt.lat,
                pf->anno.c_str());
            s << ptmp << MEOL;
        }
        for (ii2 = 0; ii2 < max2; ii2++) {
            papt = &pf->vPoints[ii2];
            if (verbose_xgs) {
                if (ii2) {
                    double dlon = papt->lon - plast->lon;
                    double dlat = papt->lat - plast->lat;
                    //double dist = M2NM * sqrt( (dlon * dlon) + (dlat * dlat) );
                    double dist = sqrt( (dlon * dlon) + (dlat * dlat) );
                    sprintf(ptmp,"%lf %lf # %d %lu %lf m", papt->lon, papt->lat, (int)ii2, papt->ulid, dist);
                } else {
                    sprintf(ptmp,"%lf %lf # %d %lu", papt->lon, papt->lat, (int)ii2, papt->ulid);
                }
            } else {
                sprintf(ptmp,"%lf %lf", papt->lon, papt->lat);
            }
            s << ptmp << MEOL;
            plast = papt;
        }
        s << "NEXT" << MEOL;
        if (add_each_file) {
            file_count++;
            sprintf(ptmp,"%s%d.xg", bases.c_str(), file_count);
            fp = fopen(ptmp,"w");
            if (fp) {
                fwrite(s.str().c_str(),s.str().size(),1,fp);
                fclose(fp);
                s << "# output to " << ptmp << MEOL;
            } else {
                s << "# output to " << ptmp << " FAILED" << MEOL;
            }
        }
        // maybe too large for buffer SPRTF("%s",s.str().c_str());
        direct_out_it((char *)s.str().c_str());
        all << s.str() << MEOL;
    }
    fp = fopen(out_files.c_str(),"w");
    max = all.str().size();
    if (fp) {
        max2 = fwrite(all.str().c_str(), 1, max, fp);
        if (max2 == max) {
            SPRTF("%s: Results written to '%s', %d bytes.\n", module, out_files.c_str(), (int)max);
        } else {
            SPRTF("%s: Write to '%s', %d bytes, FAILED!\n", module, out_files.c_str(), (int)max);
            iret = 1;
        }
        fclose(fp);

    } else {
        SPRTF("%s: Failed to 'open' file '%s'!\n", module, out_files.c_str());
        iret = 1;
    }
    return iret;
}
#endif // 00000000000000000000000000000000000000000000000000

Bool process_xml()
{
    UChar *ucp = xml_buf;
    xml_items++;
    if (ucp[1] == '?')
        return true;
    if (in_osm) {
        if (strncmp((const char *)ucp,changeset,11) == 0) {
            if (chset_count == 0) {
                SPRTF("%s\n",ucp);
            }
            chset_count++;
        } else
        if (strcmp((const char *)ucp,chgend) == 0) {
            chgend_count++;
        } else 
        if (strncmp((const char *)ucp,tag,5) == 0) {
            tag_count++;
        } else 
        if (strncmp((const char *)ucp,node,6) == 0) {
            if (node_count == 0) {
                SPRTF("%s\n",ucp);
            }
            node_count++;
            last_xml = is_closed ? xml_none : xml_node;
            _s_pnm = add_node( &ucp[6] );
        } else
        if (strcmp((const char *)ucp,nodeend) == 0) {
            node_end++;
        } else 
        if (strncmp((const char *)ucp,way,5) == 0) {
            if (way_cnt == 0) {
                SPRTF("%s\n",ucp);
            }
            way_cnt++;
            last_xml = is_closed ? xml_none : xml_way;
            _s_pwm = add_way( &ucp[5] );
        } else 
        if (strcmp((const char *)ucp,way_end) == 0) {
            way_ends++;
        } else 
        if (strncmp((const char *)ucp,nd,4) == 0) {
            if (nd_cnt == 0) {
                SPRTF("%s\n",ucp);
            }
            nd_cnt++;
            add_nd( &ucp[4] );
        } else 
        if (strncmp((const char *)ucp,relation,10) == 0) {
            relat_cnt++;
        } else
        if (strcmp((const char *)ucp,relat_end) == 0) {
            rel_ends++;
        } else 
        if (strncmp((const char *)ucp,member,8) == 0) {
            memb_cnt++;
        } else 
        if (strncmp((const char *)ucp,bound,7) == 0) {
            bnds_cnt++;
        } else
        if (strcmp((const char *)ucp,osm_end) == 0) {
            end_of_file = 1;
            in_osm = 0;
        } else {
            SPRTF("%s UNCODED\n",ucp);
        }
    } else if (strncmp((const char *)ucp,osm,5) == 0) {
        in_osm = 1;
    }
    return True;
}


static Bool process_byte_stream()
{
    Bool bret = True;
    UChar uc, pc;
    Bool in_xml = False;
    pc = 0;
    while (get_byte( &uc ))
    {
        file_bytes++;
        if (in_xml) {
            xml_buf[xml_in++] = uc;
            if (uc == '>') {
                in_xml = False;
                is_closed = (pc == '/') ? true : false;
                xml_buf[xml_in] = 0;
                if (!process_xml()) {
                    bret = False;
                    break;
                }
            }
            if (xml_in >= MX_XML_ITEM) {
                SPRTF( "%s: XML item overrun! Increase MX_XML_ITEM, presently %d. and recompile.\n", module,
                    MX_XML_ITEM );
                bret = False;
                exit(1);
                break;
            }
        } else if (uc == '<') {
            xml_in = 0;
            xml_buf[xml_in++] = uc;
            in_xml = True;
        }
        pc = uc;
    }
    return bret;
}

/*---------------------------------------------*/
static Bool process_file( Char *name )
{
    Bool bret;
    static struct MY_STAT statBuf;
    copyFileName ( inName, name );
    MY_STAT(name, &statBuf);
    if ( MY_S_ISDIR(statBuf.st_mode) ) {
        SPRTF( "%s: Input file %s is a directory.\n", module, name);
        return False;
    }
    if (verbosity >= 1) {
        SPRTF ( "%s: File size %s ", module, get_I64_Stg(statBuf.st_size) );
    }
    _s_inStr = fopen ( name, "rb" );
    if ( _s_inStr == NULL ) {
        SPRTF ( "%s: Can't open input file %s:%s.\n", module, name, strerror(errno) );
        return False;
    }
    if (verbosity >= 1) {
       SPRTF( "%s\n", inName );
    }

    SET_BINARY_MODE( _s_inStr );
    if (ferror(_s_inStr)) {
       //goto errhandler_io;
       if (verbosity >= 1) SPRTF("failed in set binary!\n" );
       //setExit(1);
       return False;
    }
    bret = process_byte_stream();
    SPRTF("%s: File bytes processed %s\n", module, get_I64_Stg(file_bytes));
    SPRTF( "%s: xml items %d, changesets %d, tags %d, nodes %d, relat %d, memb %d, way %d, nd %d\n", module,
        xml_items, chset_count, tag_count, node_count, relat_cnt, memb_cnt, way_cnt, nd_cnt );
    write_features();
    return bret;
}


/*---------------------------------------------*/
static 
void cadvise ( void )
{
   if (noisy) {
        SPRTF("%s: It is possible that the compressed file(s) have become corrupted.\n"
        "You may be able to use the `bzip2recover' program to attempt to recover\n"
        "data from undamaged sections of corrupted files.\n", module );
   }
}


/*---------------------------------------------*/
static 
void showFileNames ( void )
{
    if (noisy) {
        SPRTF("%s: Input file = %s\n", module, inName );
    }
}

/*---------------------------------------------*/
static 
void cleanUpAndFail ( Int32 ec )
{
   //IntNative      retVal;
   if (noisy && numFileNames > 0 && numFilesProcessed < numFileNames) {
      SPRTF( "%s: WARNING: some files have not been processed:\n"
              "%d specified on command line, %d not processed yet.\n\n",
              module,
              numFileNames, numFileNames - numFilesProcessed );
   }
   setExit(ec);
   exit(exitValue);
}

/*---------------------------------------------*/
static 
void panic ( const Char* s )
{
   SPRTF( "%s: PANIC -- internal consistency error:\n"
          "\t%s\n"
          "\tThis may be a BUG.  Please report it to me at:\n"
          "\treports _AT_ geoffair _DOT_ info\n",
             module, s );
   showFileNames();
   cleanUpAndFail( 3 );
}


/*---------------------------------------------*/
static 
void crcError ( void )
{
   SPRTF( "%s: Data integrity error when decompressing.\n", module );
   showFileNames();
   cadvise();
   cleanUpAndFail( 2 );
}


#if 0 // 000000000 UNUSED 000000000


/*---------------------------------------------*/
static 
void compressedStreamEOF ( void )
{
  if (noisy) {
    SPRTF("%s: Compressed file ends unexpectedly;\n\t"
	      "perhaps it is corrupted?  *Possible* reason follows.\n", module );
    perror ( module );
    showFileNames();
    cadvise();
  }
  cleanUpAndFail( 2 );
}

#endif // 000000000 UNUSED 000000000

/*---------------------------------------------*/
static 
void ioError ( void )
{
   SPRTF("%s: I/O or other error, bailing out.  "
             "Possible reason follows.\n",
             module );
   perror ( module );
   showFileNames();
   cleanUpAndFail( 1 );
}

#if 0 // 000000000 UNUSED 000000000

/*---------------------------------------------*/
static 
void mySignalCatcher ( IntNative n )
{
   SPRTF("%s: Control-C or similar caught, quitting.\n", module );
   cleanUpAndFail(1);
}

#endif // 000000000 UNUSED 000000000


/*---------------------------------------------*/
static 
void outOfMemory ( void )
{
   SPRTF("%s: couldn't allocate enough memory\n", module );
   showFileNames();
   cleanUpAndFail(1);
}


/*---------------------------------------------*/
static 
void configError ( void )
{
   SPRTF( "%s: not configured correctly for this platform!\n"
             "\tI require Int32, Int16 and Char to have sizes\n"
             "\tof 4, 2 and 1 bytes to run properly, and they don't.\n"
             "\tProbably you can fix this by defining them correctly,\n"
             "\tand recompiling.  Bye!\n", module );
   setExit(3);
   exit(exitValue);
}

// #ifdef _MSC_VER
Char *get_file_name( Char *from )
{
    static char _s_fn[FILE_NAME_LEN];
    Char *fn = _s_fn;
    size_t ii, len;
    int c;
    strcpy(fn,from);    // copy in whole file name
    len = strlen(from);
    for (ii = 0; ii < len; ii++) {
        c = from[ii];
        if ((c == '\\')||(c == '/')) {
            strcpy(fn,&from[ii+1]);
        }
    }
    len = strlen(fn);
    while(len--) {
        if (fn[len] == '.') {
            fn[len] = 0;
            break;
        }
    }
    return fn;
}
// #endif

/*---------------------------------------------*/
static 
void copyFileName ( Char* to, Char* from ) 
{
    if ( strlen(from) > FILE_NAME_LEN-10 )  {
        SPRTF( "%s: file name\n`%s'\n"
            "is suspiciously (more than %d chars) long.\n"
            "Try using a reasonable file name instead.  Sorry! :-)\n", module,
            from, FILE_NAME_LEN-10 );
        setExit(1);
        exit(exitValue);
    }

    strncpy(to,from,FILE_NAME_LEN-10);
    to[FILE_NAME_LEN-10]='\0';
}

/*---------------------------------------------*/
void do_init()
{
   /*-- Be really really really paranoid :-) --*/
   if (sizeof(Int32) != 4 || sizeof(UInt32) != 4  ||
       sizeof(Int16) != 2 || sizeof(UInt16) != 2  ||
       sizeof(Char)  != 1 || sizeof(UChar)  != 1)
      configError();

   /*-- Initialise --*/
   outputHandleJustInCase  = NULL;
   smallMode               = False;
   keepInputFiles          = False;
   forceOverwrite          = False;
   noisy                   = True;
   verbosity               = 0;
   blockSize100k           = 9;
   testFailsExist          = False;
   unzFailsExist           = False;
   numFileNames            = 0;
   numFilesProcessed       = 0;
   workFactor              = 30;
   deleteOutputOnInterrupt = False;
   exitValue               = 0;
}

const char *planet = "C:\\Users\\user\\Downloads\\planet-latest.osm.bz2";

int main( int argc, char **argv )
{
    int iret = 0;
    do_init();
    longestFileName = (Int32)strlen(planet);   // 7;
    numFileNames    = 1;
    opMode          = OM_UNZ;
    verbosity       = 4;        // NOTE: MAXIMUM verbosity
    srcMode         = SM_F2O;   // SM_I2O;

    copyFileName ( progNameReally, get_file_name(argv[0]) );
    progName = &progNameReally[0];
#ifdef _MSC_VER
    if (argc < 2) {
        if (is_file_or_directory64((char *)planet) == DT_FILE) {
            SPRTF("%s: Using default '%s' file, %I64d bytes...\n", module, planet, get_last_file_size64());
            process_file((Char *)planet);
        } else {
            SPRTF("%s: Add like a planet.osm.bz2 file path to process...\n", module);
        }
    } else {
        if (is_file_or_directory64(argv[1]) == DT_FILE) {
            SPRTF("%s: Using '%s' file, %I64d bytes...\n", module, argv[1], get_last_file_size64());
            process_file(argv[1]);
        } else {
            SPRTF("%s: First item '%s' not a valid file to process...\n", module, argv[1]);
        }
    }

#else
    if (argc < 2) {
        if (is_file_or_directory32((char *)planet) == DT_FILE) {
            SPRTF("%s: Using default '%s' file, %ld bytes...\n", module, planet, get_last_file_size32());
            process_file((Char *)planet);
        } else {
            SPRTF("%s: Add like a planet.osm.bz2 file to process...\n", module);
        }
    } else {
        if (is_file_or_directory32(argv[1]) == DT_FILE) {
            SPRTF("%s: Using '%s' file, %ld bytes...\n", module, argv[1], get_last_file_size32());
            process_file(argv[1]);
        } else {
            SPRTF("%s: First item '%s' not a valid file to process...\n", module, argv[1]);
        }
    }
#endif
    return iret;
}

// eof = osm2xgs.cxx
