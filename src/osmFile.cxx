/*\
 * osmFile.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <string.h> // for memcpy(), ...
#endif
#include <bzlib.h>
#include <zlib.h>
#include "sprtf.hxx"
#include "utils.hxx"
#include "osmFile.hxx"

static const char *module = "osmFile";

typedef struct tagBZ2STG {
    int err;
    const char *stg;
}BZ2STG, *PBZ2STG;

static BZ2STG BZ2Stg[] = {
    { BZ_OK,          "BZ_OK" },        // 0
    { BZ_RUN_OK,      "BZ_RUN_OK" },    // 1
    { BZ_FLUSH_OK,    "BZ_FLUSH_OK" },  // 2
    { BZ_FINISH_OK,   "BZ_FINISH_OK" }, // 3
    { BZ_STREAM_END, "BZ_STREAM_END" }, // 4
    { BZ_SEQUENCE_ERROR, "BZ_SEQUENCE_ERROR" }, // (-1)
    { BZ_PARAM_ERROR,    "BZ_PARAM_ERROR" },    // (-2)
    { BZ_MEM_ERROR,      "BZ_MEM_ERROR" },      // (-3)
    { BZ_DATA_ERROR,     "BZ_DATA_ERROR" },     // (-4)
    { BZ_DATA_ERROR_MAGIC, "BZ_DATA_ERROR_MAGIC" }, // (-5)
    { BZ_IO_ERROR,         "BZ_IO_ERROR" },         // (-6)
    { BZ_UNEXPECTED_EOF,   "BZ_UNEXPECTED_EOF" },   // (-7)
    { BZ_OUTBUFF_FULL,     "BZ_OUTBUFF_FULL" },     // (-8)
    { BZ_CONFIG_ERROR,     "BZ_CONFIG_ERROR" },     // (-9)
    // end of table
    { -1,                  0 }
};

static const char *get_bzerr_stg( int bzerr )
{
    PBZ2STG pb = &BZ2Stg[0];
    while (pb->stg) {
        if (pb->err == bzerr)
            return pb->stg;
        pb++;
    }
    static char bzerr_buf[32];
    char *tmp = bzerr_buf;
    sprintf(tmp,"Unlist %d!", bzerr);
    return tmp;
}


void osmFile::osm_Init()
{
    bzf = 0;
    nUnused = 0;
    streamNo = 0;
    smallMode = False;
    inStr = 0;
    verbose = 0;
    buffer = 0;
    bufSize = MX_IO_BUF;
    last_read = 0;
    read_done = 0;
    gzf = 0;
    oft = ot_None;
}

osmFile::osmFile()
{
    osm_Init();

}
osmFile::~osmFile()
{
    osmClose();
}

#ifdef _MSC_VER
#define FTELL64 _ftelli64
#define FSEEK64 _fseeki64
#else
#define FTELL64 ftello64
#define FSEEK64 fseeko64
#endif


UInt64 osmFile::getFileSize( FILE *fp )
{
    UInt64 curr = FTELL64(fp);
    if (curr == -1)
        return curr;
    Int32 set = FSEEK64(fp,0,SEEK_END);
    if (set) {
        FSEEK64(fp,curr,SEEK_SET); // try to put it back where it was
        return -1;
    }
    UInt64 size = FTELL64(fp);
    set = FSEEK64(fp,curr,SEEK_SET); // put it back where it was
    if (set)
        return -1;
    
    return size;

}

Bool osmFile::myfeof ( FILE* f )
{
    Int32 c = fgetc ( f );
    if (c == EOF) return True;
    ungetc ( c, f );
    return False;
}

Bool osmFile::osm_Next()
{
    read_done = 0;
    last_read = 0;
    if (!inStr && !gzf) {
        return False;
    }
    if (oft == ot_Bz2) {
        if (!bzf) {
            bzf = BZ2_bzReadOpen ( &bzerr, inStr, 0,      // verbosity - need no output from libbzip2, 
                       (int)smallMode, unused, nUnused );
            if (bzf == 0 || bzerr != BZ_OK) {
                if ((verbose >= 9) && (bzerr < 0)) {
                    SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
                }
                if (verbose >= 9) {
                    SPRTF("Can NOT bzReadOpen '%s'!\n", in_file.c_str());
                }
                bzf = 0;
                osmClose();
                return False;
            }
        }
        last_read = BZ2_bzRead( &bzerr, bzf, buffer, bufSize );
        if ( !((bzerr == BZ_OK) || (bzerr == BZ_STREAM_END)) ) {
            if ((verbose >= 9) && (bzerr < 0)) {
                SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
            }
            if (verbose >= 9) {
                SPRTF("Failed on read of file '%s'!\n", in_file.c_str());
            }
            osmClose();
            return False;
        }
        if (bzerr == BZ_STREAM_END) {
            UChar *unusedTmp;
            BZ2_bzReadGetUnused( &bzerr, bzf, (void **)&unusedTmp, &nUnused );
            if (bzerr != BZ_OK) {
                if ((verbose >= 9) && (bzerr < 0)) {
                    SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
                }
                if (verbose >= 9) {
                    SPRTF("Failed at stream end on file '%s'!\n", in_file.c_str());
                }
                osmClose();
                return False;
            }
            for (Int32 i = 0; i < nUnused; i++) {
                unused[i] = unusedTmp[i];
            }
            BZ2_bzReadClose ( &bzerr, bzf );
            bzf = 0;
            if (bzerr != BZ_OK) {
                if ((verbose >= 9) && (bzerr < 0)) {
                    SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
                }
                if (verbose >= 9) {
                    SPRTF("Failed bzReadClose on file '%s'!\n", in_file.c_str());
                }
            }
            if ((nUnused == 0) && myfeof(inStr)) {
                fclose(inStr);
                inStr = 0;
            }
        }
    } else {
        last_read = gzread( gzf, buffer, bufSize );
        if (last_read == -1) {
            if (verbose >= 9) {
                SPRTF("Failed on read of file '%s'!\n", in_file.c_str());
            }
            osmClose();
            return False;
        }
        gzErrMsg = gzerror( gzf, &gzError );
        if (gzError != Z_OK) {
            // this could be a END OF FILE
            if (verbose) {
                if (gzErrMsg && gzErrMsg[0])
                    SPRTF("Got gz Error %d (%s)\n", gzError, gzErrMsg);
                else
                    SPRTF("Got gz Error %d\n", gzError);
            }
            //if (gzError == Z_BUF_ERROR) {
                if (gzeof(gzf)) {
                    gzclose(gzf);
                    gzf = 0;
                } else {
                    // could still be eof - try reading one more
                    int res = gzread( gzf, &buffer[last_read],1 );
                    if (res == -1) {
                        if (gzeof(gzf)) {
                            gzclose(gzf);
                            gzf = 0;
                        }
                    } else {
                        last_read += 1;
                    }
                }
            //}

        }
    }
    return True;
}

Bool osmFile::osmOpen( std::string file )
{
    in_file = file;
    osmFileType(file);
    if (oft == ot_Bz2) {
        inStr = fopen(in_file.c_str(),"rb");
        if (!inStr) {
            if (verbose) {
                SPRTF("Can NOT open '%s'!\n", file.c_str());
            }
            return False;
        }
        bzf = BZ2_bzReadOpen ( &bzerr, inStr, 0,      // verbosity - need no output from libbzip2, 
                   (int)smallMode, unused, nUnused );
        if (bzf == 0 || bzerr != BZ_OK) {
            if ((verbose >= 9) && (bzerr < 0)) {
                SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
            }
            if (verbose) {
                SPRTF("Can NOT bzReadOpen '%s'!\n", file.c_str());

            }
            bzf = 0;
            osmClose();
            return False;
        }
    } else {
        gzf = gzopen(in_file.c_str(),"rb");
        if (!gzf) {
            if (verbose) {
                SPRTF("Can NOT open '%s'!\n", file.c_str());
            }
            osmClose();
            return False;
        }
    }

    buffer = new UChar[bufSize+2];  // NOTE: 2 extra bytes allocated
    if (!buffer) {
        if (verbose >= 5) {
            SPRTF("Memory allocation of %u bytes FAILED!\n", bufSize);
        }
        osmClose();
        return False;
    }

    read_done = 0;

    if (oft == ot_Bz2) {
        last_read = BZ2_bzRead( &bzerr, bzf, buffer, bufSize );
        if ( !((bzerr == BZ_OK) || (bzerr == BZ_STREAM_END)) ) {
            if ((verbose >= 9) && (bzerr < 0)) {
                SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
            }
            if (verbose >= 2) {
                SPRTF("Failed on first read of file '%s'!\n", in_file.c_str());
            }
            osmClose();
            return False;
        }
        if (bzerr == BZ_STREAM_END) {
            UChar *unusedTmp;
            BZ2_bzReadGetUnused( &bzerr, bzf, (void **)&unusedTmp, &nUnused );
            if (bzerr != BZ_OK) {
                if ((verbose >= 9) && (bzerr < 0)) {
                    SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
                }
                if (verbose >= 2) {
                    SPRTF("Failed at stream end on file '%s'!\n", in_file.c_str());
                }
                osmClose();
                return False;
            }
            for (Int32 i = 0; i < nUnused; i++) {
                unused[i] = unusedTmp[i];
            }
            BZ2_bzReadClose ( &bzerr, bzf );
            bzf = 0;
            if (bzerr != BZ_OK) {
                if ((verbose >= 9) && (bzerr < 0)) {
                    SPRTF("Got BZ2 Error: %s!\n", get_bzerr_stg(bzerr));
                }
                if (verbose >= 9) {
                    SPRTF("Failed bzReadClose on file '%s'!\n", in_file.c_str());
                }
            }
            if ((nUnused == 0) && myfeof(inStr)) {
                fclose(inStr);
                inStr = 0;
            }
        }
    } else {
        last_read = gzread(gzf,buffer,bufSize);
        if (last_read == -1) {
            if (verbose >= 2) {
                SPRTF("Failed on first read of file '%s'!\n", in_file.c_str());
            }
            osmClose();
            return False;
        }
    }

    if (verbose >= 5) {
        SPRTF("File '%s' open for reading, one day seeking...\n", file.c_str());
    }

    return True;
}

Bool osmFile::osmClose()
{
    Bool bret = True;
    if (bzf) {
        BZ2_bzReadClose ( &bzerr, bzf );
        if (bzerr != BZ_OK) {
            if (verbose >= 5) {
                SPRTF("bzFile '%s' close failed! %d\n", in_file.c_str(), bzerr);
            }
            bret = False;
        }
    }
    bzf = 0;

    if (inStr)
        fclose(inStr);
    inStr = 0;

    if (buffer)
        delete buffer;
    buffer = 0;
    last_read = 0;

    if (gzf) {
        if (gzclose(gzf) != Z_OK) {

        }
    }
    gzf = 0;

    return bret;
}

int osmFile::osmRead( UChar *p, int len )
{
    int avail = last_read - read_done;
    int cpy = 0;
    if (avail) {
        // copy the data to user
        cpy = avail;
        if (len < cpy)
            cpy = len;
        memcpy(p, &buffer[read_done],cpy);
        read_done += cpy;
        len -= cpy;
    }
    while (len) {
        if (inStr || gzf) {
            osm_Next();
            avail = last_read - read_done;
            if (avail) {
                int cpy2 = avail;
                if (len < cpy2)
                    cpy2 = len;
                memcpy(&p[cpy],&buffer[read_done],cpy2);
                read_done += cpy2;
                cpy += cpy2;
                len -= cpy2;
            } else
                break;
        } else 
            break;
    }
    if (cpy)
        return cpy; // return copied amount
    return -1;  // read FAILED
}

#define MX_TEST_BUF 128
#define BZ_HDR_B 0x42   /* 'B' */
#define BZ_HDR_Z 0x5a   /* 'Z' */
#define BZ_HDR_h 0x68   /* 'h' */
// #define BZ_HDR_0 0x30   /* '0' */
//////////////////////////////////////////////////////////////////////////
// get the file type
// TODO: This needs mreo work and testing - not very happy yet ;=((
//////////////////////////////////////////////////////////////////////////
osm_FileType osmFile::osmFileType( std::string file )
{
    FILE *fp = fopen(file.c_str(), "rb");
    oft = ot_None;
    if (!fp) {
        return ot_None;
    }
    unsigned char buf[MX_TEST_BUF];
    size_t ii, res = fread(buf,1,MX_TEST_BUF,fp);
    fclose(fp);
    if (res > 4) {
        unsigned char c;
        bool got_bin = false;
        // search for a 'binary' value
        for (ii = 0; ii < res; ii++) {
            c = buf[ii];
            if (c == 0) {
                got_bin = true;
                break;
            }
            if (c < ' ') {
                if ( !((c == '\t')||(c == '\r')||(c == '\n')) ) {
                    got_bin = true;
                    break;
                }
            }
        }

        // test for BZip2 - TODO: Is this a sufficient test????????
        if ((buf[0] == BZ_HDR_B) &&
            (buf[1] == BZ_HDR_Z) &&
            (buf[2] == BZ_HDR_h) &&
            ( ISDIGIT(buf[3])  ) ) {
            oft = ot_Bz2;
            return ot_Bz2;
        }
        // test for gzip - TODO: CHECK ME! is this ALWAYS right ????
        if ((buf[0] == 31) &&
            (buf[1] == 139)) {
            oft = ot_Gz1;
            return ot_Gz1;
        }
        gzFile gz_f = gzopen(file.c_str(),"rb");
        if (gz_f) {
            unsigned char buf2[MX_TEST_BUF];
            int ret = gzread(gz_f, buf2, MX_TEST_BUF);
            gzclose(gz_f);
            if (ret != -1) {
                oft = ot_Gz2;
                return ot_Gz2;
            }
        }

        if (!got_bin) {
            oft = ot_Txt;
            return ot_Txt;
        }

    }
    oft = ot_Unk;
    return ot_Unk;
}


// eof = osmFile.cxx
