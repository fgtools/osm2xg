/*\
 * osmFile.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _OSMFILE_HXX_
#define _OSMFILE_HXX_

#include <string>
#include <bzlib.h>
#include <zlib.h>

typedef int                Int32;
typedef unsigned char      Bool;
typedef unsigned char      UChar;
typedef unsigned long long UInt64;

#define True  ((Bool)1)
#define False ((Bool)0)

#ifndef MX_IO_BUF
#define MX_IO_BUF 5000
#endif

enum osm_FileType {
    ot_None = 0,
    ot_Bz2,
    ot_Gz1,     /* has gz header bytes */
    ot_Gz2,     /* can be handled by zlib */
    ot_Txt,
    ot_Unk
};

// add body
class osmFile {
public:
    osmFile();
    ~osmFile();

    Bool osmOpen( std::string file );
    int  osmRead( UChar *p, int len );
    Bool osmClose();
    Bool myfeof ( FILE* f );
    osm_FileType osmFileType( std::string file );
    UInt64 getFileSize( FILE *fp );
    int verbose;

private:
    void osm_Init();
    Bool osm_Next();
    std::string in_file;
    BZFILE *bzf;
    Int32   bzerr;
    UChar   unused[BZ_MAX_UNUSED];
    Int32   nUnused;
    Int32   streamNo;
    Bool    smallMode;
    FILE   *inStr;
    UChar  *buffer;
    Int32   bufSize;
    Int32   last_read, read_done;
    Bool    bret;
    // zlib commpression
    gzFile  gzf;
    osm_FileType oft;
    int     gzError;
    const char *gzErrMsg;
};



#endif // #ifndef _OSMFILE_HXX_
// eof - osmFile.hxx
