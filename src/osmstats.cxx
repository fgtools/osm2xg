/*\
 * osmstats.cxx
 *
 * I can not find a function like BZ2_seek64 in the Bzip2 documentation, although there are some 
 * mentions of it doing an internet search.
 * One such is from : https://bitbucket.org/james_taylor/seek-bzip2
 * I cloned this hg repository and compiled it in Windows - hseek-bzip2f.bat - but could not get 
 * the two examples to work. This may be due to a bigendian/littlendian thing. From reading the code
 * there is a mention of bigendianess in the CRC generation. They do try to use and undrestand the 
 * raw format of bz2 files. I did have some success using the actual BZip2 library to build a seek-bz2 app
 * whihc will 'stride' though a bzip2 file, showing the blocks, read cound and total 'uncompressed' byte
 * size. This success led to this -
 *
 * This is an 'attempt' at developing such a BZ2_seek64 function... The BZ2 library interface has a 
 * 'strange' way to allow you to read say 5000 bytes buffers from a bz2 file. The library is reading 
 * sequencially from the FILE * supplied. Thus although you may have requested say 5000 uncompressed 
 * bytes from the file, the libray, at that moment, may have read beyond that in the compressed data,
 * thus indicates a BZ_STREAM_END, and the reader app must 'save' the over-read unused compressed 
 * byte, to pass back to the library on the next BZ2_bzReadOpen. A littel puzzling, but works fine ;=))
 *
 * Thus at this time, I see for each BZ2_seek64 request I must re-wind the files to its beginning,
 * and march forward get uncompressed blocks until until the seek64 desired offset is exceeded, thus must 
 * also handle the subsequent 'read' first passing back any 'excess' read, then proceeding to do an 
 * actual BZ2_bzRead, so must have other names for this.
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string>
#include <map>
#include <bzlib.h>
#ifndef _MSC_VER
#include <stdlib.h> // for exit(), ...
#include <string.h> // for memset(), ...
#endif
#include "sprtf.hxx"
#include "utils.hxx"
#include "osmFile.hxx"
#include "osmstats.hxx"

static const char *module = "osmstats";
//static const char *module = "BZ2_seek64";
//////////////////////////////////////////////////////////////////////////////////
// OSM STATS 
//////////////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#include <conio.h> // for _getch(), ...
#endif
static int check_me( const char *msg )
{
    int i = SPRTF("CHECK ME: %s\n",msg);
#if (defined(_MSC_VER) && !defined(NDEBUG))
    if (!bIsRedON()) {
        //_kbhit();
        SPRTF("Any key to continue!\n");
        _getch();
    }
#endif
    return i;
}

static const char *def_log_file = "tempstats.txt";
static char *in_file = 0;   // user input file to 'stat'
static unsigned long long file_size = 0; // compressed size of input file
static UInt64 total_read = 0;   // uncompressed total read count
static UInt64 show_count = DEF_SHOW_COUNT; // for display if VERB1
static bool show_node_id = false;
static bool show_tags    = false;
static int tag_wrap      = 8;   // wrap tag output at this count - 0 = no wrap - must be positive
static bool store_nodes  = false;
//static const char *example = "C:\\Users\\user\\Downloads\\planet-latest.osm.bz2";
//static const char *example = "D:\\SAVES\\OSM\\piccatwr.osm.bz2";
static const char *example = "D:\\SAVES\\OSM\\australia.osm.bz2";
static bool run_debug_example = false;
static const char *out_file = 0;
static bool do_xg_out = false;
static bool verbose_xgs = false;
static bool xg_auto_color = false;

static int verbosity = 1;   // default to 1
#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

/////////////////////////////////////////////////////////////////////////////////////
// maybe these need ot be larger, like full 64-bits, but for now...
////////////////////////////////////////////////////////////////////////////////////
typedef struct tagXSTATS {
    UInt64 ll_xml_items, ll_chset_count, ll_tag_count, ll_node_count, ll_relat_cnt, ll_memb_cnt, ll_way_cnt, ll_nd_cnt;
    UInt64 ll_xml_hdr, ll_xml_osm, ll_xml_close, ll_xml_open;
}XSTATS, *PXSTATS;

static XSTATS xStats;

static NODEM nFirst, nMin, nMax, nLast;
static UInt64 node_id_seq_err = 0;

#define xml_none 0
#define xml_node 1
#define xml_way  2
static int last_xml_item = xml_none;
static Bool isxmlend = False;

static UChar rd_buff[MX_RD_BUFF];

// for storing nodes
typedef std::map<unsigned long,APT> mNODES;
typedef mNODES::iterator mNODESi;
static mNODES mNodes;

void show_stats( const char *msg )
{
    PXSTATS pxs = &xStats;
    //        pxs->chset_count, 
    //        pxs->memb_cnt, pxs->xml_open );
    if (msg && msg[0]) {
        SPRTF("Items %s, nodes %s, ways %s, relats %s, tags %s, nd %s, done %s. %s\n",
            get_I64_Stg(pxs->ll_xml_items),
            get_I64_Stg(pxs->ll_node_count),
            get_I64_Stg(pxs->ll_way_cnt),
            get_I64_Stg(pxs->ll_relat_cnt), 
            get_I64_Stg(pxs->ll_tag_count),
            get_I64_Stg(pxs->ll_nd_cnt),
            get_k_num(total_read),
            msg );
    } else {
        SPRTF("I*1Mil %s",
            get_I64_Stg(pxs->ll_xml_items / show_count));
        if (pxs->ll_node_count) {
            SPRTF(", nodes %s",
                get_I64_Stg(pxs->ll_node_count));
        }
        if (pxs->ll_way_cnt) {
            SPRTF(", ways %s",
                get_I64_Stg(pxs->ll_way_cnt));
        }
        if (pxs->ll_relat_cnt) {
            SPRTF(", relats %s",
                get_I64_Stg(pxs->ll_relat_cnt));
        }
        if (pxs->ll_tag_count) {
            SPRTF(", tags %s",
                get_I64_Stg(pxs->ll_tag_count));
        }
        if (pxs->ll_nd_cnt) {
            SPRTF(", nd %s",
                get_I64_Stg(pxs->ll_nd_cnt));
        }
        SPRTF(", done %s.\n",
            get_k_num(total_read));
    }
}

void clean_nodem( PNODEM pn )
{
    pn->ulid = -1; //="133840431" 
    pn->lat = BAD_LL; // ="-31.7156299" 
    pn->lon = BAD_LL; //="148.6700669" 
}

void clean_stats()
{
    PXSTATS pxs = &xStats;
    memset(pxs,0,sizeof(xStats)); // clear the stats
    clean_nodem(&nFirst);
    clean_nodem(&nMin);
    clean_nodem(&nMax);
    clean_nodem(&nLast);
    nMin.lat = 400.0;
    nMin.lon = 400.0;
    nMax.lat = -400.0;
    nMax.lon = -400.0;
}

typedef struct tagXMLTAGS {
    char *key;
    char *val;
}XMLTAGS, *PXMLTAGS;
#define MY_MAX_TAGS 64
static XMLTAGS xmlTags[MY_MAX_TAGS];
static int next_tag;

////////////////////////////////////////////////////////////////////////////////////
// Parse an xml line < ... >, 
// splitting it into key and value
//////////////////////////////
int get_xml_tags( char *xbgn, char *xend )
{
    int iret = 0;
    next_tag = 0;
    int c, d;
    xbgn++; // skip '<'
    while ((c = *xbgn) && (c > ' ') && (xbgn < xend)) xbgn++;   // skip type, tag in this case
    // gather attributes
    while (xbgn < xend) {
        while ((*xbgn <= ' ')&&(xbgn < xend)) xbgn++;
        c = *xbgn;
        if ((c == '/')||(c == '>'))
            break;
        xmlTags[next_tag].key = xbgn;
        while ((c = *xbgn) && (c > ' ') && (c != '=') && (c != '/') && (c != '>') && (xbgn < xend)) xbgn++;
        if (c == '=') {
            xbgn++; // skip '='
            d = *xbgn++; // skip quote '"' or "'"
            xmlTags[next_tag++].val = xbgn; // set begin
            while ((c = *xbgn) && (c != d) && (xbgn < xend)) xbgn++;
            xbgn++; // skip trailing quote
        } else {
            xmlTags[next_tag++].val = 0;
        }
        if (next_tag >= MY_MAX_TAGS) {
            SPRTF("Recompile with larger MY_MAX_TAGS\n");
            check_me("Need more tag buffer");
            exit(1);
        }
    }
    return iret;
}


#define v_nodem (v_id | v_lat | v_lon)
int add_node(char *xbgn, char *xend)
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    PXSTATS pxs = &xStats;
    NODEM n;
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'i') {
                n.ulid = strtoul(cp,0,10);
                valid |= v_id;
            } else if (c == 'l') {
                if (kp[1] == 'a') {
                    n.lat = atof(cp);
                    valid |= v_lat;
                } else if (kp[1] == 'o') {
                    n.lon = atof(cp);
                    valid |= v_lon;
                }
            }
        }
        if (valid == v_nodem)
            break;
    }
    if (valid == v_nodem) {
        static bool dn_first_node = false;
        if (dn_first_node) {
            if (n.lat > nMax.lat)
                nMax.lat = n.lat;
            if (n.lat < nMin.lat)
                nMin.lat = n.lat;
            if (n.lon > nMax.lon)
                nMax.lon = n.lon;
            if (n.lon < nMin.lon)
                nMin.lon = n.lon;
            if (n.ulid <= nLast.ulid)
                node_id_seq_err++;
        } else {
            dn_first_node = true;
            nFirst = n;
            nMin = n;
            nMax = n;
        }

        if (store_nodes) {
            APT apt;
            apt.lat = n.lat;
            apt.lon = n.lon;
            apt.ulid = n.ulid;
            mNodes[n.ulid] = apt;
        }
        nLast = n;
    }
    return iret;
}

#if 0   // 0000000000000000000000000000000000000
/////////////////////////////////////////////////////////////////
// utiltity function - should move to utils library
// HOWEVER, it is VERY SLOW, when checking say more than a million itmes
// REPLACED with using 'map', - seems MUCH faster
bool stg_in_vec(std::string &v, vSTG &vs)
{
    std::string t;
    size_t ii, max = vs.size();
    for (ii = 0; ii < max; ii++) {
        t = vs[ii];
        if (v == t)
            return true;
    }
    return false;
}
#endif // 000000000000000000000000000000000000000

/////////////////////////////////////////////////////////////////
typedef std::map<std::string,int> mSTGC;
typedef std::pair<std::string,int> mSTGCPair;

typedef mSTGC::iterator mSTGCi;
typedef std::map<std::string,mSTGC> mTAGS;
// TOO SLOW! - typedef std::map<std::string,vSTG> mTAGS;
typedef mTAGS::iterator mTAGSi;
static mTAGS mTags;

static PWAYM last_way = 0;
static vWAYM vWays;

#define v_tag (v_k|v_v)
int add_tag( char *xbgn, char *xend )
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    TAG tag;
    char *ptmp = GetNxtBuf();
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'k') {
                strcpy2qt(ptmp,cp,xend);
                tag.k = ptmp;
                valid |= v_k;
            } else if (c == 'v') {
                strcpy2qt(ptmp,cp,xend);
                tag.v = ptmp;
                valid |= v_v;
            }
        }
        if (valid == v_tag)
            break;
    }
    if (valid == v_tag) {
        if (do_xg_out) {
            if (last_xml_item == xml_way) {
                if (last_way)
                    last_way->vTags.push_back(tag); // keep way tags
            }
            // nodes are now mapped, so no last_node pointer kept,
            // and anyway, NODEM (short form) has no vTags!!!
            //else if (last_xml_item == xml_node) {
            //    if (last_node)
            //        last_node->vTags.push_back(tag);
            //}
        }
        if (show_tags) {
            mTAGSi iter, iend = mTags.end();
            mSTGC msc;
            iter = mTags.find(tag.k);
            if (iter == iend) {
                msc[tag.v] = 1;
                mTags[tag.k] = msc;
            } else {
                mSTGC *pmsc = &(*iter).second;
                mSTGCi it, ie = pmsc->end();
                it = pmsc->find(tag.v);
                if (it == ie) {
                    pmsc->insert(mSTGCPair(tag.v,1));
                } else {
                    (*it).second++;
                }
            }
        }
    }
    return iret;
}

int list_tags()
{
    static char _s_buf[1024];
    int iret = 0;
    mTAGSi iter, iend = mTags.end();
    mSTGC *psc;
    mSTGCi sci;
    std::string k,v;
    size_t max, cnt, len;
    size_t max_len = 0;
    long long tot_keys = 0; // get key count
    long long tot_vals = 0;
    long long tot_tot = 0;
    for (iter = mTags.begin(); iter != iend; iter++) {
        tot_keys++;
        k = (*iter).first;
        len = k.size();
        if (len > max_len) {
            max_len = len;
        }
    }
    SPRTF("\nList of %d mTags 'keys', alphabetic...\n", (int)tot_keys);
    char *cp = _s_buf;
    tot_keys = 0;
    for (iter = mTags.begin(); iter != iend; iter++) {
        tot_keys++;
        k = (*iter).first;
        psc = &(*iter).second;
        max = psc->size();
        strcpy(cp, k.c_str());
        while (strlen(cp) < max_len) {
            strcat(cp, " ");
        }
        //SPRTF("Key '%s' seen with %d values\n", k.c_str(), (int)max);
        tot_vals += max;
        cnt = 0;
        SPRTF("%s ", cp);
        for (sci = psc->begin(); sci != psc->end(); sci++) {
            max = (*sci).second;
            v = (*sci).first;
            tot_tot += max;
            SPRTF("'%s'(%d), ", v.c_str(), (int)max);
            cnt++;
            //if (tag_wrap && (cnt == tag_wrap)) {
            //    cnt = 0;
            //    SPRTF("\n");
            //}
        }
        //if (cnt)
            SPRTF("\n");

    }
    SPRTF("Shown %s tags, %s keys, %s values\n",
        get_I64_Stg(tot_tot),
        get_I64_Stg(tot_keys),
        get_I64_Stg(tot_vals));

    return iret;
}

#if 0 // 0000000000000000000000000000000000000000000000000
// see better listing above...
int list_tags_org()
{
    int iret = 0;
    mTAGSi iter, iend = mTags.end();
    mSTGC *psc;
    mSTGCi sci;
    std::string k, v;
    size_t max, cnt;
    long long tot_keys = 0;
    long long tot_vals = 0;
    long long tot_tot = 0;
    for (iter = mTags.begin(); iter != iend; iter++) {
        tot_keys++;
        k = (*iter).first;
        psc = &(*iter).second;
        max = psc->size();
        SPRTF("Key '%s' seen with %d values\n", k.c_str(), (int)max);
        tot_vals += max;
        cnt = 0;
        for (sci = psc->begin(); sci != psc->end(); sci++) {
            max = (*sci).second;
            v = (*sci).first;
            tot_tot += max;
            SPRTF("'%s'(%d) ", v.c_str(), (int)max);
            cnt++;
            if (tag_wrap && (cnt == tag_wrap)) {
                cnt = 0;
                SPRTF("\n");
            }
        }
        if (cnt)
            SPRTF("\n");

    }
    SPRTF("Shown %s tags, %s keys, %s values\n",
        get_I64_Stg(tot_tot),
        get_I64_Stg(tot_keys),
        get_I64_Stg(tot_vals));

    return iret;
}

#endif // #if 0 // 0000000000000000000000000000000000000000000000000

/////////////////////////////////////////////////////////////////////////////
// <way id="169178885" version="1" timestamp="2012-06-26T23:51:59Z" 
// changeset="12031826" uid="168136" user="goldfishxyz">

void clean_waym( PWAYM pw )
{
    pw->ulid = -1; //="133840431" 
    //pw->timestamp = 0; //="2010-10-11T19:55:07Z" 
    //pw->uluid = 0;    // ="79144"
    //pw->user = "";   //="John H" 
    //pw->visible = false;   //="true" 
    //pw->version = 0;    //="5" 
    //pw->ulchangeset = 0;  //="6015441" 
    //pw->valid = 0;
    pw->vTags.clear();
    pw->vNDm.clear();
}

int add_way( char *xbgn, char *xend )
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    WAYM w;
    clean_waym(&w);
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'i') {
                w.ulid = strtoul(cp,0,10);
                valid |= v_id;
            }
        }
        if (valid == v_id)
            break;
    }
    if (valid == v_id) {
        size_t sz = vWays.size();
        vWays.push_back(w);
        last_way = &vWays[sz];
    } else {
        last_way = 0;
    }

    return iret;
}

// <nd ref="1803431261"/>
int add_nd( char *xbgn, char *xend )
{
    if (!last_way || (last_xml_item != xml_way)) {
        return 1;
    }
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    NDM ndm;
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'r') {
                ndm.ulref = strtoul(cp,0,10);
                last_way->vNDm.push_back(ndm);
                break;
            }
        }
    }
    return iret;
}

/////////////////////////////////////////////////////////////////////////////
// Have an XML item < ... >
////////////////////////////////////////////////////////////////////////////
int process_xml( char *xbgn, char *xend )
{
    int iret = 0;
    PXSTATS pxs = &xStats;
    char *tmp;
    char c1 = xbgn[1];
    pxs->ll_xml_items++;
    if (VERB1) {
        if (( pxs->ll_xml_items % show_count ) == 0 ) {
            show_stats("");
        }
    }
    if (c1 == '?') {
        // header
        last_xml_item = xml_none;
        pxs->ll_xml_hdr++;
        return iret;
    } else if (c1 == '/') {
        // close
        last_xml_item = xml_none;
        pxs->ll_xml_close++;
        pxs->ll_xml_open--;
        return iret;
    } else if (c1 == 'o') {
        // osm
        last_xml_item = xml_none;
        pxs->ll_xml_osm++;
        if (!isxmlend)
            pxs->ll_xml_open++;
        return iret;
    }
    char c2 = xbgn[2];
    //char c3 = xbgn[3];
    if (!isxmlend)
        pxs->ll_xml_open++;
    if (c1 == 'n') {
        if (c2 == 'o') {
            // node
            if (VERB1) {
                if (pxs->ll_node_count == 0) {
                    show_stats("First node");
                }
            }
            if (!isxmlend)
                last_xml_item = xml_node;
            if (show_node_id) {
                iret = add_node(xbgn,xend);
            }
            pxs->ll_node_count++;
        } else if (c2 == 'd') {
            // nd
            if (do_xg_out) {
                iret = add_nd(xbgn,xend);
            }
            pxs->ll_nd_cnt++;
        } else {
            tmp = GetNxtBuf();
            sprintf(tmp,"What is this '%c'", (char)c1);
            check_me(tmp);
        }
    } else if (c1 == 't') {
        // tag
        pxs->ll_tag_count++;
        if (show_tags || do_xg_out) {
            iret = add_tag(xbgn,xend);
        }
    } else if (c1 == 'w') {
        // way
        if (VERB1) {
            if (pxs->ll_way_cnt == 0) {
                show_stats("First way");
            }
        }
        pxs->ll_way_cnt++;
        if (do_xg_out) {
            iret = add_way(xbgn,xend);
        }
        if (!isxmlend) {
            last_xml_item = xml_way;
        }

    } else if (c1 == 'r') {
        // relation
        if (VERB1) {
            if (pxs->ll_relat_cnt == 0) {
                show_stats("First relat");
            }
        }
        pxs->ll_relat_cnt++;
    } else if (c1 == 'm') {
        // member
        pxs->ll_memb_cnt++;
    } else if (c1 == 'b') {
        // bounds
    } else if (c1 == 'c') {
        // changeset
        pxs->ll_chset_count++;
    } else {
        tmp = GetNxtBuf();
        sprintf(tmp,"What is this '%c'", (char)c1);
        check_me(tmp);
    }

    return iret;
}
////////////////////////////////////////////////////////////////////////////
static vFEATS vFeats;
static FEAT feat;
int write_features() 
{
    int iret = 0;
    size_t ii, max = vWays.size();
    mNODESi iter, iend = mNodes.end();
    size_t i3, max3;
    size_t i4, max4;
    PWAYM pw;
    PTAG pt;
    PNDM pndm;
    APT apt;
    PFEAT pf = &feat;
    double lat,lon;
    char *ptmp;
    max3 = 0;
    for (ii = 0; ii < max; ii++) {
        pw = &vWays[ii];
        max3 += pw->vNDm.size();
    }
    if (VERB1) {
        SPRTF("%s: Have collects %d ways, with %d nd's, to apply to %s nodes\n", module,
            (int)max, (int)max3, get_I64_Stg(xStats.ll_node_count));
    }
    for (ii = 0; ii < max; ii++) {
        pw = &vWays[ii];
        max3 = pw->vNDm.size();
        max4 = pw->vTags.size();
        clean_feat(pf);
        lat = BAD_LL;
        for (i3 = 0; i3 < max3; i3++) {
            pndm = &pw->vNDm[i3];
            iter = mNodes.find(pndm->ulref);
            if (iter == iend) {
                SPRTF("pn ref=%ld NOT found!\n", pndm->ulref);
            } else {
                apt = (*iter).second;
                pf->vPoints.push_back(apt);
                lat = apt.lat;
                lon = apt.lon;
            }
        }
        for (i4 = 0; i4 < max4; i4++) {
            pt = &pw->vTags[i4];
            if ((pt->k == "name") && (lat != BAD_LL)) {
#ifdef USE_NEW_FEAT
                ANNO a;
                a.anno = pt->v;
                a.pt.lat = lat;
                a.pt.lon = lon;
                pf->vAnnos.push_back(a);
#else
                pf->anno = pt->v;
                pf->pt.lat = lat;
                pf->pt.lon = lon;
#endif       
            } else {
                if ((pf->color == 0) && (strcmp(pt->k.c_str(),"created_by"))) {
                    pf->color = get_color_for_tag(pt);
                }
                ptmp = GetNxtBuf();
                sprintf(ptmp,"k='%s' v='%s'", pt->k.c_str(), pt->v.c_str());
                pf->comments.push_back(ptmp);
            }
        }
        vFeats.push_back(*pf);
    }
    iret |= output_feats( vFeats, out_file, verbose_xgs, xg_auto_color, verbosity );

    return iret;
}

////////////////////////////////////////////////////////////////////////////
void show_results(double bgn)
{
    if (do_xg_out && out_file) {
        write_features();
    }
    if (VERB1) {
        PXSTATS pxs = &xStats;
        double diff = (get_seconds() - bgn);
        SPRTF("\n");
        SPRTF("%s: Done %s %s bytes (%s), in %s.\n", module, 
            ((total_read > file_size) ? "uncompressed" : ""),
            get_nn(get_I64_Stg(total_read)), get_k_num(total_read), get_seconds_stg(diff) );
        SPRTF( "%s: ", module );
        show_stats("End");
        SPRTF("%s: changesets %s, members %s, open %s", module,
            get_I64_Stg(pxs->ll_chset_count), 
            get_I64_Stg(pxs->ll_memb_cnt),
            get_I64_Stg(pxs->ll_xml_open) );
        if (total_read > file_size) {
            UInt64 dif = total_read - file_size;
            double pct = ( (double)dif / (double)total_read ) * 100.0;
            pct = (double)((int)(pct * 100.0)) / 100.0;
            SPRTF(", comp %s%%", double_to_stg(pct));
        }
        if (diff > 10.0) {
            // calc bytes per second
            UInt64 bps = total_read / (UInt64)diff;
            SPRTF(", %s/sec", get_k_num(bps));
        }
        SPRTF("\n");
    }
    if (show_tags) {
        list_tags();
    }
    if (show_node_id) {
        //                                          L  B  T  R
        SPRTF("%s: node id: first %lu, last %lu, bbox=%s,%s,%s,%s", module,
            nFirst.ulid, nLast.ulid,
            double_to_stg(nMin.lon),
            double_to_stg(nMin.lat),
            double_to_stg(nMax.lon),
            double_to_stg(nMax.lat) );
        SPRTF(", %s seq.errs",
            ((node_id_seq_err) ? get_I64_Stg(node_id_seq_err) : "No"));
        SPRTF("\n");
    }
}

////////////////////////////////////////////////////////////////////////////
// Process the data, xml item by item
///////////////////////////////////////////////////////////////////////////
int process_data( osmFile &bzr )
{
    int read;
    int read_size = MX_RD_BUFF;
    int pc, c, i, read_off  = 0;
    Bool inxml = False;
    UChar *xbgn, *xend;
    pc = 0;
    double bgn = get_seconds();
    clean_stats();

    while ((read = bzr.osmRead(&rd_buff[read_off], read_size)) != -1) {
        total_read += read; // add to total
        read += read_off;   // bump by any remainder
        for (i = 0; i < read; i++) {
            c = rd_buff[i];
            if (inxml) {
                if (c == '>') {
                    inxml = False;
                    xend = &rd_buff[i];
                    isxmlend = (pc == '/') ? true : false;
                    process_xml( (char *)xbgn, (char *)xend );  // got '<...>'
                }
                pc = c;
            } else if (c == '<') {
                xbgn = &rd_buff[i];
                inxml = True;
            }
        }
        if (inxml) {    // failed to get next complete '< ... >', so read more
            // copy xbgn to end to buffer head
            size_t size = read - (xbgn - rd_buff);
            if (size) {
                memmove(rd_buff,xbgn,size);
                read_off = (int)size;
                read_size = (int)(MX_RD_BUFF - size);
            } else {
                read_off = 0;
                read_size = MX_RD_BUFF;
            }
            inxml = False;
        } else {
            read_off = 0;
            read_size = MX_RD_BUFF;
        }
    }

    show_results(bgn);

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////
// Just a DEBUG test of an 'example' file
////////////////////////////////////////////////////////////////////////////////
int test_example()
{
    int iret = 0;
    char *file = (char *)example;
    osmFile bzr;
    FILE *fp = fopen(file,"rb");
    if (!fp) {
        SPRTF("%s: Failed to open file '%s'\n", module, file);
        return 1;
    }
    UInt64 size = bzr.getFileSize(fp);
    if (size == -1) {
        SPRTF("%s: Failed to get size of '%s'\n", module, file);
    } else {
        SPRTF("%s: File '%s' is %s bytes (%s).\n", module, file, get_nn(get_I64_Stg(size)), get_k_num(size));
    }
    fclose(fp);
    bzr.verbose = 9;
    if (bzr.osmOpen(file)) {
        process_data( bzr );
        bzr.osmClose();
    } else {
        iret = 1;
    }
    exit(iret);
}

void give_help( char *name )
{
    SPRTF("%s [Options] in_file\n", module );
    SPRTF("Options:\n");
    SPRTF(" --help   (-h or -?) = This help and exit(2).\n");
    SPRTF(" --verb[nn]     (-v) = Bump or set verbosity. (def=%d)\n", verbosity);
    SPRTF(" --log file     (-l) = Set log file name, (def=%s)\n", def_log_file );
    SPRTF(" --append       (-a) = Append to the log file. Default is a new files.\n");
    SPRTF(" --id           (-i) = Also show first and largest node ids, and bbox.\n");
    SPRTF(" --tags         (-t) = Collect all tag key/value pairs, and show list.\n");
    SPRTF(" --xg file      (-x) = Ouput ways/nd to an xgraph file. Implies show node ids.\n");
    SPRTF(" These options can take lots of extra time.\n");
    SPRTF(" --Color        (-C) = Add auto color to xg file. (def=%s)\n",
        xg_auto_color ? "On" : "Off");
    SPRTF(" --Verb         (-V) = Add comments to xg file. (def=%s)\n",
        verbose_xgs ? "On" : "Off");
    SPRTF(" Naturally these options only apply is an xg file being written.\n");
    SPRTF("\n");
    SPRTF("Purpose: Scan the osm records in the file, and output some\n");
    SPRTF(" statistics on the items found.\n");

}

/////////////////////////////////////////////////////////////////////////
// parse command line argument
////////////////////////////////////////////////////////////////////////
// deal with log file name and append if any
int get_log_file( int argc, char **argv )
{
    int iret = 0;
    int c, i, i2;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'a':
                add_append_log(1);
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                } else {
                    SPRTF("%s: Expected an output log file name to follow %s\n", module, arg);
                    return 1;
                }
                break;
            }
        }
    }

    return iret;
}

int parse_args( int argc, char **argv )
{
    int iret = 0;
    int c, i, i2;
    char *arg, *sarg;
    iret = get_log_file( argc, argv);   // check only for 'a' and 'l' befor anything else
    if (iret)
        return iret;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
                // already dealt with thee two options
            case 'a':
                break;
            case 'l':
                i++;
                break;
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'v':
                verbosity++;
                sarg++;
                while (*sarg) {
                    if (*sarg == 'v') {
                        verbosity++;
                    } else if (ISDIGIT(*sarg)) {
                        verbosity = atoi(sarg);
                    }
                }
                if (VERB1) SPRTF("%s: Set verbosity to %d\n", module, verbosity);
                break;
            case 'i':
                show_node_id = true;
                if (VERB1) SPRTF("%s: Set to show node id, and bbox\n", module);
                break;
            case 't':
                show_tags = true;
                if (VERB1) SPRTF("%s: Set to show tag key, value pairs\n", module);
                break;
            case 'x':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_file = strdup(sarg);
                    do_xg_out = true;
                    store_nodes = true;
                    show_node_id = true;
                    if (VERB1) SPRTF("%s: Set xgraph output to '%s'\n", module, sarg);
                } else {
                    SPRTF("%s: Expect output file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'C':
                xg_auto_color = true;
                if (VERB1) SPRTF("%s: Set xg auto color ON\n", module);
                break;
            case 'V':
                verbose_xgs = true;
                if (VERB1) SPRTF("%s: Set xg verbosity to ON\n", module);
                break;
            default:
                SPRTF("%s: Unknown argument '%s'! Aborting 1\n", module, arg);
                return 1;
            }
        } else {
            if (in_file) {
                SPRTF("%s: Already have input file '%s'. What is this '%s'? Aborting 1\n", module,
                    in_file, arg );
                return 1;
            }
            if (is_file_or_directory64(arg) != DT_FILE) {
                SPRTF("%s: Unable to 'stat' file '%s'! Aborting 1\n", module, arg);
                return 1;
            }
            file_size = get_last_file_size64();
            in_file = strdup(arg);
        }
    }
    if (in_file == 0) {
        SPRTF("%s: No input file found in command! Aborting 1\n", module);
        iret = 1;
    }
    return iret;

}

void clean_up()
{
    size_t ii, max = vWays.size();
    PWAYM pw;
    for (ii = 0; ii < max; ii++) {
        pw = &vWays[ii];
        pw->vNDm.clear();
        pw->vTags.clear();
    }

    vWays.clear();
    mNodes.clear();
    mTags.clear();
}

//////////////////////////////////////////////////////////////////////
// main() OS entry
//////////////////////////////////////////////////////////////////////
int main( int argc, char **argv )
{
    int iret = 0;
    double bgn = get_seconds();
    set_log_file((char *)def_log_file,false);

    if (run_debug_example) {
        iret = test_example();  // just a DEBUG example
        return iret;
    }
    
    if (argc < 2) {
        give_help(argv[0]);
        SPRTF("%s: No input file found in command! Aborting 1\n", module );
        return 1;
    }
    iret = parse_args(argc,argv);
    if (iret)
        return iret;
    osmFile bzr;
    bzr.verbose = verbosity;
    if (VERB9) {
        osm_FileType ot = bzr.osmFileType(in_file);
        if (ot == ot_Bz2) {
            SPRTF("%s: Appears to be a Bzip2 compressed file.\n", module);
        } else if (ot == ot_Gz1) {
            SPRTF("%s: Appears to be a zlib compressed file.\n", module);
        } else if (ot == ot_Gz2) {
            SPRTF("%s: Appears can be handled by zlib...\n", module);
        } else if (ot == ot_Txt) {
            SPRTF("%s: Appears to be a text file.\n", module);
        } else {
            SPRTF("%s: Unable to determine file type.\n", module);
        }
    }
    if (bzr.osmOpen(in_file)) {
        if (VERB1) {
            long long fs = file_size;
            char *tmp = get_I64_Stg(fs);
            SPRTF("%s: Doing '%s', of %s bytes (%s)...\n", module, in_file, get_nn(tmp), get_k_num(fs));
        }
        process_data( bzr );
        bzr.osmClose();
    } else {
        SPRTF("%s: Failed to open file '%s'! Aborting 1\n", module, in_file);
        iret = 1;
    }

    clean_up();

    if (VERB1) {
        SPRTF("%s: ran for %s, exit(%d)\n", module, get_seconds_stg(get_seconds() - bgn), iret );
    }

    close_log_file();
    return iret;
}

//////////////////////////////////////////////////////////////////////////////////
// END OSMSTATS
//////////////////////////////////////////////////////////////////////////////////

// eof = osmstats.cxx
