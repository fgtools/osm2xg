/*\
 * osmstats.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _OSMSTATS_HXX_
#define _OSMSTATS_HXX_

#ifndef DEF_SHOW_COUNT
#define DEF_SHOW_COUNT 1000000
#endif // DEF_SHOW_COUNT
#ifndef MX_RD_BUFF
#define MX_RD_BUFF 2048
#endif // MX_RD_BUFF

#endif // #ifndef _OSMSTATS_HXX_
// eof - osmstats.hxx
