/*\
 * osmtags.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/


#define USE_STD_MAP

#include <sstream>
#include <stdio.h>
#ifdef _MSC_VER
#include <conio.h>
#else
#include <stdlib.h> // for atoi(), ...
#include <string.h> // for memset(), ...
#endif
#include <map>
#include <math.h>   // for atof(), ...
#include "sprtf.hxx"
#include "utils.hxx"
#include "osm2xg.hxx"
#include "osmtags.hxx"

static const char *module = "osmtags";

// forward reference
int process_buffer(char *text, size_t len);
int use_data();
void clean_up();

static const char *example1 = "F:\\Projects\\osm2xg\\build\\ygil.osm";
static const char *example4 = "F:\\Projects\\osm2xg\\build\\ygil1.osm";
static const char *example = "D:\\SAVES\\OSM\\australia.osm";
static const char *example3 = "C:\\Users\\Public\\Documents\\JOSM\\ygil.osm";

static vSTG vIn_Files;
static bool verbose_xg = false;
static bool add_each_file = false;
static std::string base = "temp";
//static std::string out_file = "tempallf.xg";
static std::string out_file = "tempall1.xg";
static int verbosity = 0;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

static int check_me( const char *msg )
{
    int i = SPRTF("CHECK ME: %s\n",msg);
#ifdef _MSC_VER
    if (!bIsRedON()) {
        //_kbhit();
        SPRTF("Any key to continue!\n");
        _getch();
    }
#endif
    return i;
}

// stats
typedef struct tagXSTATS {
    int xml_items, chset_count, tag_count, node_count, relat_cnt, memb_cnt, way_cnt, nd_cnt;
}XSTATS, *PXSTATS;

static XSTATS xStats;

void give_help( char *name )
{
    SPRTF("\n");
    SPRTF("%s version %s\n", name, VERSION);
    SPRTF("Usage: osm2xg [options] in_file1.osm [in_file2.osm ... in_fileN.osm]\n");
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = Give this help and exit(2)\n");
    SPRTF(" --base name   (-b) = Set 'base' name for each file. (def=%s)\n", base.c_str());
    SPRTF(" Setting a base name implies write each feature to a numbered file.\n");
    SPRTF(" --each        (-e) = Write each feature to a numbered file list. (def=%s)\n",
        (add_each_file ? "On" : "Off"));
    SPRTF(" --log file    (-l) = Set log file. (def=%s)\n", get_log_file());
    SPRTF(" --out file    (-o) = Set output file. (def=%s)\n", out_file.c_str());
    SPRTF(" --verb[n]     (-v) = Bump/Set verbosity. (def=%d)\n", verbosity );
    SPRTF(" --xgverb      (-x) = Add extra comments to xgraph file(s). (def=%s)\n",
        (verbose_xg ? "On" : "Off"));
    SPRTF(" Includes index, way id, and distance from previous (simple Pythagorean calc)\n");

}

int check_log_file(int argc, char **argv)
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    set_log_file((char *)"tempexf.txt",false);
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'b':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    base = sarg;
                    if (VERB1) SPRTF("%s: Set base of numbered file to '%s'\n", module, base.c_str());
                } else {
                    printf("%s: Expected base file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                // NOTE fall through - setting base implies each feature to new numbered file
            case 'e':
                add_each_file = true;
                if (VERB1) SPRTF("%s: Set to write each feature to '%snn.xg", module, base.c_str());
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    set_log_file(sarg,false);
                } else {
                    printf("%s: Expected file name to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            }
        }
    }
    return iret;
}


int parse_command( int argc, char **argv )
{
    int iret = 0;
    int i, i2, c;
    char *arg, *sarg;
    iret = check_log_file(argc,argv);
    if (iret)
        return iret;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
            case 'l':
                i++;    // already dealt with
                break;
            case 'o':
                if (i2 < argc) {
                    i++;
                    sarg = argv[i];
                    out_file = sarg;
                    if (VERB1) SPRTF("%s: Set out file to '%s'.\n", module, out_file.c_str());
                } else {
                    SPRTF("%s: Expected out_file to follow '%s'! Aborting...\n", module, arg);
                    return 1;
                }
                break;
            case 'v':
                verbosity++;
                while (*sarg) {
                    if (*sarg == 'v') {
                        verbosity++;
                    } else if (ISDIGIT(*sarg)) {
                        verbosity = atoi(sarg);
                    }
                }
                if (VERB1) SPRTF("%s: Set verbosity to %d\n", module, verbosity);
                break;
            default:
                SPRTF("%s: Unknown argument '%s'! Aborting...\n", module, arg);
                return 1;

            }
        } else {
#ifdef _MSC_VER
            if (is_file_or_directory64(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#else
            if (is_file_or_directory32(arg) != DT_FILE) {
                SPRTF("%s: Can NOT 'stat' file '%s'! Aborting...\n", module, arg );
                return 1;
            }
#endif
            vIn_Files.push_back(arg);
            if (VERB1) SPRTF("%s: Added file '%s' for processing.\n", module, arg);
        }

    }
    i = (int)vIn_Files.size();
#ifndef NDEBUG
#ifdef _MSC_VER
    if ((i == 0) && (is_file_or_directory64((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    

#else
    if ((i == 0) && (is_file_or_directory32((char *)example) == DT_FILE)) {
        vIn_Files.push_back(example);
        SPRTF("%s: Loading default example '%s'!\n", module, example);
        i++;
    }    
#endif
#endif
    if (i == 0) {
        SPRTF("%s: No input files found in command! Aborting...\n", module );
        iret = 1;
    }
    return iret;

}

int process_in_files()
{
    int iret = 0;
    size_t ii,max = vIn_Files.size();
    std::stringstream s;
    std::string indent = " ";
    char *text;
    size_t len, res;
    PXSTATS pxs = &xStats;
    double bgn = get_seconds();

    memset(pxs,0,sizeof(xStats)); // clear the stats

    for (ii = 0; ii < max; ii++) {
        double bgnf = get_seconds();
        std::string f = vIn_Files[ii];
        char *file = (char *)f.c_str();
#ifdef _MSC_VER
        if (is_file_or_directory64(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        __int64 i64 = get_last_file_size64();
        __int64 size;
        size_t bytes = sizeof(size_t);
        if (bytes == sizeof(unsigned int)) {
            size = UINT_MAX;
            SPRTF("%s: Error: size_t is ONLY an 'unsigned int'\n", module, bytes);
            exit(1);
        } else if (bytes == sizeof(unsigned long))
            size = ULONG_MAX;
        else if (bytes == sizeof(unsigned long long))
            size = ULLONG_MAX;
        // memory allocation limit for a 32-bit app on a 64-bit system
        // You should link your application with /LARGEADDRESSAWARE to make more 
        // than 2GB available to the application. Then you can use up to 4GB 
        // on a 64-bit OS in a 32-bit application.
        // australia.osm is 2798125270
        if (i64 > 3000000000) {
            SPRTF("%s: Files size %s TOO big to allocate memory\n", module, get_I64_Stg( i64 ));
            exit(1);
        }
        len = (size_t)i64;
#else
        if (is_file_or_directory32(file) != DT_FILE) {
            SPRTF("%s: Can NOT 'stat' file %s\n", module, file );
            return 1;
        }
        len = get_last_file_size32();
#endif
        if (len > 500000000) {
            SPRTF("%s: Large file... loading can take time... patients...\n");
        }
        text = new char[len+2];
        if (!text) {
            SPRTF("%s: Memory allocation FAILED on %d bytes!\n", module, (int)len);
            return 1;
        }
        FILE *fp = fopen(file,"rb"); // text file
        if (!fp) {
            SPRTF("%s: Unable to open file %s!\n", module, file );
            delete text;
            return 1;
        }
        //res = fread(text,len,1,fp);
        //if (res != 1) {
        res = fread(text,1,len,fp);
        if (res != len) { // this fails if not "rb"
            SPRTF("%s: Read FAILED! re %d, got %d\n", module, (int)len, (int)res );
            fclose(fp);
            delete text;
            return 1;
        }
        fclose(fp);

        text[len] = 0;


        SPRTF("%s: Loaded file '%s', %u bytes, in %s\n", module, file, (int)len,
            get_seconds_stg( get_seconds() - bgnf) );

        iret = process_buffer(text,len);

        delete text;

        if (iret)
            break;
    }

    SPRTF( "%s: xml items %d, changesets %d, tags %d, nodes %d, relat %d, memb %d, way %d, nd %d, in %s\n", module,
        pxs->xml_items, pxs->chset_count, pxs->tag_count, pxs->node_count, 
        pxs->relat_cnt, pxs->memb_cnt, pxs->way_cnt, pxs->nd_cnt,
        get_seconds_stg( get_seconds() - bgn ) );

    ///////////////////////////////////////////
    iret |= use_data(); // whole purpose of the load
    //////////////////////////////////////////
    SPRTF("%s: Done data output... total elapsed %s\n", module, get_seconds_stg( get_seconds() - bgn ) );
    return iret;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    double bgns = get_seconds();
    // test_check_me();
    iret = parse_command(argc,argv);
    if (iret)
        return iret;
    iret = process_in_files();
    clean_up();

    SPRTF("%s: End processing in %s\n", module, get_seconds_stg( get_seconds() - bgns ));
    return iret;
}

////////////////////////////////////////////////////////////////////////////

static bool isxmlend = false;
typedef struct tagXMLTAGS {
    char *key;
    char *val;
}XMLTAGS, *PXMLTAGS;
#define MY_MAX_TAGS 64
static XMLTAGS xmlTags[MY_MAX_TAGS];
static int next_tag;

int get_xml_tags( char *xbgn, char *xend )
{
    int iret = 0;
    next_tag = 0;
    int c, d;
    xbgn++; // skip '<'
    while ((c = *xbgn) && (c > ' ') && (xbgn < xend)) xbgn++;   // skip type
    // gather attributes
    while (xbgn < xend) {
        while ((*xbgn <= ' ')&&(xbgn < xend)) xbgn++;
        c = *xbgn;
        if ((c == '/')||(c == '>'))
            break;
        xmlTags[next_tag].key = xbgn;
        while ((c = *xbgn) && (c > ' ') && (c != '=') && (c != '/') && (c != '>') && (xbgn < xend)) xbgn++;
        if (c == '=') {
            xbgn++; // skip '='
            d = *xbgn++; // skip quote '"' or "'"
            xmlTags[next_tag++].val = xbgn; // set begin
            while ((c = *xbgn) && (c != d) && (xbgn < xend)) xbgn++;
            xbgn++; // skip trailong quote
        } else {
            xmlTags[next_tag++].val = 0;
        }
        if (next_tag >= MY_MAX_TAGS) {
            SPRTF("Recompile with larger MY_MAX_TAGS\n");
            check_me("Need more tag buffer");
            exit(1);
        }


    }
    return iret;
}

#define xml_none 0
#define xml_node 1
#define xml_way  2
#define xml_rel  3

static int last_xml_item = xml_none;

typedef std::map<std::string,vSTG> mTAGS;
typedef mTAGS::iterator mTAGSi;
static mTAGS mTags;
#define v_nodem (v_id|v_lat|v_lon)
static vWAYM vWays;
static PWAYM last_way = 0;
static vFEATS vFeats;
static FEAT feat;

int use_data()
{
    int iret = 0;
    mTAGSi iter, iend = mTags.end();
    vSTG *pvs;
    std::string k,v;
    size_t ii, max, cnt, wrap = 10;
    for (iter = mTags.begin(); iter != iend; iter++) {
        k = (*iter).first;
        pvs = &(*iter).second;
        max = pvs->size();
        SPRTF("Key '%s' seen with %d values\n", k.c_str(), (int)max);
        cnt = 0;
        for (ii = 0; ii < max; ii++) {
            v = pvs->at(ii);
            SPRTF("'%s' ", v.c_str());
            cnt++;
            if (cnt == wrap) {
                cnt = 0;
                SPRTF("\n");
            }
        }
        if (cnt)
            SPRTF("\n");

    }
    return iret;
}

#define v_tag (v_k|v_v)
bool stg_in_vec(std::string &v, vSTG &vs)
{
    std::string t;
    size_t ii, max = vs.size();
    for (ii = 0; ii < max; ii++) {
        t = vs[ii];
        if (v == t)
            return true;
    }
    return false;
}

int add_tag( char *xbgn, char *xend )
{
    int i, c, iret = 0;
    PXMLTAGS pxt = xmlTags;
    int valid = 0;
    char *cp, *kp;
    iret = get_xml_tags(xbgn,xend);
    TAG tag;
    char *ptmp = GetNxtBuf();
    for (i = 0; i < next_tag; i++) {
        kp = pxt[i].key;
        cp = pxt[i].val;
        if (kp && cp) {
            c = *kp;
            if (c == 'k') {
                strcpy2qt(ptmp,cp,xend);
                tag.k = ptmp;
                valid |= v_k;
            } else if (c == 'v') {
                strcpy2qt(ptmp,cp,xend);
                tag.v = ptmp;
                valid |= v_v;
            }
        }
        if (valid == v_tag)
            break;
    }
    if (valid == v_tag) {
        mTAGSi iter, iend = mTags.end();
        iter = mTags.find(tag.k);
        if (iter == iend) {
            mTags[tag.k].push_back(tag.v);
        } else {
            vSTG vs = (*iter).second;
            if (!stg_in_vec(tag.v,vs)) {
                mTags[tag.k].push_back(tag.v);
            }
        }
    }
    return iret;
}


int process_xml( char *xbgn, char *xend )
{
    int iret = 0;
    PXSTATS pxs = &xStats;
    char *tmp;
    char c1 = xbgn[1];
    pxs->xml_items++;
    if (c1 == '?') {
        // header
        last_xml_item = xml_none;
        return iret;
    } else if (c1 == '/') {
        // close
        last_xml_item = xml_none;
        return iret;
    } else if (c1 == 'o') {
        // osm
        last_xml_item = xml_none;
        return iret;
    }
    char c2 = xbgn[2];
    char c3 = xbgn[3];
    if (c1 == 'n') {
        if (c2 == 'o') {
            // node
            pxs->node_count++;
            if (!isxmlend)
                last_xml_item = xml_node;
        } else if (c2 == 'd') {
            // nd
            pxs->nd_cnt++;
        } else {
            tmp = GetNxtBuf();
            sprintf(tmp,"What is this '%c'", (char)c1);
            check_me(tmp);
        }
    } else if (c1 == 't') {
        // tag
        pxs->tag_count++;
        iret = add_tag( xbgn, xend );
    } else if (c1 == 'w') {
        // way
        pxs->way_cnt++;
        if (!isxmlend)
            last_xml_item = xml_way;
    } else if (c1 == 'r') {
        // relation
        pxs->relat_cnt++;
        if (!isxmlend)
            last_xml_item = xml_rel;
    } else if (c1 == 'm') {
        // member
        pxs->memb_cnt++;
    } else if (c1 == 'b') {
        // bounds
    } else if (c1 == 'c') {
        // changeset
        pxs->chset_count++;
    } else {
        tmp = GetNxtBuf();
        sprintf(tmp,"What is this '%c'", (char)c1);
        check_me(tmp);
    }

    return iret;
}

void clean_up()
{
    double bgns = get_seconds();
    mTags.clear();
    size_t ii,max = vWays.size();
    PWAYM pwm;
    PFEAT pf;
    for (ii = 0; ii < max; ii++) {
        pwm = &vWays[ii];
        pwm->vNDm.clear();
    }
    vWays.clear();
    max = vFeats.size();
    for (ii = 0; ii < max; ii++) {
        pf = &vFeats[ii];
        pf->vPoints.clear();
#ifdef USE_NEW_FEAT
        pf->vAnnos.clear();
#else
        pf->anno.clear();
        pf->comments.clear();
#endif
    }
    vFeats.clear();
    SPRTF("%s: Clean up took %s\n", module, get_seconds_stg( get_seconds() - bgns ));
}



int process_buffer(char *text, size_t len)
{
    int iret = 0;
    int c, pc;
    size_t i;
    int inxml = 0;
    char *xbgn;
    char *xend;
    for (i = 0; i < len; i++) {
        c = text[i];
        if (inxml) {
            if (c == '>') {
                xend = &text[i];
                inxml = 0;
                isxmlend = (pc == '/') ? true : false;
                iret = process_xml( xbgn, xend );
            }
            pc = c;
        } else if (c == '<') {
            xbgn = &text[i];
            inxml = 1;
        }
    }

    return iret;
}

// eof = osmtags.cxx
