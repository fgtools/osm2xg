/******************************************************************************
 * $Id: shp2xg.cxx,v 1.0 2014-06-22 00:00:00 gmclane Exp $
 *
 * Project:  osm2xg
 * Purpose:  Application to dump contents of a shapefile to the terminal in xgraph form.
 * Author:   Geoff R. McLane, reports _AT_ geoffair _DOT_ info
 *
 ******************************************************************************
 * Copyright (c) 2014, Geoff R. McLane
 *
 * This software is available under the GNU GPL license,
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 ******************************************************************************
 *
 * Revision 1.0  2014-07-15 00:00:00  geoff
 * initial cut
 *
 */

#include <string.h>
#include <stdlib.h>
#include <shapefil.h>

// SHP_CVSID("$Id: shp2xg.c,v 1.0 2014-07-15 00:00:00 geoff Exp $")

int main( int argc, char ** argv )
{
    SHPHandle	hSHP;
    int		a, nShapeType, nEntities, i, iPart;
    const char 	*pszPlus;
    double 	adfMinBound[4], adfMaxBound[4];
    int give_help = 0;

    for (a = 1; a < argc; a++) {
        char *currArg = argv[a];
        if (strcmp( currArg, "-h"     ) == 0 || strcmp( currArg, "--h"    ) == 0 ||
            strcmp( currArg, "-help"  ) == 0 || strcmp( currArg, "--help" ) == 0 ||
            strcmp( currArg, "-?"     ) == 0 || strcmp( currArg, "--?"    ) == 0 )
        {
            give_help = 1;
            break;
        }

    }
/* -------------------------------------------------------------------- */
/*      Display a usage message.                                        */
/* -------------------------------------------------------------------- */
    if ( give_help || ( argc < 2 ) )
    {
        printf( "# shp2xg shp_file [shp_file2 [... shp_filen]]\n" );
        exit( 1 );
    }

/* -------------------------------------------------------------------- */
/*      Open the passed shapefile.                                      */
/* -------------------------------------------------------------------- */
    for (a = 1; a < argc; a++) 
    {

        hSHP = SHPOpen( argv[a], "rb" );

        if( hSHP == NULL )
        {
            fprintf( stderr, "# Unable to open : '%s'\n", argv[a] );
            continue;
        }

        /* -------------------------------------------------------------------- */
        /*      Print out the file bounds.                                      */
        /* -------------------------------------------------------------------- */
        SHPGetInfo( hSHP, &nEntities, &nShapeType, adfMinBound, adfMaxBound );

        printf( "# Shapefile Type: %s   # of Shapes: %d\n",
                SHPTypeName( nShapeType ), nEntities );
    
        printf( "# File Bounds: (%.15g,%.15g,%.15g,%.15g) to  (%.15g,%.15g,%.15g,%.15g)\n",
                adfMinBound[0], 
                adfMinBound[1], 
                adfMinBound[2], 
                adfMinBound[3], 
                adfMaxBound[0], 
                adfMaxBound[1], 
                adfMaxBound[2], 
                adfMaxBound[3] );

        /* -------------------------------------------------------------------- */
        /*	Skim over the list of shapes, printing all the vertices.	*/
        /* -------------------------------------------------------------------- */
        for( i = 0; i < nEntities; i++ )
        {
            int		j;
            SHPObject	*psShape;

            psShape = SHPReadObject( hSHP, i );

            if( psShape == NULL )
            {
                fprintf( stderr, "# Unable to read shape %d, terminating object reading.\n", i );
                break;
            }

            if( psShape->bMeasureIsUsed )
                printf( "# Shape:%d (%s)  nVertices=%d, nParts=%d Bounds:(%.15g,%.15g, %.15g, %.15g) to (%.15g,%.15g, %.15g, %.15g)\n",
                        i, SHPTypeName(psShape->nSHPType),
                        psShape->nVertices, psShape->nParts,
                        psShape->dfXMin, psShape->dfYMin,
                        psShape->dfZMin, psShape->dfMMin,
                        psShape->dfXMax, psShape->dfYMax,
                        psShape->dfZMax, psShape->dfMMax );
            else
                printf( "# Shape:%d (%s)  nVertices=%d, nParts=%d Bounds:(%.15g,%.15g, %.15g) to (%.15g,%.15g, %.15g)\n",
                        i, SHPTypeName(psShape->nSHPType),
                        psShape->nVertices, psShape->nParts,
                        psShape->dfXMin, psShape->dfYMin,
                        psShape->dfZMin,
                        psShape->dfXMax, psShape->dfYMax,
                        psShape->dfZMax );

            if( psShape->nParts > 0 && psShape->panPartStart[0] != 0 )
            {
                fprintf( stderr, "# panPartStart[0] = %d, not zero as expected.\n",
                         psShape->panPartStart[0] );
            }

            for( j = 0, iPart = 1; j < psShape->nVertices; j++ )
            {
                const char	*pszPartType = "";

                if( j == 0 && psShape->nParts > 0 )
                    pszPartType = SHPPartTypeName( psShape->panPartType[0] );
            
                if( iPart < psShape->nParts && psShape->panPartStart[iPart] == j )
                {
                    pszPartType = SHPPartTypeName( psShape->panPartType[iPart] );
                    iPart++;
                }

                if( psShape->bMeasureIsUsed ) {
                    if (pszPartType && *pszPartType)
                        printf("# PartType %s\n", pszPartType);
                    printf("%.15g %.15g\n",
                           psShape->padfX[j],
                           psShape->padfY[j] );
                } else {
                    if (pszPartType && *pszPartType)
                        printf("# PartType %s\n", pszPartType);
                    printf("%.15g %.15g\n",
                           psShape->padfX[j],
                           psShape->padfY[j] );
                }
            }
            if (j)
                printf("NEXT\n");

            SHPDestroyObject( psShape );

        }   // for nEntities

        SHPClose( hSHP );
    }
#ifdef USE_DBMALLOC
    malloc_dump(2);
#endif

    return 0;
}

// eof = shp2xg.cxx
