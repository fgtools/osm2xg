/*\
 * utils.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <sstream>
#include <math.h> // for sqrt(), ...
#include <limits> // for std::numeric_limits<double>::epsilon()
#ifdef _MSC_VER
#include <Windows.h>
#else
#include <string.h> // for strlen(), ...
#include <sys/time.h> // for gettimeofday(), ...
typedef unsigned char byte;
#endif
#include "sprtf.hxx"
#include "osm2xg.hxx"
#include "utils.hxx"

#ifndef M_PI
#define M_PI       3.14159265358979323846
#endif

#define RAD2DEG ( 180.0 / M_PI )
#define DEG2RAD ( M_PI / 180.0 )

static const char *module = "utils";

static struct stat buf;
DiskType is_file_or_directory32 ( const char * path )
{
    if (!path)
        return DT_NONE;
	if (stat(path,&buf) == 0)
	{
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

size_t get_last_file_size32() { return buf.st_size; }
#ifdef _MSC_VER
static struct _stat64 buf64;
DiskType is_file_or_directory64 ( const char * path )
{
    if (!path)
        return DT_NONE;
	if (_stat64(path,&buf64) == 0)
	{
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

long long get_last_file_size64() { return buf64.st_size; }
#else
static struct stat64 buf64;
DiskType is_file_or_directory64 ( const char * path )
{
    if (!path)
        return DT_NONE;
	if (stat64(path,&buf64) == 0)
	{
		if (buf.st_mode & M_IS_DIR)
			return DT_DIR;
		else
			return DT_FILE;
	}
	return DT_NONE;
}

long long get_last_file_size64() { return buf64.st_size; }

#endif

bool valid_lat( double lat )
{
    if ((lat >= -90.0)&&
        (lat <=  90.0))
        return true;
    return false;
}
bool valid_lon( double lon )
{
    if ((lon >= -180.0)&&
        (lon <=  180.0))
        return true;
    return false;
}
bool in_world_range( double lat, double lon )
{
    if (valid_lat(lat) && valid_lon(lon))
        return true;
    return false;
}

#ifdef _MSC_VER
bool bIsRedON()
{
    HANDLE hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    DWORD val;
    if ( !GetConsoleMode( hOut, &val ) )
        return true;
    return false;
}
#endif

char *get_I64_Stg( long long val )
{
    char *cp = GetNxtBuf();
#ifdef _MSC_VER
    sprintf(cp,"%I64d",val);
#else
    sprintf(cp,"%lld",val);
#endif
    return cp;
}

char *get_nn(char *num) // nice number nicenum add commas
{
    char *cp = GetNxtBuf();
    size_t len = strlen(num);
    size_t ii, jj, kk, out;
    if (len <= 3) {
        strcpy(cp,num);
    } else {
        ///if (len > 3) {
        size_t mod = len % 3;
        size_t max = (int)( len / 3 );
        ii = 0;
        for (; ii < mod; ii++) {
            cp[ii] = num[ii]; // copy in upper numbers, if any
        }
        cp[ii] = 0;
        out = ii;
		for (jj = 0; jj < max; jj++ ) {
            if ( !((mod == 0) && (jj == 0)) )
                cp[out++] = ',';    // add in comma
            for (kk = 0; kk < 3; kk++) {
                // then 3 numbers
                cp[out++] = num[ii + (jj*3) + kk];
            }
		}
		cp[out] = 0;
	}
	return cp;
}


double get_seconds()
{
    struct timeval tv;
    gettimeofday(&tv,0);
    double t1 = (double)(tv.tv_sec+((double)tv.tv_usec/1000000.0));
    return t1;
}

// see : http://geoffair.org/misc/powers.htm
char *get_seconds_stg( double dsecs )
{
    static char _s_secs_buf[256];
    char *cp = _s_secs_buf;
    sprintf(cp,"%g",dsecs);
    if (dsecs < 0.0) {
        strcpy(cp,"?? secs");
    } else if (dsecs < 0.0000000000001) {
        strcpy(cp,"~0 secs");
    } else if (dsecs < 0.000000005) {
        // nano- n 10 -9 * 
        dsecs *= 1000000000.0;
        sprintf(cp,"%.3f nano-secs",dsecs);
    } else if (dsecs < 0.000005) {
        // micro- m 10 -6 * 
        dsecs *= 1000000.0;
        sprintf(cp,"%.3f micro-secs",dsecs);
    } else if (dsecs <  0.005) {
        // milli- m 10 -3 * 
        dsecs *= 1000.0;
        sprintf(cp,"%.3f milli-secs",dsecs);
    } else if (dsecs < 60.0) {
        sprintf(cp,"%.3f secs",dsecs);
    } else {
        int mins = (int)(dsecs / 60.0);
        dsecs -= (double)mins * 60.0;
        if (mins < 60) {
            sprintf(cp,"%d:%.3f min:secs", mins, dsecs);
        } else {
            int hrs = mins / 60;
            mins -= hrs * 60;
            sprintf(cp,"%d:%02d:%.3f hrs:min:secs", hrs, mins, dsecs);
        }
    }
    return cp;
}

void clean_feat( PFEAT pf )
{
#ifdef USE_NEW_FEAT
    pf->color = 0;
    pf->vAnnos.clear();
    pf->vPoints.clear();
    pf->comments.clear();
#else
    pf->anno = "";
    pf->comments.clear();
    pf->pt.lat = BAD_LL;
    pf->pt.lon = BAD_LL;
    pf->vPoints.clear();
#endif
}

double string_to_double( const std::string& s )
{
    std::istringstream i(s);
    double x;
    if (!(i >> x))
      return 0;
    return x;
} 

typedef struct tagFLAG2STG {
    int bit;
    const char *nm;
}FLAG2STG, *PFLAG2STG;

static FLAG2STG sFlag2Stg[] = {
    { v_id, "id" }, //   0x00000001
    { v_ts, "timestamp" },  //   0x00000002
    { v_uid, "uid" },   //  0x00000004
    { v_user, "user" }, // 0x00000008
    { v_vis, "visible" }, //  0x00000010
    { v_vers, "version" },  // 0x00000020
    { v_chg, "changeset" }, //  0x00000040
    { v_lat, "lat" },   //  0x00000080
    { v_lon, "lon" },   //  0x00000100
    /* END OF TABLE */
    { 0, 0 }
};

std::string getFlagNames( int flag )
{
    std::stringstream s;
    PFLAG2STG pfs = &sFlag2Stg[0];
    while (pfs->nm) {
        if (flag & pfs->bit) {
            if (s.str().size())
                s << " ";
            s << pfs->nm;
            flag &= ~pfs->bit;
            if (flag == 0)
                break;
        }
        pfs++;
    }
    if (flag) {
        char *tmp = GetNxtBuf();
        sprintf(tmp,"Other bits %X", flag);
        if (s.str().size())
            s << " ";
        s << tmp;
    }
    return s.str();
}

std::string getMissingBits( int got, int expect )
{
    expect &= ~got;
    if (expect)
        return getFlagNames(expect);
    return "none";
}

long long ut_strtosint64(const char* s)
{
    // read a number and convert it to a signed 64-bit integer;
    // return: number;
    int sign;
    long long i;
    byte b;

    if( *s =='-') { 
        s++; 
        sign = -1; 
    } else 
        sign = 1;

    i= 0;
    for(;;) {
        b = (byte)(*s++ -'0');
        if( b >= 10 )
            break;
        i = (i * 10) + b;
    }
    return i * sign;
}   // end   ut_strtosint64()


//////////////////////////////////////////////////////////////////////////
#ifndef MEOL
#define MEOL std::endl
#endif
///////////////////////////////////////////////////////////
//// output the xgraph files
///////////////////////////////////////////////////////////
static bool reset_color = true;
static const char *def_color = "yellow";

std::string get_xg_bbox( double minlon, double minlat, double maxlon, double maxlat )
{
    std::stringstream s;
    s << "# bbox " << minlon << " " << minlat << " " << maxlon << " " << maxlat << std::endl;
    SPRTF("%s", s.str().c_str());

    s << "color gray" << std::endl;
    s << minlon << " " << minlat << std::endl;
    s << minlon << " " << maxlat << std::endl;
    s << maxlon << " " << maxlat << std::endl;
    s << maxlon << " " << minlat << std::endl;
    s << minlon << " " << minlat << std::endl;
    s << "NEXT" << std::endl;
    return s.str();
}

////////////////////////////////////////////////////////////////////
// Output features collected from the OSM data
///////////////////////////////////////////////////////////////////
int output_feats( vFEATS &vFeats, std::string out_files, bool verbxg, 
    bool auto_color, int options ) 
{
    int iret = 0;
    size_t ii, max = vFeats.size();
    if (max == 0) {
        SPRTF("%s: No features found!\n", module);
        return 1;
    }
    SPRTF("%s: Collected %d Features\n", module, (int)max);
    std::stringstream s;
    PAPT papt, plast;
    FILE *fp;
    char *ptmp;
    size_t ii4, max4;
    PFEAT pf;
    size_t ii2, max2;
    size_t len, res, total_out = 0;
    double min_lat = 400.0;
    double min_lon = 400.0;
    double max_lat = -400.0;
    double max_lon = -400.0;
    double lat,lon;
#ifdef USE_NEW_FEAT
    const char *last_color = 0;
#endif
    std::string stg;

    s.precision(15);
    fp = fopen(out_files.c_str(),"w");
    if (!fp) {
        SPRTF("%s: Failed to 'open' file '%s'!\n", module, out_files.c_str());
        return 1;
    }
    ptmp = GetNxtBuf();
    sprintf(ptmp,"# Collected %d Features - %s\n", (int)max, get_date_time_stg());
    len = strlen(ptmp);
    res = fwrite(ptmp,1,len,fp);
    if (res != len) {
        SPRTF("Error: File write failed\n");
        fclose(fp);
        return 1;
    }
    for (ii = 0; ii < max; ii++) {
        pf = &vFeats[ii];
        max2 = pf->vPoints.size();
        if (!max2)
            continue;
        s.str("");  // clear the string
        ptmp = GetNxtBuf();
        papt = &pf->vPoints[0];
        lon = papt->lon;
        lat = papt->lat;
        max4 = pf->comments.size();
        if (auto_color) {
            sprintf(ptmp,"color = %d", (int)ii);
            s << ptmp << MEOL;
        }
#ifdef USE_NEW_FEAT
        else if (pf->color) {
            // we have a color - set it, even if last_color is already this
            sprintf(ptmp,"color = %s", pf->color);
            s << ptmp << MEOL;
            last_color = pf->color;
        } else if (reset_color) {
            // we do NOT have a color - and we have a reset instruction
            if (last_color) {
                // do we need a reset of the color
                if (strcmp(last_color,def_color)) {
                    sprintf(ptmp,"color = %s", def_color);
                    s << ptmp << MEOL;
                    last_color = def_color;
                }
            } else {
                // have not had a color, so set default
                sprintf(ptmp,"color = %s", def_color);
                s << ptmp << MEOL;
                last_color = def_color;
            }
        }
        size_t i3, max3 = pf->vAnnos.size();
        for (i3 = 0; i3 < max3; i3++) {
            PANNO pa = &pf->vAnnos[i3];
            sprintf(ptmp,"anno %lf %lf %s",
                pa->pt.lon,
                pa->pt.lat,
                pa->anno.c_str());
            s << ptmp << MEOL;
        }
#else
        if (pf->anno.size() && pf->pt.lat != BAD_LL) {
            sprintf(ptmp,"anno %lf %lf %s",
                pf->pt.lon,
                pf->pt.lat,
                pf->anno.c_str());
            s << ptmp << MEOL;
        }
#endif
        for (ii4 = 0; ii4 < max4; ii4++) {
            strcpy(ptmp,pf->comments[ii4].c_str());
            s << "# " << ptmp << MEOL;
        }
        for (ii2 = 0; ii2 < max2; ii2++) {
            papt = &pf->vPoints[ii2];
            lon = papt->lon;
            lat = papt->lat;
            s << lon << " " << lat;
            if (options & 0x01) {
                if (ii2) {
                    double dlon = lon - plast->lon;
                    double dlat = lat - plast->lat;
                    //double dist = M2NM * sqrt( (dlon * dlon) + (dlat * dlat) );
                    double dist = sqrt( (dlon * dlon) + (dlat * dlat) );
                    sprintf(ptmp," # %d %lu %lf m", (int)ii2, papt->ulid, dist);
                } else {
                    sprintf(ptmp," # %d %lu", (int)ii2, papt->ulid);
                }
                s << ptmp;
            }
            s << MEOL;
            if (lat > max_lat)
                max_lat = lat;
            if (lat < min_lat)
                min_lat = lat;
            if (lon > max_lon)
                max_lon = lon;
            if (lon < min_lon)
                min_lon = lon;
            plast = papt;
        }
        s << "NEXT" << MEOL;
        if (options & 0x8000) {
            // maybe too large for buffer SPRTF("%s",s.str().c_str());
            direct_out_it((char *)s.str().c_str());
        }
        ///////////////////////////////////
        // write out the OSM features
        ///////////////////////////////////
        len = s.str().size();
        res = fwrite(s.str().c_str(), 1, len, fp);
        if (res != len) {
            SPRTF("Error: File write failed\n");
            fclose(fp);
            return 1;
        }
        total_out += len;
    }
    stg = get_xg_bbox(min_lon, min_lat, max_lon, max_lat );
    len = stg.size();
    if (len) {
        res = fwrite(stg.c_str(), 1, len, fp);
        if (res != len) {
            SPRTF("Error: File write failed\n");
            fclose(fp);
            return 1;
        }
        total_out += len;
    }

    // add to file
    ptmp = GetNxtBuf();
    sprintf(ptmp,"# <bounds minlat=\"%lf\" minlon=\"%lf\" maxlat=\"%lf\" maxlon=\"%lf\" />\n",
        min_lat, min_lon, max_lat, max_lon);
    len = strlen(ptmp);
    res = fwrite(ptmp,1,len,fp); // write a 'bounds' line
    if (res != len) {
        SPRTF("Error: File write failed\n");
        fclose(fp);
        return 1;
    }
    total_out += len;

    sprintf(ptmp,"# eof %s - %d bytes (appx)\n", out_files.c_str(), (int)total_out );
    len = strlen(ptmp);
    total_out += len;
    sprintf(ptmp,"# eof %s - %d bytes (appx)\n", out_files.c_str(), (int)total_out );
    len = strlen(ptmp);
    res = fwrite(ptmp,1,len,fp);
    if (res != len) {
        SPRTF("Error: File write failed\n");
        fclose(fp);
        return 1;
    }
    fclose(fp);
    SPRTF("%s: Results written to '%s', %d bytes.\n", module, out_files.c_str(), (int)max);
    return iret;
}

//////////////////////////////////////////////////////////////////////////
#ifndef _T
#define _T(a)   a
#endif
#ifndef _TCHAR
#define _TCHAR char
#endif
#ifndef SPRINTF
#define SPRINTF sprintf
#endif
#ifndef STRCAT
#define STRCAT strcat
#endif
#ifndef STRLEN
#define STRLEN strlen
#endif
void trim_float_buf( _TCHAR * pb )
{
   size_t len = STRLEN(pb);
   size_t i, dot, zcnt;
   for( i = 0; i < len; i++ )
   {
      if( pb[i] == '.' )
         break;
   }
   dot = i + 1; // keep position of decimal point (a DOT)
   zcnt = 0;
   for( i = dot; i < len; i++ )
   {
      if( pb[i] != '0' )
      {
         i++;  // skip past first
         if( i < len )  // if there are more chars
         {
            i++;  // skip past second char
            if( i < len )
            {
               size_t i2 = i + 1;
               if( i2 < len )
               {
                  if ( pb[i2] >= '5' )
                  {
                     if( pb[i-1] < '9' )
                     {
                        pb[i-1]++;
                     }
                  }
               }
            }
         }
         pb[i] = 0;
         break;
      }
      zcnt++;     // count ZEROS after DOT
   }
   if( zcnt == (len - dot) )
   {
      // it was ALL zeros
      pb[dot - 1] = 0;
   }
}

char *get_k_num( long long i64, int type )
{
   int dotrim = 1;
   char *pb = GetNxtBuf();
   const char *form = _T(" bytes");
   unsigned long long byts = i64;
   double res;
   _TCHAR * ffm = (_TCHAR *)_T("%0.20f");  // get 20 digits
   if( byts < 1024 ) {
      SPRINTF(pb, _T("%s"), get_I64_Stg(byts));
      dotrim = 0;
   } else if( byts < 1024*1024 ) {
      res = ((double)byts / 1024.0);
      form = (type ? _T(" KiloBypes") : _T(" KB"));
      SPRINTF(pb, ffm, res);
   } else if( byts < 1024*1024*1024 ) {
      res = ((double)byts / (1024.0*1024.0));
      form = (type ? _T(" MegaBypes") : _T(" MB"));
      SPRINTF(pb, ffm, res);
   } else { // if( byts <  (1024*1024*1024*1024)){
      double val = (double)byts;
      double db = (1024.0*1024.0*1024.0);   // x3
      double db2 = (1024.0*1024.0*1024.0*1024.0);   // x4
      if( val < db2 )
      {
         res = val / db;
         form = (type ? _T(" GigaBypes") : _T(" GB"));
         SPRINTF(pb, ffm, res);
      }
      else
      {
         db *= 1024.0;  // x4
         db2 *= 1024.0; // x5
         if( val < db2 )
         {
            res = val / db;
            form = (type ? _T(" TeraBypes") : _T(" TB"));
            SPRINTF(pb, ffm, res);
         }
         else
         {
            db *= 1024.0;  // x5
            db2 *= 1024.0; // x6
            if( val < db2 )
            {
               res = val / db;
               form = (type ? _T(" PetaBypes") : _T(" PB"));
               SPRINTF(pb, ffm, res);
            }
            else
            {
               db *= 1024.0;  // x6
               db2 *= 1024.0; // x7
               if( val < db2 )
               {
                  res = val / db;
                  form = (type ? _T(" ExaBypes") : _T(" EB"));
                  SPRINTF(pb, ffm, res);
               }
               else
               {
                  db *= 1024.0;  // x7
                  res = val / db;
                  form = (type ? _T(" ZettaBypes") : _T(" ZB"));
                  SPRINTF(pb, ffm, res);
               }
            }
         }
      }
   }
   if( dotrim > 0 )
      trim_float_buf(pb);

   STRCAT(pb, form);

   //if( ascii > 0 )
   //   Convert_2_ASCII(pb);

   return pb;
}
//////////////////////////////////////////////////////////////////////////

// copy string up to next 'quote' mark
int strcpy2qt( char *dst, char *src, char *xend)
{
    int c, cnt = 0;
    // while (( c = *src ) && ( c != '"' ) && ( c != '\'' ) && ( c > ' ' ) && ( src < xend )) {
    while (( c = *src ) && ( c != '"' ) && ( c != '\'' ) && ( src < xend )) {
        *dst++ = *src++;
        cnt++;
    }
    *dst = 0;
    return cnt;
}

int doubletofixedint( double d )
{
    int res = (int)(d * 10000000L);
    return res;
}

// read a number and convert it to a signed 64-bit integer;
// return: number;
long long strtosint64(const char* s) 
{
    int sign = 0;
    long long i = 0;
    unsigned char b;

    if( *s == '-' ) { 
        s++; 
        sign = 1; 
    }

    for ( ; ; ) {

        if (!ISDIGIT(*s))
            break;

        b = (unsigned char)(*s++ - '0');

        i = (i * 10) + b;
        
    }
    if (sign)
        i *= -1;

    return i;
}

char *double_to_stg( double d )
{
    char *cp = GetNxtBuf();
    sprintf(cp,"%lf",d);
    trim_float_buf(cp);
    return cp;
}


//////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

typedef struct tagTAG2CLR {
    const char *key;
    const char *val;
    const char *color;
} TAG2CLR, *PTAG2CLR;

// TODO: This table needs to be filled from an external source! ie user configurable
static TAG2CLR sTag2Color[] = {
    { "aeroway", "*", "red" },
    { "highway", "primary", "orange" },
    { "highway", "residential", "grey" },
    { "highway", "secondary", "orange" },
    { "highway", "service", "grey" },
    { "highway", "tertiary", "brown" },
    { "highway", "track", "brown" },
    { "highway", "trunk", "orange" },
    { "highway", "turning_circle", "grey" },
    { "highway", "unclassified", "brown" },
    { "highway", "*", "grey" },
    { "railway", "level_crossing", "red" },
    { "railway", "*", "grey" },
    { "waterway", "*", "blue" },
    { "landuse", "conservation", "green" },
    { "landuse", "farmland", "violet" },
    { "barrier", "*", "red" },
    { "leisure", "*", "green" },

    // must be LAST
    { 0, 0, 0 }
};

const char *get_color_for_tag( PTAG pt )
{
    PTAG2CLR ptc = &sTag2Color[0];
    while (ptc->val) {
        if (ptc->val[0] == '*') {
            if (strcmp(ptc->key,pt->k.c_str()) == 0) {
                return ptc->color;
            }
        } else
        if (strcmp(ptc->val,pt->v.c_str()) == 0) {
            return ptc->color;
        }
        ptc++;
    }
    return 0;
}
/////////////////////////////////////////////////////////////////////////////
static vSTG whitespace_split( const std::string &str, int maxsplit )
{
    vSTG result;
    std::string::size_type len = str.length();
    std::string::size_type i = 0;
    std::string::size_type j;
    int countsplit = 0;

    while (i < len) {
        while ((i < len) && isspace((unsigned char)str[i])) {
            i++;
		}
		j = i;  // keep end of white space

		while ((i < len) && !isspace((unsigned char)str[i])) {
            i++;
		}
		if (j < i) {
            result.push_back( str.substr(j, i-j) );
		    ++countsplit;
		    while ((i < len) && isspace((unsigned char)str[i])) {
                i++;
		    }

		    if (maxsplit && (countsplit >= maxsplit) && (i < len)) {
                result.push_back( str.substr( i, len-i ) );
			    i = len;
		    }
		}
    }
    return result;
}

vSTG string_split( const std::string &str, const char *seps, int maxsplit )
{
    if (seps == 0)
        return whitespace_split( str, maxsplit );

    vSTG result;
    size_t k, n = strlen( seps );
    if (n == 0) {
        // empty separator string
        return result;
    }
    const char *s = str.c_str();
    std::string::size_type len = str.length();
	std::string::size_type i = 0;
	std::string::size_type j = 0;
	int splitcount = 0;
    char c;
    bool matched;
	while ( i < len) {
        c = s[i];
        matched = false;
        for (k = 0; k < n; k++) {
            if (c == seps[k]) { // does char match any of the separators
                if (( i - j ) > 0) {
                    result.push_back( str.substr(j,i-j) );
    		        ++splitcount;
                }
		        i = j = (i + 1);
                matched = true;
                // skip over any subsequent match
                for ( ; i < len; i++, j++) {
                    c = s[i];
                    for (k = 0; k < n; k++) {
                        if (c == seps[k]) { // does char match any of the separators
                            break;
                        }
                    }
                    if (k == n) // no match found
                        break;
                }
                break;
            }
        }
        if (matched) {
            // i has already been incremented to next
            if (maxsplit && (splitcount >= maxsplit))
                break;
        } else { // if (!matched)
		    i++;    // just bump i
		}
    }

    result.push_back( str.substr(j,len-j) );
    return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// from : http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
//////////////////////////////////////////////////////////////////////////////////////////////////////////
double f_sqr(double x) { return x * x; }
double f_dist2(Vec2 &v, Vec2 &w) { return f_sqr(v.x - w.x) + f_sqr(v.y - w.y); }
double distToSegmentSquared(Vec2 &p, Vec2 &v, Vec2 &w)
{
    double l2 = f_dist2(v, w);
    if (l2 == 0)
        return f_dist2(p, v);
    double t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
    if (t < 0)
        return f_dist2(p, v);
    if (t > 1)
        return f_dist2(p, w);
    Vec2 v2;
    v2.x = v.x + t * (w.x - v.x);
    v2.y = v.y + t * (w.y - v.y);
    return f_dist2(p, v2);
}

double distToSegment(Vec2 &p, Vec2 &v, Vec2 &w)
{ 
    return sqrt(distToSegmentSquared(p, v, w)); 
}

// OTHER SOLUTIONS
// ===============
double pDistance(double x, double y, double x1, double y1, double x2, double y2)
{
    double A = x - x1;
    double B = y - y1;
    double C = x2 - x1;
    double D = y2 - y1;

    double dot = A * C + B * D;
    double len_sq = C * C + D * D;
    double param = dot / len_sq;

    double xx, yy;

    if ((param < 0) || ((x1 == x2) && (y1 == y2))) {
        xx = x1;
        yy = y1;
    } else if (param > 1) {
        xx = x2;
        yy = y2;
    } else {
        xx = x1 + param * C;
        yy = y1 + param * D;
    }

    double dx = x - xx;
    double dy = y - yy;

    return sqrt((dx * dx) + (dy * dy));
}

//Compute the dot product AB . AC
double DotProduct2D(double *pointA, double *pointB, double *pointC)
{
    double AB[2];
    double BC[2];
    AB[0] = pointB[0] - pointA[0];
    AB[1] = pointB[1] - pointA[1];
    BC[0] = pointC[0] - pointB[0];
    BC[1] = pointC[1] - pointB[1];
    double dot = AB[0] * BC[0] + AB[1] * BC[1];
    return dot;
}

//Compute the cross product AB x AC
double CrossProduct2D(double *pointA, double *pointB, double *pointC)
{
    double AB[2];
    double AC[2];
    AB[0] = pointB[0] - pointA[0];
    AB[1] = pointB[1] - pointA[1];
    AC[0] = pointC[0] - pointA[0];
    AC[1] = pointC[1] - pointA[1];
    double cross = AB[0] * AC[1] - AB[1] * AC[0];
    return cross;
}

//Compute the distance from A to B
double Distance2D(double *pointA, double *pointB)
{
    double d1 = pointA[0] - pointB[0];
    double d2 = pointA[1] - pointB[1];
    return sqrt(d1 * d1 + d2 * d2);
}

//Compute the distance from AB to C
//if isSegment is true, AB is a segment, not a line.
double LineToPointDistance2D(double *pointA, double *pointB, double *pointC, bool isSegment)
{
    double dist = CrossProduct2D(pointA, pointB, pointC) / Distance2D(pointA, pointB);
    if (isSegment)
    {
        double dot1 = DotProduct2D(pointA, pointB, pointC);
        if (dot1 > 0) 
            return Distance2D(pointB, pointC);

        double dot2 = DotProduct2D(pointB, pointA, pointC);
        if (dot2 > 0) 
            return Distance2D(pointA, pointC);
    }
    return fabs(dist);
} 


//////////////////////////////////////////////////
// fuzzy test for zero, or close to zero
#ifndef MY_VERYSMALL
#define MY_VERYSMALL  (1.0E-150)
#endif
#ifndef MY_EPSILON
#define MY_EPSILON    (1.0E-8)
#endif

bool absoluteIsZero(double x)
{
    if (x == 0.0)
        return true;
    bool bret = (fabs(x) <= std::numeric_limits<double>::epsilon()) ? true : false;
    return bret;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// from : http://stackoverflow.com/questions/17333/most-effective-way-for-float-and-double-comparison

bool absoluteToleranceCompare(double x, double y)
{
    bool bret = (fabs(x - y) <= std::numeric_limits<double>::epsilon()) ? true : false;
    return bret;
}

// from : http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
// and : http://en.wikipedia.org/wiki/Line-line_intersection

// This gets the intersection point IF one exists when the lines are extended to infinity
// **************************************************************************************
bool IntersectionPoint( double x1, double y1, double x2, double y2, 
    double x3, double y3, double x4, double y4,
    double *x, double *y )
{
    double denom = ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4));
    if (absoluteIsZero(denom)) {
        return false;   // lines are parallel - NO INTERSECTION
    }
    double dx = (((x1 * y2) - (y1 * x2)) * (x3 - x4)) -
                ((x1 - x2) * ((x3 * y4) - (y3 * x4)));

    double dy = (((x1 * y2) - (y1 * x2)) * (y3 - y4)) -
                ((y1 - y2) * ((x3 * y4) - (y3 * x4)));

    *x = dx / denom;
    *y = dy / denom;

    return true;
}

// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines 
// intersect the intersection point may be stored in the floats i_x and i_y.
// This checks the lines as is, with no extension.
char get_line_intersection(double p0_x, double p0_y, double p1_x, double p1_y, 
    double p2_x, double p2_y, double p3_x, double p3_y,
    double *i_x, double *i_y)
{
    double s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;
    s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;
    s2_y = p3_y - p2_y;

    double d = (-s2_x * s1_y + s1_x * s2_y);

    if (absoluteIsZero(d)) {
        return 0;
    }

    double s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / d;
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / d;

    if ((s >= 0) && (s <= 1) && (t >= 0) && (t <= 1))
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }

    return 0; // No collision
}

//////////////////////////////////////////////////////////////////////////
// from : http://ptspts.blogspot.fr/2010/06/how-to-determine-if-two-line-segments.html
// 
bool IsOnSegment(double xi, double yi, double xj, double yj,
                        double xk, double yk) 
{
    return (xi <= xk || xj <= xk) && (xk <= xi || xk <= xj) &&
         (yi <= yk || yj <= yk) && (yk <= yi || yk <= yj);
}

char ComputeDirection(double xi, double yi, double xj, double yj,
                             double xk, double yk) 
{
      double a = (xk - xi) * (yj - yi);
      double b = (xj - xi) * (yk - yi);
      return a < b ? -1 : a > b ? 1 : 0;
}

/** Do line segments (x1, y1)--(x2, y2) and (x3, y3)--(x4, y4) intersect? */
bool DoLineSegmentsIntersect(double x1, double y1, double x2, double y2,
                             double x3, double y3, double x4, double y4) 
{
    char d1 = ComputeDirection(x3, y3, x4, y4, x1, y1);
    char d2 = ComputeDirection(x3, y3, x4, y4, x2, y2);
    char d3 = ComputeDirection(x1, y1, x2, y2, x3, y3);
    char d4 = ComputeDirection(x1, y1, x2, y2, x4, y4);
    return (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
           ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) ||
           (d1 == 0 && IsOnSegment(x3, y3, x4, y4, x1, y1)) ||
           (d2 == 0 && IsOnSegment(x3, y3, x4, y4, x2, y2)) ||
           (d3 == 0 && IsOnSegment(x1, y1, x2, y2, x3, y3)) ||
           (d4 == 0 && IsOnSegment(x1, y1, x2, y2, x4, y4));
}

////////////////////////////////////////////////////////////////
double positive_angle_degs( double x1, double y1, double x2, double y2 )
{
    double dx = (x1 > x2) ? x1 - x2 : x2 - x1;
    double dy = (y1 > y2) ? y1 - y2 : y2 - y1;
    double degs = atan2( dx, dy ) * RAD2DEG;
    return degs;
}

double angle_degs( double x1, double y1, double x2, double y2 )
{
    double dx = x1 - x2;
    double dy = y1 - y2;
    double degs = atan2( dx, dy ) * RAD2DEG;
    return degs;
}

// always ensure the least y value is the first
double yfixed_angle_degs( double x1, double y1, double x2, double y2 )
{
    double dx,dy;
    if (y1 < y2) {
        dx = x2 - x1;
        dy = y2 - y1;
    } else {
        dx = x1 - x2;
        dy = y1 - y2;
    }
    double degs = atan2( dx, dy ) * RAD2DEG;
    return degs;
}

// always ensure the least x value is the first
double xfixed_angle_degs( double x1, double y1, double x2, double y2 )
{
    double dx,dy;
    if (x1 < x2) {
        dx = x2 - x1;
        dy = y2 - y1;
    } else {
        dx = x1 - x2;
        dy = y1 - y2;
    }
    double degs = atan2( dx, dy ) * RAD2DEG;
    return degs;
}

////////////////////////////////////////////////////////////////////////////
// eof = utils.cxx
