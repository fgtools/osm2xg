/*\
 * utils.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _UTILS_HXX_
#define _UTILS_HXX_
#include <string>
#include "osm2xg.hxx"   // osm structures

#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else // !_MSC_VER
#define M_IS_DIR S_IFDIR
#endif

enum DiskType {
    DT_NONE,
    DT_FILE,
    DT_DIR
};

extern DiskType is_file_or_directory32 ( const char * path );
extern size_t get_last_file_size32();
//#ifdef _MSC_VER
extern DiskType is_file_or_directory64 ( const char * path );
extern long long get_last_file_size64();
//#endif

extern bool valid_lat( double lat );
extern bool valid_lon( double lon );
extern bool in_world_range( double lat, double lon );
#ifdef _MSC_VER
extern bool bIsRedON();
#endif
extern double get_seconds();
extern char *get_seconds_stg( double dsecs );

#define ISDIGIT(a) ((a >= '0')&&(a <= '9'))

extern char *get_I64_Stg( long long val );
extern char *get_nn(char *num); // nice number nicenum add commas
extern char *get_k_num( long long i64, int type = 0 );

extern void clean_feat( PFEAT pf );

extern double string_to_double( const std::string& s );
extern char *double_to_stg( double d );

extern std::string getFlagNames( int flag );
extern std::string getMissingBits( int got, int expect );

extern int output_feats( vFEATS &vFeats, std::string out_files, bool verbxg = false,
    bool auto_color = true, int verb = 0 );
// copy string up to next 'quote' mark
extern int strcpy2qt( char *dst, char *src, char *xend);
extern int doubletofixedint( double d ); // double * 10000000L
extern long long strtosint64(const char *s); // stop at fist non-digit

// develop tag to color service
extern const char *get_color_for_tag( PTAG pt );

extern vSTG string_split( const std::string &str, const char* sep = 0, int maxsplit = 0 );

typedef struct tagVec2 {
    double x,y;
}Vec2, *pVec2;

extern double f_sqr(double x);
extern double f_dist2(Vec2 &v, Vec2 &w);
extern double distToSegmentSquared(Vec2 &p, Vec2 &v, Vec2 &w);
extern double distToSegment(Vec2 &p, Vec2 &v, Vec2 &w);

extern double positive_angle_degs( double x1, double y1, double x2, double y2 );
extern double angle_degs( double x1, double y1, double x2, double y2 );
// always ensure the least x value is the first
extern double xfixed_angle_degs( double x1, double y1, double x2, double y2 );
// always ensure the least y value is the first
extern double yfixed_angle_degs( double x1, double y1, double x2, double y2 );

extern std::string get_xg_bbox( double minlon, double minlat, double maxlon, double maxlat );


#endif // #ifndef _UTILS_HXX_
// eof - utils.hxx
